export default [
  {
    path: "/parent",
    component: () => import("@/views/parent/Base.vue"),
    meta: { auth: true, role: "parent" },
    children: [
      {
        path: "/",
        name: "parent",
        component: () => import("@/views/parent/Profile.vue"),
        meta: { auth: true, role: "parent" },
      },
      {
        path: "momdadclub",
        name: "parent-momdadclub",
        component: () => import("@/views/parent/MomDadClub.vue"),
        meta: { auth: true, role: "Parent" },
      },
      {
        path: "children",
        component: () => import("@/views/main/Base.vue"),
        meta: { auth: true, role: "Parent" },
        children: [
          {
            path: "/",
            name: "parent-children",
            component: () => import("@/views/parent/Children.vue"),
            meta: { auth: true, role: "Parent" },
          },
          {
            path: ":childId",
            name: "parent-child",
            component: () => import("@/views/parent/Child.vue"),
            meta: { auth: true, role: "Parent" },
          },
        ],
      },
    ],
  },
];
