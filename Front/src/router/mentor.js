export default [
  {
    path: "/mentor",
    component: () => import("@/views/mentor/Base.vue"),
    meta: { auth: true, role: "Mentor" },
    children: [
      {
        path: "/",
        name: "mentor",
        component: () => import("@/views/parent/Profile.vue"),
        meta: { auth: true, role: "Mentor" },
      },
      {
        path: "dictionary",
        name: "mentor-dictionary",
        component: () => import("@/views/mentor/Dictionary.vue"),
        meta: { auth: true, role: "Mentor" },
      },
      {
        path: "groups",
        component: () => import("@/views/main/Base.vue"),
        meta: { auth: true, role: "Mentor" },
        children: [
          {
            path: "/",
            name: "mentor-groups",
            component: () => import("@/views/mentor/Groups.vue"),
            meta: { auth: true, role: "Mentor" },
          },
          {
            path: ":groupId",
            name: "mentor-group",
            component: () => import("@/views/mentor/Group.vue"),
            meta: { auth: true, role: "Mentor" },
          },
          {
            path: ":groupId/child/:childId",
            name: "mentor-child",
            component: () => import("@/views/mentor/Child.vue"),
            meta: { auth: true, role: "Mentor" },
          },
        ],
      },
    ],
  },
];
