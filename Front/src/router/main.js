export default [
  {
    // Страница регистрации для родителя и ментора
    path: "/",
    name: "main-register",
    component: () => import("@/views/main/Register.vue"),
  },
  {
    path: "/login",
    name: "main-login",
    component: () => import("@/views/main/Login.vue"),
  },
  {
    path: "/register/partner",
    component: () => import("@/views/main/partner/Register.vue"),
    children: [
      {
        path: "/",
        name: "main-partner-register",
        component: () => import("@/views/main/partner/Role.vue"),
      },
      {
        path: "agent",
        name: "main-partner-register-agent",
        component: () => import("@/views/main/partner/Agent.vue"),
      },
      // TODO: Доделать франчайз на бэке
      {
        path: "franchisee",
        name: "main-partner-register-franchisee",
        component: () => import("@/views/main/partner/Franchisee.vue"),
      },

      {
        path: "franchisee/:referal",
        name: "global-admin-franchisee-referal",
        component: () => import("@/views/main/partner/Franchisee.vue"),
      },
    ],
  },
  {
    path: "/franchisee-authentification",
    component: () => import("@/views/main/Login.vue"),
    children: [
      {
        path: "/",
        name: "global-admin-role",
        component: () => import("@/views/main/partner/Role.vue"),
      },
      {
        path: "agent",
        name: "global-admin-agent",
        component: () => import("@/views/main/partner/Agent.vue"),
      },
      {
        path: "franchisee",
        name: "global-admin-franchisee",
        component: () => import("@/views/main/partner/Franchisee.vue"),
      },
      {
        path: "franchisee/:referal",
        name: "global-admin-franchisee-referal",
        component: () => import("@/views/main/partner/Franchisee.vue"),
      },
    ],
  },
];
