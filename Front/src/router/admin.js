export default [
  {
    path: "/admin",
    name: "admin",
    component: () => import("@/views/admin/Base.vue"),
    meta: { auth: true, role: "admin" },
    children: [
      {
        path: "analytics",
        component: () => import("@/views/admin/analytics/Analytics.vue"),
        meta: { auth: true, role: "admin" },
        children: [
          {
            path: "/",
            name: "admin-analytics-crm",
            component: () => import("@/views/admin/analytics/CRM.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "franchisee/:id",
            component: () =>
              import("@/views/franchisee/analytics/Analytics.vue"),
            meta: { auth: true, role: "admin" },
            children: [
              {
                path: "/",
                name: "admin-analytics-branches",
                component: () =>
                  import("@/views/franchisee/analytics/Branches.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "moreinfo",
                name: "admin-analytics-moreinfo",
                component: () =>
                  import("@/views/admin/analytics/Franchisee/more-info.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "mentor/:id",
                name: "admin-analytics-mentor",
                component: () => import("@/views/branch/analytics/Mentor.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "branch/:id",
                name: "admin-analytics-branch",
                component: () =>
                  import("@/views/franchisee/analytics/Branch.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "child/:id",
                name: "admin-analytics-child",
                component: () =>
                  import("@/views/branch/analytics/child/Info.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "child/:id/tale/:tale",
                name: "admin-analytics-child-tale",
                component: () =>
                  import("@/views/branch/analytics/child/Progress.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "child/:id/archive",
                name: "admin-analytics-child-archive",
                component: () =>
                  import("@/views/branch/analytics/child/Archive.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "franch-data",
                name: "admin-analytics-fr-data",
                component: () => import("@/views/admin/data/Change.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "franch-personal-data",
                name: "admin-analytics-fr-data-mydata",
                component: () => import("@/views/admin/data/Data.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: "franch-documents/:id",
                name: "admin-analytics-fr-data-documents",
                component: () =>
                  import("@/views/franchisee/data/Documents.vue"),
                meta: { auth: true, role: "admin" },
              },
              {
                path: ":pack",
                name: "admin-analytics-pack",
                component: () =>
                  import("@/views/admin/analytics/Franchisee/more-info.vue"),
                meta: { auth: true, role: "admin" },
              },
            ],
          },
          {
            path: "agent/:id",
            name: "admin-agent",
            component: () => import("@/views/agent/Analytics.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "agent-data/:id",
            name: "admin-agent-data",
            component: () => import("@/views/admin/data/AgentData.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "agent-anketa/:id",
            name: "admin-agent-anketa",
            component: () => import("@/views/agent/Docs.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "reports",
            name: "admin-analytics-reports",
            component: () => import("@/views/admin/analytics/Reports.vue"),
            meta: { auth: true, role: "admin" },
          },
        ],
      },
      {
        path: "children",
        component: () => import("@/views/admin/children/Analytics.vue"),
        meta: { auth: true, role: "admin" },
        children: [
          {
            path: "/",
            name: "admin-children-all",
            component: () => import("@/views/admin/children/All.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id",
            name: "admin-children-child-info",
            component: () => import("@/views/branch/analytics/child/Info.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id/tale/:tale",
            name: "admin-children-child-progress",
            component: () =>
              import("@/views/branch/analytics/child/Progress.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id/archive",
            name: "admin-children-child-archive",
            component: () =>
              import("@/views/branch/analytics/child/Archive.vue"),
            meta: { auth: true, role: "admin" },
          },
        ],
      },
      {
        path: "branch",
        component: () => import("@/views/admin/branch/Analytics.vue"),
        meta: { auth: true, role: "admin" },
        children: [
          {
            path: "/",
            name: "admin-branch-my",
            component: () => import("@/views/admin/branch/MyBranches.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "branchinfo",
            name: "admin-branch-info",
            component: () => import("@/views/admin/branch/Info.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "mentor/:id",
            name: "admin-branch-mentor-info",
            component: () => import("@/views/branch/analytics/Mentor.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id",
            name: "admin-branch-child-info",
            component: () => import("@/views/branch/analytics/child/Info.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id/tale/:tale",
            name: "admin-branch-child-progress",
            component: () =>
              import("@/views/branch/analytics/child/Progress.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "child/:id/archive",
            name: "admin-branch-child-archive",
            component: () =>
              import("@/views/branch/analytics/child/Archive.vue"),
            meta: { auth: true, role: "admin" },
          },
        ],
      },

      {
        path: "panel",
        component: () => import("@/views/admin/administrate/AdminUnion.vue"),
        meta: { auth: true, role: "admin" },
        children: [
          {
            path: "/",
            name: "admin-system",
            component: () => import("@/views/admin/administrate/System.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "data-changing",
            name: "admin-data-change",
            component: () =>
              import("@/views/admin/administrate/DataChanging.vue"),
            meta: { auth: true, role: "admin" },
          },
          {
            path: "support/:id",
            name: "admin-support",
            component: () => import("@/views/admin/administrate/Support.vue"),
            meta: { auth: true, role: "admin" },
          },
        ],
      },

      // {
      //   path: "manager",
      //   component: () => import("@/views/headSalesManager/managers/ManagerIndex.vue"),
      //   meta: { auth: true, role: "HeadSalesManager" },
      //   children: [
      //     {
      //       path: "/",
      //       name: "admin-managers",
      //       component: () => import("@/views/headSalesManager/managers/All.vue"),
      //       meta: { auth: true, role: "HeadSalesManager" },

      //     },

      //     {
      //       path: "manager/:id",
      //       name: "admin-managers-info",
      //       component: () => import("@/views/headSalesManager/managers/Info.vue"),
      //       meta: { auth: true, role: "HeadSalesManager" },
      //     },

      //   ],
      // },
    ],
  },
];
