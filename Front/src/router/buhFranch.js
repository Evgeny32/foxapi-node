export default [
  {
    path: "/buh-franch",
    component: () => import("@/views/buhfranch/BuhFranch.vue"),
    meta: { auth: true, role: "BuhFranch" },

    children: [
      {
        path: "agent-doc",
        name: "buhfranch-agent-doc",
        component: () =>
          import("@/components/buhfranch/docs/AgentDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "product-doc",
        name: "buhfranch-product-doc",
        component: () =>
          import("@/components/buhfranch/docs/ProductDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "bill-doc",
        name: "buhfranch-bill-doc",
        component: () =>
          import("@/components/buhfranch/docs/BillDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "act-doc",
        name: "buhfranch-act-doc",
        component: () =>
          import("@/components/buhfranch/docs/ActDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "upd-doc",
        name: "buhfranch-upd-doc",
        component: () =>
          import("@/components/buhfranch/docs/UPDDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "seminar-doc",
        name: "buhfranch-seminar-doc",
        component: () =>
          import("@/components/buhfranch/docs/SeminarDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "license-doc",
        name: "buhfranch-license-doc",
        component: () =>
          import("@/components/buhfranch/docs/LicenseDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },
      {
        path: "sublicense-doc",
        name: "buhfranch-sublicense-doc",
        component: () =>
          import("@/components/buhfranch/docs/SubLicenseDocViewer.vue"),
        meta: { auth: true, role: "BuhFranch" },
      },      
      {
        path: "children",

        component: () => import("@/views/buhfranch/ChildrenUnion.vue"),
        meta: { auth: true, role: "BuhFranch" },
        children: [
          {
            path: "/",
            name: "buhfranch-children-table",
            component: () =>
              import("@/components/buhfranch/Tables/Children.vue"),
            meta: { auth: true, role: "BuhFranch" },
          },
          {
            path: "info/:id",
            name: "buhfranch-children-info",
            component: () =>
              import(
                "@/components/buhfranch/PersonalInfo/PersonalInfoChildren.vue"
              ),
            meta: { auth: true, role: "BuhFranch" },
          },
          {
            path: "report",
            name: "buhfranch-children-report",
            component: () => import("@/components/buhfranch/Reports/ReportChildren.vue"),
            meta: { auth: true, role: 'BuhFranch' }
          },
          {
            path: "act/:id",
            name: "buhfranch-children-act",
            component: () =>
              import("@/components/buhfranch/Acts/ActViewerChild.vue"),
            meta: { auth: true, role: "BuhFranch" },
          },


        ],
      },
      {
        path: "mentors",

        component: () => import("@/views/buhfranch/MentorsUnion.vue"),
        meta: { auth: true, role: "BuhFranch" },
        children: [
          {
            path: "/",
            name: "buhfranch-mentors-table",
            component: () =>
              import("@/components/buhfranch/Tables/Mentors.vue"),
            meta: { auth: true, role: "BuhFranch" },
          },
          {
            path: "info/:id",
            name: "buhfranch-mentor-info",
            component: () =>
              import(
                "@/components/buhfranch/PersonalInfo/PersonalInfoMentor.vue"
              ),
            meta: { auth: true, role: "BuhFranch" },
          },
          {
            path: "report",
            name: "buhfranch-mentor-report",
            component: () => import("@/components/buhfranch/Reports/ReportMentor.vue"),
            meta: { auth: true, role: 'BuhFranch' }
          },
          {
            path: "act/:id",
            name: "buhfranch-mentor-act",
            component: () =>
              import("@/components/buhfranch/Acts/ActViewerMentor.vue"),
            meta: { auth: true, role: "BuhFranch" },
          },
        ],
      },
    ],
  },
];
