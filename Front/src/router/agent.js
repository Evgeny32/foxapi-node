export default [
  {
    path: "/agent",
    component: () => import("@/views/agent/Base.vue"),
    meta: { auth: true, role: "agent" },
    children: [
      {
        path: "/",
        name: "agent",
        component: () => import("@/views/agent/Analytics.vue"),
        meta: { auth: true, role: "agent" },
      },
      {
        path: "data",
        component: () => import("@/views/main/Base.vue"),
        meta: { auth: true, role: "agent" },
        children: [
          {
            path: "/",
            name: "agent-data",
            component: () => import("@/views/agent/Data.vue"),
            meta: { auth: true, role: "agent" },
          },
          {
            path: "docs",
            name: "agent-docs",
            component: () => import("@/views/agent/Docs.vue"),
            meta: { auth: true, role: "agent" },
          },
        ],
      },

      {
        path: "support",
        name: "agent-support",
        component: () => import("@/views/agent/Support.vue"),
        meta: { auth: true, role: "agent" },
      },
    ],
  },
];
