import Vue from "vue";
import VueRouter from "vue-router";
import mainLinks from "@/router/main";
import parentLinks from "@/router/parent";
import mentorLinks from "@/router/mentor";
import childLinks from "@/router/child";
import agentLinks from "@/router/agent";
import franchiseeLinks from "@/router/franchisee";
import branchLinks from "@/router/branch";
import adminLinks from "@/router/admin";
import buhAdminLinks from "@/router/buhAdmin";
import buhFranchLinks from "@/router/buhAdmin";
import clientManLinks from "@/router/clientMan";
import clientManCabLinks from "@/router/clientManCab";
import headSalesLinks from "@/router/headSales";
import salesManLinks from "@/router/salesMan";
import salesManCabLinks from "@/router/salesManCab";
import talesLinks from "@/router/tales/tale1";

// import { store } from '../store'

Vue.use(VueRouter);

const routes = [
  ...mainLinks,
  ...parentLinks,
  ...mentorLinks,
  ...childLinks,
  ...agentLinks,
  ...franchiseeLinks,
  ...branchLinks,
  ...adminLinks,
  ...buhAdminLinks,
  ...buhFranchLinks,
  ...clientManLinks,
  ...clientManCabLinks,
  ...headSalesLinks,
  ...salesManLinks,
  ...salesManCabLinks,
  ...talesLinks,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// router.beforeEach((to, _, next) => {
//   if (to.meta.auth && !store.getters['UserStore/online']) {
//     next('/login')
//   }
//   else if (['/', '/login', '/childlogin', '/adminlogin'].includes(to.path) && store.getters['UserStore/online']) {
//     next(`/${store.getters['UserStore/role'].toLowerCase()}/me`)
//   }
//   else if (to.meta.role && to.meta.role != store.getters['UserStore/role']) {
//     next(`/${store.getters['UserStore/role'].toLowerCase()}/me`)
//   }
//   else if (to.meta.tale && to.meta.tale > store.getters['TaleStore/tales']) {
//     if (store.getters['TaleStore/tales']) {
//       next(`/tale${store.getters['TaleStore/tales']}-1`)
//     } else[
//       next(`/${store.getters['UserStore/role'].toLowerCase()}/me`)
//     ]
//   }
//   else { next() }
// })

export default router;
