import UIButton from "./UIButton.vue";
import UIButtonRound from "./UIButtonRound.vue";
import UIInput from "./UIInput.vue";
import UIDatePicker from "./UIDatePicker.vue";
import UIFileLoad from "./UIFileLoad.vue";
import UISelect from "./UISelect.vue";
import UITextArea from "./UITextArea.vue";
import Number from "./Number.vue";

export default [
  UIButtonRound,
  UIButton,
  Number,
  UIInput,
  UISelect,
  UIDatePicker,
  UIFileLoad,
  UITextArea,
];
