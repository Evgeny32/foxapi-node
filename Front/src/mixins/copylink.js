export default {
  methods: {
    copylink(value) {
      var copytext = document.createElement("input");
      copytext.value = value;
      document.body.appendChild(copytext);
      copytext.select();
      document.execCommand("copy");
      document.body.removeChild(copytext);
    },
  },
};
