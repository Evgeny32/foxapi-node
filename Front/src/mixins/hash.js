export default {
  methods: {
    hash(value) {
      return window.btoa(JSON.stringify(value));
    },
    unhash(value) {
      return JSON.parse(window.atob(value));
    },
  },
};
