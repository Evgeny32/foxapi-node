import { TransMapper } from "@/store/trans";
import { SberMapper } from "@/store/sber";
import { UserMapper } from "@/store/user";
export default {
  data() {
    return {
      options: [0, 100, 500, 1000, 2000, 5000],
    };
  },
  computed: {
    ...SberMapper.mapState(["type", "value", "title"]),
    ...TransMapper.mapState(["transaction", "error"]),
    ...UserMapper.mapState(["me"]),
  },
  methods: {
    ...SberMapper.mapMutations(["setValue"]),
    // ...TransMapper.mapActions(["postTransaction"]),
    async createTransaction() {
      let transactionData = {
        user: this.me.id,
        value: this.value,
        name: this.title,
      };
      await this.postTransaction(transactionData);
      if (!this.error) {
        let suffix = `?transactionId=${this.transaction.id}`;
        await this.fillBill(suffix);
      }
    },
    async fillBill(suffix) {
      let response = await fetch(
        `http://localhost:8080/payment/rest/register.do?amount=${this.value *
          100}&currency=643&language=ru&orderNumber=${Math.floor(
          Math.random() * this.value
        )}&returnUrl="http://localhost:8080/parent/children/mychild${suffix}"&password=T5024213977&userName=T5024213977-api`
      ).then((res) => res.json());
      if (response.formUrl) {
        let formUrl = response.formUrl;
        window.location.href = formUrl;
      } else {
        console.error(response.errorMessage);
      }
    },
  },
};
