export async function playSound(sound) {
    let audio = new Audio(require("@/assets/sounds/" + sound));
    audio.play();
    audio.onended = () => {
        console.log(audio.duration);
        return
    }
}
