import Vue from "vue";
import App from "./App.vue";
import Vuelidate from "vuelidate";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueTheMask from "vue-the-mask";
import VueApexCharts from "vue-apexcharts";
import moment from "moment";
// import VueTouch from "vue-touch";
import vueScrollBehavior from "vue-scroll-behavior";
import router from "@/router";
import { store } from "@/store";
import "@/assets/fonts/Montserrat/stylesheet.css";
import errorToastMixin from "@/mixins/errorToast";
import toastMixin from "@/mixins/toast";
import ui from "@/components/ui";
// Vue.use(VueTouch, { name: "v-touch" });
Vue.use(VueTheMask);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(vueScrollBehavior, {
  router: router, // The router instance
  ignore: [/\/.*/],
  delay: 0, // Delay by a number of milliseconds
});

Vue.mixin(errorToastMixin);
Vue.mixin(toastMixin);

Vue.config.productionTip = false;
Vue.component("apexchart", VueApexCharts);
ui.forEach((component) => {
  Vue.component(component.name, component);
});
moment.locale("ru");
Vue.prototype.moment = moment;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
