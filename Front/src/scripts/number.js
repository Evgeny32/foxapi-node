export function numberWithSpaces(num) {
  let number = num.toString();
  let text = "";
  let length = number.length;
  number.split("").forEach((symbol, i) => {
    (length - i) % 3 == 0 ? (text += ` ${symbol}`) : (text += symbol);
  });
  return text;
}
