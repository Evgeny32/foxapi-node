import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/package";

class PackageState {
  error = null;
}

class PackageGetters extends Getters {}

class PackageMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
}

class PackageActions extends Actions {
  async getAgentPackagesById(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/agent/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}
const pack = new Module({
  state: PackageState,
  getters: PackageGetters,
  mutations: PackageMutations,
  actions: PackageActions,
});
export const PackageMapper = createMapper(pack);
export default pack;
