import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/term";
// const media_url = process.env.VUE_APP_API_URL;

class TermState {
  mentorTerms = [];
  ageCategory = null;
  error = null;
}

class TermGetters extends Getters {}

class TermMutations extends Mutations {
  setMentorTerms(mentorTerms) {
    this.state.mentorTerms = mentorTerms;
  }
  setAgeCategory(ageCategory) {
    this.state.ageCategory = ageCategory;
  }
  setError(error) {
    this.state.error = error;
  }
}

class TermActions extends Actions {
  async getMentorTermsById(id) {
    this.mutations.setError(null);
    this.mutations.setMentorTerms([]);
    await Axios.get(baseUrl + "/mentor/" + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setMentorTerms(response.data.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildTermsById(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/child/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async postTerm(term) {
    this.mutations.setError(null);
    await Axios.post(baseUrl, term, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async patchTermById({ id, data }) {
    this.mutations.setError(null);
    await Axios.patch(baseUrl + "/" + id, data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
}
const term = new Module({
  state: TermState,
  getters: TermGetters,
  mutations: TermMutations,
  actions: TermActions,
});
export const TermMapper = createMapper(term);
export default term;
