import { Getters, Mutations, Actions, Module } from "vuex-smart-module";
import UserStore from "./user";
import ChildStore from "./child";
import CoachingStore from "./coaching";
import MentorStore from "./mentor";
import TermStore from "./term";
import GroupStore from "./group";
import TaleStore from "./tale";
import PackageStore from "./package";
import AgentStore from "./agent";
import SupportStore from "./support";
import FranchiseeStore from "./franchisee";
import AdminStore from "./admin";
import AdminBuchStore from "./adminBuch";
import FranchBuchStore from "./franchBuch";
import BranchStore from "./branch";
import ClientManagerStore from "./clientManager";
import HeadManagerStore from "./headManager";
import SalesManagerStore from "./salesManager";
import playerStore from "./player";

class RootState {}

class RootGetters extends Getters {}

class RootMutations extends Mutations {}

class RootActions extends Actions {}

export default new Module({
  state: RootState,
  getters: RootGetters,
  mutations: RootMutations,
  actions: RootActions,
  modules: {
    UserStore,
    ChildStore,
    CoachingStore,
    MentorStore,
    playerStore,
    TermStore,
    GroupStore,
    TaleStore,
    PackageStore,
    AgentStore,
    SupportStore,
    FranchiseeStore,
    AdminStore,
    AdminBuchStore,
    BranchStore,
    ClientManagerStore,
    HeadManagerStore,
    SalesManagerStore,
    FranchBuchStore,    
  },
});
