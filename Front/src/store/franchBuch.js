import {
    Getters,
    Mutations,
    Actions,
    Module,
    createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const baseUrl = process.env.VUE_APP_API_URL + "/api/v1/franch-buch/";
//   const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class FranchBuchState {
  
    children = [];
    childDetails = {};
    mentors = [];
    mentorDetails = {};
    error = null;
}

class FranchBuchGetters extends Getters { }

class FranchBuchMutations extends Mutations {


    setChildren(child) {
        this.state.children = child;
    }

    clearChildren() {
        this.state.children = [];
    }

    setChildDetails(child) {
        this.state.childDetails = child;
    }

    clearChildDetails() {
        this.state.childDetails = [];
    }

    setMentors(mentor) {
        this.state.mentors = mentor;
    }

    clearMentors() {
        this.state.mentors = [];
    }

    setMentorDetails(mentor) {
        this.state.mentorDetails = mentor;
    }

    clearMentorDetails() {
        this.state.mentorDetails = {};
    }

    setError(error) {
        this.state.error = error;
    }
    clearError() {
        this.state.error = null;
    }
}

class FranchBuchActions extends Actions {


    async getChildrenList() {
        this.mutations.clearError();
        this.mutations.clearChildren();
        await Axios.get(baseUrl + "children/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setChildren(response.data))
            .catch(() => this.mutations.setError(true));
    }


    async getChildDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearChildDetails();
        await Axios.get(
            baseUrl + "child/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setChildDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }



    async getMentors() {
        this.mutations.clearError();
        this.mutations.clearMentors();
        await Axios.get(baseUrl + "mentors/", {
            headers: {
                Authorization: "Token " + store.getters["AuthStore/token"],
            },
        })
            .then((response) => this.mutations.setMentors(response.data))
            .catch(() => this.mutations.setError(true));
    }

    async getMentorDetails(id) {
        // * Получение подробностей об одном менторе
        this.mutations.clearError();
        this.mutations.clearMentorDetails();
        await Axios.get(
            baseUrl + "mentor/" + id.toString(),

            {
                headers: {
                    Authorization: "Token " + store.getters["AuthStore/token"],
                },
            }
        )
            .then((response) => this.mutations.setMentorDetails(response.data))
            .catch((error) => this.mutations.setError(error.response.data));
    }

}

const franchBuch = new Module({
    getters: FranchBuchGetters,
    state: FranchBuchState,
    mutations: FranchBuchMutations,
    actions: FranchBuchActions,
});
export const FranchBuchMapper = createMapper(franchBuch);
export default franchBuch;
