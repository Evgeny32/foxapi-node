import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/group";
// const media_url = process.env.VUE_APP_API_URL;

class GroupState {
  mentorGroups = [];
  error = null;
}

class GroupGetters extends Getters {}

class GroupMutations extends Mutations {
  setMentorGroups(mentorGroups) {
    this.state.mentorGroups = mentorGroups;
  }
  setError(error) {
    this.state.error = error;
  }
}

class GroupActions extends Actions {
  async getMentorGroupsById(id) {
    this.mutations.setError(null);
    this.mutations.setMentorGroups([]);
    await Axios.get(baseUrl + "/mentor/" + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setMentorGroups(response.data.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async postGroup(group) {
    this.mutations.setError(null);
    await Axios.post(baseUrl, group, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async getGroupById(id) {
    this.mutations.setError(null);
    try {
      const response = await Axios.get(baseUrl + "/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async patchGroupById({ id, data }) {
    this.mutations.setError(null);
    try {
      const response = await Axios.patch(baseUrl + "/" + id, data, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}
const group = new Module({
  state: GroupState,
  getters: GroupGetters,
  mutations: GroupMutations,
  actions: GroupActions,
});
export const GroupMapper = createMapper(group);
export default group;
