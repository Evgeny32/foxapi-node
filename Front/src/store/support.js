import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/support";

class SupportState {
  error = null;
}

class SupportGetters extends Getters { }

class SupportMutations extends Mutations {
  setError(error) {
    this.state.error = error;
  }
}

class SupportActions extends Actions {
  async getMySupportMessages() {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async postMessage(message) {
    this.mutations.setError(null);
    await Axios.post(baseUrl, message, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }

  async getAdminSupport() {
    this.mutations.setError(null);

    try {
      let response = await Axios.get(baseUrl + "/admin", {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }


  }
  async sendAdminSupport({ id, data }) {
    this.mutations.setError(null);

    try {
      let response = await Axios.post(`${baseUrl}/admin/${id}`,
        { ...data }, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }

  }

  async getAdminSupportTheme(id) { // * Получение подробностей об одном филиале

    this.mutations.setError(null);

    try {
      let response = await Axios.get(`${baseUrl}/admin/${id}`,
         {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }

  }

}
const support = new Module({
  state: SupportState,
  getters: SupportGetters,
  mutations: SupportMutations,
  actions: SupportActions,
});
export const SupportMapper = createMapper(support);
export default support;
