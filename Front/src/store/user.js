import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl =
  (process.env.VUE_APP_API_URL || "http://localhost:5000/") + "api/v1/user";
// const media_url = process.env.VUE_APP_API_URL;

class UserState {
  token = null;
  me = {};
  error = null;
}

class UserGetters extends Getters {
  get token() {
    return this.state.token;
  }
  get role() {
    return this.state.me.role;
  }
}

class UserMutations extends Mutations {
  setToken(token) {
    this.state.token = token;
  }
  setMe(me) {
    if (me && me.img) me.img = `${me.img}?cacheBust=${new Date().getTime()}`;
    this.state.me = me;
  }
  setError(error) {
    this.state.error = error;
  }
}

class UserActions extends Actions {
  // Регистрация
  async signUp(payload) {
    this.mutations.setError(null);
    await Axios.post(baseUrl + "/signup", { ...payload })
      .then((response) => this.mutations.setToken(response.data.token))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  //   Отправка кода
  async sendCode(payload) {
    this.mutations.setError(null);
    await Axios.post(baseUrl + "/code", payload).catch((error) =>
      this.mutations.setError(error.response.data)
    );
  }
  //   Логин
  async signIn(payload) {
    this.mutations.setError(null);
    await Axios.post(baseUrl + "/login", payload)
      .then((response) => this.mutations.setToken(response.data.token))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  //   Получение данных пользователя
  async getMe() {
    this.mutations.setError(null);
    await Axios.get(baseUrl + "/me", {
      headers: {
        token: this.state.token,
      },
    })
      .then((response) => this.mutations.setMe(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchMe(payload, isFormData) {
    let headers = {
      token: this.state.token,
    };
    isFormData ? (headers["Content-Type"] = "multipart/form-data") : null;
    this.mutations.setError(null);
    await Axios.patch(baseUrl + "/me", payload, { headers })
      .then((response) => this.mutations.setMe(response.data.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // Получение пользователя по id
  async getUserById(id) {
    this.mutations.setError(null);
    try {
      const response = await Axios.get(baseUrl + "/" + id, {
        headers: {
          token: this.state.token,
        },
      });
      if (response.status == 200) {
        return response.data;
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async patchUserById({ id, data }, isFormData) {
    let headers = {
      token: this.state.token,
    };
    isFormData ? (headers["Content-Type"] = "multipart/form-data") : null;
    this.mutations.setError(null);
    await Axios.patch(baseUrl + "/" + id, data, { headers }).catch((error) =>
      this.mutations.setError(error.response.data)
    );
  }
}
const user = new Module({
  state: UserState,
  getters: UserGetters,
  mutations: UserMutations,
  actions: UserActions,
});
export const UserMapper = createMapper(user);
export default user;
