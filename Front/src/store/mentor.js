import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/mentor";
// const media_url = process.env.VUE_APP_API_URL;

class MentorState {
  mentor = null;
  error = null;
}

class MentorGetters extends Getters {}

class MentorMutations extends Mutations {
  setMentor(mentor) {
    this.state.mentor = mentor;
  }
  setError(error) {
    this.state.error = error;
  }
}

class MentorActions extends Actions {
  async getMentorById(id) {
    this.mutations.setError(null);
    this.mutations.setMentor(null);
    await Axios.get(baseUrl + "/" + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setMentor(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
}
const mentor = new Module({
  state: MentorState,
  getters: MentorGetters,
  mutations: MentorMutations,
  actions: MentorActions,
});
export const MentorMapper = createMapper(mentor);
export default mentor;
