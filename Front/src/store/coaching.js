import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/coaching";
// const media_url = process.env.VUE_APP_API_URL;

class CoachingState {
  parentFreeCoachings = [];
  parentCoachings = [];
  error = null;
}

class CoachingGetters extends Getters {}

class CoachingMutations extends Mutations {
  setParentFreeCoachings(parentFreeCoachings) {
    this.state.parentFreeCoachings = parentFreeCoachings;
  }
  setParentCoachings(parentCoachings) {
    this.state.parentCoachings = parentCoachings;
  }
  setError(error) {
    this.state.error = error;
  }
}

class CoachingActions extends Actions {
  async getParentFreeCoachings() {
    this.mutations.setError(null);
    this.mutations.setParentFreeCoachings([]);
    await Axios.get(baseUrl + "/parent/free", {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) =>
        this.mutations.setParentFreeCoachings(response.data.data)
      )
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getParentCoachings() {
    this.mutations.setError(null);
    this.mutations.setParentCoachings([]);
    await Axios.get(baseUrl + "/parent/my", {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setParentCoachings(response.data.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async signUpForCoaching(coachingId) {
    this.mutations.setError(null);
    await Axios.post(
      `${baseUrl}/${coachingId}/signup`,
      {},
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }
}
const coaching = new Module({
  state: CoachingState,
  getters: CoachingGetters,
  mutations: CoachingMutations,
  actions: CoachingActions,
});
export const CoachingMapper = createMapper(coaching);
export default coaching;
