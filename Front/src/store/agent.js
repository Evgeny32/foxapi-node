import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/agent";
// const media_url = process.env.VUE_APP_API_URL;

// class AgentState {
//   agentMe = null;
//   error = null;
// }

// class AgentGetters extends Getters {}

// class AgentMutations extends Mutations {
// setAgentMe(agentMe) {
//   if (agentMe && agentMe.user.img)
//     agentMe.user.img = `${
//       agentMe.user.img
//     }?cacheBust=${new Date().getTime()}`;
//   this.state.agentMe = agentMe;
// }
// setError(error) {
//   this.state.error = error;
// }
// }

class AgentState {
  agentProfile = { user: {} };
  error = null;
  packages = [];
}

class AgentGetters extends Getters {}

class AgentMutations extends Mutations {
  clearAgentProfile() {
    this.state.agentProfile = { user: {} };
  }
  setAgentMe(agentMe) {
    if (agentMe && agentMe.user.img)
      agentMe.user.img = `${
        agentMe.user.img
      }?cacheBust=${new Date().getTime()}`;
    this.state.agentMe = agentMe;
  }

  setAgentProfile(profile) {
    // profile.passportPDF = mediaUrl + profile.passportPDF;
    // profile.snilsPDF = mediaUrl + profile.snilsPDF;
    // profile.innPDF = mediaUrl + profile.innPDF;
    // profile.resumePDF = mediaUrl + profile.resumePDF;
    this.state.agentProfile = profile;
  }
  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }
  setPackages(packages) {
    this.state.packages = packages;
  }
  clearPackages() {
    this.state.packages = [];
  }
}

class AgentActions extends Actions {
  async getAgentMe() {
    this.mutations.setError(null);
    this.mutations.setAgentMe(null);
    try {
      let response = await Axios.get(baseUrl + "/me", {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      this.mutations.setAgentMe(response.data);
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async postAgentMe(data) {
    this.mutations.setError(null);
    try {
      await Axios.post(baseUrl + "/me", data, {
        headers: {
          token: store.getters["UserStore/token"],
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getOtherAgentProfile(id) {
    this.mutations.clearError();
    this.mutations.clearAgentProfile();
    await Axios.get(process.env.VUE_APP_API_URL + "api/v1/agentProfile/" + id, {
      headers: {
        token: store.getters["UserStore/token"],
        //"Content-Type": "multipart/form-data",
      },
    })

      .then((response) => this.mutations.setAgentProfile(response.data))
      .catch(() => this.mutations.setError(true));
  }

  async getPackages() {
    this.mutations.clearError();
    this.mutations.clearPackages();
    await Axios.get(baseUrl + "packages", {
      headers: {
        Authorization: "Token " + store.getters["AuthStore/token"],
      },
    })
      .then((response) => this.mutations.setPackages(response.data))
      .catch(() => this.mutations.setError(true));
  }
}
const agent = new Module({
  state: AgentState,
  getters: AgentGetters,
  mutations: AgentMutations,
  actions: AgentActions,
});
export const AgentMapper = createMapper(agent);
export default agent;
