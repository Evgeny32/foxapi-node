import {
    Getters,
    Mutations,
    Actions,
    Module,
    createMapper,
} from "vuex-smart-module";

class PlayerState {
    audio = new Audio()
    isPlaying = false
    currentTime = 0
    currentSeconds = 0
    rightAudio = new Audio()
}

class PlayerGetters extends Getters {
}

class PlayerMutations extends Mutations {

    setAudio(audio) {
        this.state.audio = audio;
        this.state.audio.ontimeupdate = () => {
            this.state.currentTime = this.state.audio.currentTime * (100 / this.state.audio.duration)
            this.state.currentSeconds = this.state.audio.currentTime
        }
    }
    clearAudio() {
        this.state.audio = null;
    }
    setIsPlaying(state) {
        this.state.isPlaying = state;
    }
    setRightAudio(audio) {
        this.state.rightAudio = audio;
    }
    clearAudioCurrentTime() {
        this.state.audio.currentTime = 0;
        this.state.currentTime = 0;
    }
    rewindAudio(seconds) {
        this.state.audio.currentTime += seconds;
    }

}

class PlayerActions extends Actions {
    stopAllAudio() {
        try { this.state.audio.pause() }
        catch { null }
        try { this.state.rightAudio.pause() }
        catch { null }
    }
    async playTopAudio() {
        await this.actions.stopAllAudio();
        this.state.audio.play();
    }
    async playRightAudio() {
        await this.actions.stopAllAudio();
        this.state.rightAudio.play();
    }
}

const player = new Module({
    getters: PlayerGetters,
    state: PlayerState,
    mutations: PlayerMutations,
    actions: PlayerActions,
});
export const PlayerMapper = createMapper(player);
export default player;
