import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/child";
// const media_url = process.env.VUE_APP_API_URL;

class ChildState {
  parentChildren = [];
  childMe = null;
  error = null;
  profile = {};
  kid = {};
}

class ChildGetters extends Getters {}

class ChildMutations extends Mutations {
  setParentChildren(parentChildren) {
    this.state.parentChildren = parentChildren;
  }
  setChildMe(childMe) {
    if (childMe && childMe.child.img)
      childMe.child.img = `${
        childMe.child.img
      }?cacheBust=${new Date().getTime()}`;
    this.state.childMe = childMe;
  }
  setError(error) {
    this.state.error = error;
  }

  setChildProfile(profile) {
    this.state.profile = profile;
  }

  setChild(kid) {
    this.state.kid = kid;
  }

  clearChild() {
    this.state.kid = {};
  }
}

class ChildActions extends Actions {
  async getParentChildren() {
    this.mutations.setError(null);
    this.mutations.setParentChildren([]);
    await Axios.get(baseUrl + "/parent", {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setParentChildren(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildById(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }


  async getChild(id) {
    this.mutations.clearError();
    this.mutations.clearChild();
    try {
      const response = await Axios.get(baseUrl + "/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      if (response.status == 200) {
        this.mutations.setChild(response.data);
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }



  async getChildUserById(id) {
    this.mutations.setError(null);

    try {
      let response = await Axios.get(baseUrl + "/user/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      console.log(response.data.id);
      return response.data.id;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  // async getChildUserById(id) {
  //   this.mutations.setError(null);
  //   //this.mutations.setChildProfile({});
  //   await Axios.get(baseUrl + "/user/" + id, {
  //     headers: {
  //       token: store.getters["UserStore/token"],
  //     },
  //   })
  //     .then((response) => this.mutations.setChildProfile(response.data))
  //     .catch((error) => this.mutations.setError(error.response.data));
  // }

  async patchChildById({ id, data }) {
    this.mutations.setError(null);
    await Axios.patch(baseUrl + "/" + id, data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }

  async postChild(data) {
    this.mutations.setError(null);
    await Axios.post(baseUrl + "/me", data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildMe() {
    this.mutations.setError(null);
    this.mutations.setChildMe(null);
    try {
      let response = await Axios.get(baseUrl + "/me", {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      this.mutations.setChildMe(response.data);
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
}
const child = new Module({
  state: ChildState,
  getters: ChildGetters,
  mutations: ChildMutations,
  actions: ChildActions,
});
export const ChildMapper = createMapper(child);
export default child;
