import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

Axios.defaults.headers.post["Content-Type"] = "application/json";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/tale";
//const mediaUrl = process.env.VUE_APP_API_URL;
// const media_url = process.env.VUE_APP_API_URL;

class TaleState {
  childTales = [];
  error = null;
  tales = [];
  tale = {};
  tasks = [];
}

class TaleGetters extends Getters {}

class TaleMutations extends Mutations {
  setChildTales(childTales) {
    this.state.childTales = childTales;
  }
  setError(error) {
    this.state.error = error;
  }

  setTale(tale) {
    // tale.tasks.forEach((task) => {
    //   task.answers.forEach((answer) => {
    //     if (answer.answer) {
    //       answer.answer = mediaUrl + answer.answer;
    //     }
    //   });
    // });
    this.state.tale = tale;
  }
  clearTale() {
    this.state.tale = {};
  }
  // setTask(tale) {

  //   this.state.tale = tale;
  // }
  // clearTask() {
  //   this.state.tale = {};
  // }
  clearError() {
    this.state.error = null;
  }
}

class TaleActions extends Actions {
  async postTale({ childId, taleNumber }) {
    this.mutations.setError(null);
    await Axios.post(
      `${baseUrl}/${taleNumber}/${childId}`,
      {},
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }
  async getChildTalesById(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/child/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getTasks(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/tasks/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  async getAnswers(id) {
    this.mutations.setError(null);
    try {
      let response = await Axios.get(baseUrl + "/tasks/answer/" + id, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      return response.data;
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }
  async getTale(id) {
    this.mutations.clearError();
    this.mutations.clearTale();
    await Axios.get(baseUrl + "/tale/" + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setTale(response.data))
      .catch((error) => this.mutations.setError(error.data));
  }
  // async getTale(id) {
  //   this.mutations.setError(null);
  //   try {
  //     let response = await Axios.get(baseUrl + "/tale/" + id, {
  //       headers: {
  //         token: store.getters["UserStore/token"],
  //       },
  //     });
  //     return response.data;
  //   } catch (error) {
  //     this.mutations.setError(error.response.data);
  //   }
  // }
  async patchTale(data) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}/${this.state.tale.id}/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then(() => this.actions.getTale(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchTales(id) {
    this.mutations.setError(null);
    let payload = { status: "paid" };
    try {
      let response = await Axios.patch(baseUrl + "/" + id, payload, {
        headers: {
          token: store.getters["UserStore/token"],
        },
      });
      if (response.status == 200) {
        await this.actions.getChildTalesById(response.data.child);
        return response.data;
      }
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  async patchTask({ id, data }) {
    let taleid = this.state.tale.id;
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}/task/${id}/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then(() => this.actions.getTale(taleid))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchAnswer({ id, data }) {
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}/answer/${id}/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }

  async getNextTale(data) {
    this.mutations.clearError();
    await Axios.patch(`${baseUrl}/changetale/app`, data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then(() => this.actions.getChildTalesById(this.state.tale.id))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchFormAnswer({ id, data }) {
    this.mutations.clearError();
    console.log(data);
    await Axios.patch(`${baseUrl}/answer/${id}/`, data, {
      headers: {
        token: store.getters["UserStore/token"],

        "Content-Type": "multipart/form-data",
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async patchTaskByMentor(id) {
    this.mutations.clearError();
    let payload = { doneAnswer: true };
    await Axios.patch(`${baseUrl}/task/mentor/${id}/`, payload, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
}

const tale = new Module({
  state: TaleState,
  getters: TaleGetters,
  mutations: TaleMutations,
  actions: TaleActions,
});
export const TaleMapper = createMapper(tale);
export default tale;
