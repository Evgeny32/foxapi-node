import {
  Getters,
  Mutations,
  Actions,
  Module,
  createMapper,
} from "vuex-smart-module";
import Axios from "axios";
import { store } from "@/store";

const baseUrl = process.env.VUE_APP_API_URL + "api/v1/";
const mediaUrl = process.env.VUE_APP_API_URL;
// const mediaUrl = "http://localhost:8000";

// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
Axios.defaults.headers.post["Content-Type"] = "application/json";
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

class FranchiseeState {
  // * Филиалы
  branches = [];
  branch = {};
  // * Филиалы
  franchiseeProfile = {};
  franchBuch = {};
  allinfo = [];
  packageallinfo = {};
  mentors = [];
  mentor = {};
  mentorGroups = [];
  children = [];
  child = {};
  error = null;
  token = null;
  // * Пакеты
  packages = [];
  pack = {};
  // * Пакеты
}

class FranchiseeGetters extends Getters {
  get allChildCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.children).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].children
      : 0;
  }
  get allGroupCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.groups).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].groups
      : 0;
  }
  get allMentorCount() {
    return this.state.branches.length > 1
      ? this.state.branches.map((x) => x.mentors).reduce((x, y) => x + y)
      : this.state.branches.length
      ? this.state.branches[0].children
      : 0;
  }
  get allBranchCount() {
    return this.state.branches.length ? this.state.branches.length : 0;
  }
  get groupCount() {
    return this.state.mentors.length > 1
      ? this.state.mentors.map((m) => m.groups).reduce((x, y) => x + y)
      : this.state.mentors.length
      ? this.state.mentors[0].groups
      : 0;
  }
  get mentorCount() {
    return this.state.mentors && this.state.mentors.length
      ? this.state.mentors.length
      : 0;
  }
  get childCount() {
    return this.state.children && this.state.children.length
      ? this.state.children.length
      : 0;
  }

  get lastPackage() {
    return this.state.packages.length
      ? this.state.packages[this.state.packages.length - 1]
      : null;
  }
}

class FranchiseeMutations extends Mutations {
  // * Филиалы
  setBranches(branches) {
    this.state.branches = branches;
  }

  clearBranches() {
    this.state.branches = [];
  }

  setBranch(branch) {
    branch.registrationSertificatePDF =
      mediaUrl + branch.registrationSertificatePDF;
    branch.accountingSertificatePDF =
      mediaUrl + branch.accountingSertificatePDF;
    branch.partnerFormPDF = mediaUrl + branch.partnerFormPDF;

    this.state.branch = branch;
  }

  clearBranch() {
    this.state.branch = {};
  }
  // * Филиалы
  clearFranchiseeProfile() {
    this.state.franchiseeProfile = {};
  }
  setFranchiseeProfile(profile) {
    Object.entries(profile).forEach((i) => {
      if (i[0].indexOf("PDF") != -1 && i[1]) {
        profile[i[0]] = mediaUrl + profile[i[0]];
      }
    });

    this.state.franchiseeProfile = profile;
  }

  setFranchBuch(buch) {
    this.state.franchBuch = buch;
  }

  clearFranchBuch() {
    this.state.franchBuch = {};
  }

  setMentors(mentors) {
    this.state.mentors = mentors;
  }

  clearMentors() {
    this.state.mentors = [];
  }

  setMentor(mentor) {
    mentor.forEach((item) => {
      item.passport_file = mediaUrl + item.passport_file;
      item.snils_file = mediaUrl + item.snils_file;
      item.inn_file = mediaUrl + item.inn_file;
    });
    this.state.mentor = mentor;
  }

  clearMentor() {
    this.state.mentor = {};
  }

  setMentorGroups(mentorGroups) {
    this.state.mentorGroups = mentorGroups;
  }

  clearMentorGroups() {
    this.state.mentorGroups = [];
  }

  setChildren(children) {
    this.state.children = children;
  }

  clearChildren() {
    this.state.children = [];
  }

  setChild(child) {
    this.state.child = child;
  }

  clearChild() {
    this.state.child = {};
  }

  setError(error) {
    this.state.error = error;
  }
  clearError() {
    this.state.error = null;
  }

  // * Пакеты
  setPackages(packages) {
    this.state.packages = packages;
  }
  clearPackages() {
    this.state.packages = [];
  }
  setPack(pack) {
    this.state.pack = pack;
  }
  clearPack() {
    this.state.pack = [];
  }

  setAllInfo(allinfo) {
    this.state.allinfo = allinfo;
  }

  clearAllInfo() {
    this.state.allinfo = [];
  }

  setPackageAllInfo(packageallinfo) {
    this.state.packageallinfo = packageallinfo;
  }

  clearPackageAllInfo() {
    this.state.packageallinfo = {};
  }
  // * Пакеты
}

class FranchiseeActions extends Actions {
  // * Филиалы

  async getBranches() {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearBranches();
    await Axios.get(baseUrl + "franchiseeProfile/branch/", {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setBranches(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getBranchs() {
    // * Получение списка филиалов по токену
    this.mutations.clearError();
    this.mutations.clearBranches();
    await Axios.get(baseUrl + "admin/branches/", {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setBranches(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async postBranch(data) {
    // * Создание филиала
    this.mutations.clearError();
    await Axios.post(baseUrl + "franchiseeProfile/branch/", data, {
      headers: {
        token: store.getters["UserStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async postAdminBranch(data) {
    // * Создание филиала
    this.mutations.clearError();
    await Axios.post(baseUrl + "franchiseeProfile/branch/admin/", data, {
      headers: {
        token: store.getters["UserStore/token"],
        "Content-Type": "multipart/form-data",
      },
    })
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getBranch(id) {
    // * Получение подробностей об одном филиале

    this.mutations.clearError();
    this.mutations.clearBranch();
    await Axios.get(
      baseUrl + "branch/" + id.toString(),

      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setBranch(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async patchBranch({ id, data }) {
    // * Изменение филиала
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}franchiseeProfile/branch/${id.toString()}/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async banBranch({ id, data }) {
    // * Бан филиала
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}franchiseeProfile/branch/${id.toString()}/ban/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then(() => this.actions.getBranches())
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // * Филиалы

  // * Менторы
  async getMentors(id) {
    this.mutations.clearError();
    this.mutations.clearMentors(); // * Получение списка менторов
    await Axios.get(
      `${baseUrl}franchiseeProfile/branch/${id.toString()}/mentor/`,
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setMentors(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async banMentor({ id, data }) {
    // * Увольнение ментора
    this.mutations.clearError();
    await Axios.patch(
      `${baseUrl}franchiseeProfile/branch/mentor/${id.toString()}/ban/`,
      { ...data },
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }

  async getMentor(id) {
    // * Получение подробностей об одном менторе
    this.mutations.clearError();
    this.mutations.clearMentor();
    await Axios.get(
      `${baseUrl}franchiseeProfile/branch/mentor/${id.toString()}/`,
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    ).catch((error) => this.mutations.setError(error.response.data));
  }

  async getMentorGroups(id) {
    // * Получение групп ментора
    this.mutations.clearError();
    this.mutations.clearMentorGroups();
    await Axios.get(`${baseUrl}franchiseeProfile/branch/mentor/${id}/group/`, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setMentorGroups(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // * Менторы

  // * Дети
  async getChildren(id) {
    // * Получение списка детей
    this.mutations.clearError();
    this.mutations.clearChildren();
    await Axios.get(
      `${baseUrl}franchiseeProfile/branch/${id.toString()}/child/`,
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setChildren(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getChild(id) {
    // * Получение подробнной информации об одном ребенке
    this.mutations.clearError();
    this.mutations.clearChild();
    await Axios.get(
      `${baseUrl}franchiseeProfile/branch/child/${id.toString()}/`,
      {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }
    )
      .then((response) => this.mutations.setChild(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  // * Дети
  // * Франчайз
  async getFranchiseeProfile() {
    this.mutations.clearError();
    this.mutations.clearFranchiseeProfile();
    try {
      await Axios.get(baseUrl + "franchiseeProfile/", {
        headers: {
          token: store.getters["UserStore/token"],
        },
      }).then((response) => this.mutations.setFranchiseeProfile(response.data));
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  async putFormFranchiseeProfile(data) {
    this.mutations.clearError();

    await Axios.put(baseUrl + "franchiseeProfile/", data, {
      headers: {
        token: store.getters["UserStore/token"],
        "Content-Type": "multipart/form-data", //"boundary":"--------------------------847808104806312173277712"
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async putFranchiseeProfile(data) {
    this.mutations.setError(null);
    try {
      await Axios.put(baseUrl + "franchiseeProfile/", data, {
        headers: {
          token: store.getters["UserStore/token"],
          //"Content-Type": "multipart/form-data",
        },
      });
    } catch (error) {
      this.mutations.setError(error.response.data);
    }
  }

  async postFranchBuch(user) {
    this.mutations.clearError();
    this.mutations.clearFranchBuch();
    await Axios.post(`${baseUrl}franchiseeProfile/buch/`, user, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setFranchBuch(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async patchFranchBuch({ id, data }) {
    this.mutations.clearError();
    this.mutations.clearFranchBuch();
    await Axios.patch(`${baseUrl}franchiseeProfile/buch/${id}`, data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setFranchBuch(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  // * Франчайз

  // * Пакеты
  async getPackages() {
    // * Получение списка детей
    this.mutations.clearError();
    this.mutations.clearPackages();
    await Axios.get(`${baseUrl}FranchiseePackage/`, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }
  async getOtherPackages(id) {
    // * Получение списка детей
    this.mutations.clearError();
    this.mutations.clearPackages();
    await Axios.get(`${baseUrl}FranchiseePackage/package/other/${id}`, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setPackages(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getAllInfo() {
    // * Получение полного списка пакетов
    this.mutations.clearError();
    this.mutations.clearAllInfo();
    await Axios.get(`${baseUrl}franchiseeProfile/allinfo/`, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setAllInfo(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getPackageAllInfo(id) {
    // * Получение полную информацию о пакете
    this.mutations.clearError();
    this.mutations.clearPack();
    await Axios.get(`${baseUrl}franchiseeProfile/packageallinfo/` + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setPack(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async getPack(id) {
    // * Получение списка детей
    this.mutations.clearError();
    this.mutations.clearPack();
    await Axios.get(`${baseUrl}franchiseeProfile/package/` + id, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    })
      .then((response) => this.mutations.setPack(response.data))
      .catch((error) => this.mutations.setError(error.response.data));
  }

  async postPackage(data) {
    // * Создание филиала
    this.mutations.clearError();
    await Axios.post(`${baseUrl}franchiseePackage`, data, {
      headers: {
        token: store.getters["UserStore/token"],
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  async payForPackage({ id, data }) {
    this.mutations.setError(null);
    await Axios.post(`${baseUrl}franchiseeProfile/package/${id}/pay/`, data, {
      headers: {
        token: store.getters["UserStore/token"],

        "Content-Type": "multipart/form-data; charset=utf-8",
      },
    }).catch((error) => this.mutations.setError(error.response.data));
  }
  // * Пакеты
}

const franchisee = new Module({
  getters: FranchiseeGetters,
  state: FranchiseeState,
  mutations: FranchiseeMutations,
  actions: FranchiseeActions,
});
export const FranchiseeMapper = createMapper(franchisee);
export default franchisee;
