const swaggerAutogen = require("swagger-autogen")();

const outputFile = "./swagger_output.json";
//const endpointsFiles = ["./routes"];
const fs = require("fs");
const path = require("path");

const options = {
  disableWarnings: true,
};
const doc = {
  info: {
    ...require("../swagger/basicInfo"),
  },
  host: "localhost:5000",
  basePath: "/api/v1",
  tags: require("../swagger/tags"),
  securityDefinitions: {
    api_key: {
      type: "apiKey",
      name: "api_key",
      in: "header",
    },
    petstore_auth: {
      type: "oauth2",
      authorizationUrl: "https://petstore.swagger.io/oauth/authorize",
      flow: "implicit",
      scopes: {
        read_pets: "read your pets",
        write_pets: "modify pets in your account",
      },
    },
  },
  components: {},
};

const tab = fs
  .readdirSync(
    "/home/netherlander/Projects/fox-nodejs/foxapi-node/Server/routes/",
    { withFileTypes: true }
  )
  .filter((item) => !item.isDirectory())
  .map((item) => item.name);
let endpointsFiles = [];
for (let fil of tab) {
  endpointsFiles.push(`./routes/${fil}`);
  //swaggerAutogen(outputFile, endpointsFiles);
}
// console.log(endpointsFiles);
// endpointsFiles = [
//   `./routes/achievements`,
//   `./routes/admin`,
//   `./routes/adminBuch`,
//   `./routes/adminCRM`,
//   // `./routes/adminCalcData`,
//   `./routes/agent`,
//   // `./routes/agentProfile`,
//   `./routes/branch`,
//   // `./routes/branchProfile`,
//   `./routes/certificates`,
//   // `./routes/child`,
//   // `./routes/childGroup`,
//   // `./routes/childProfile`,
//   `./routes/clientManager`,
//   `./routes/cloud`,
//   // `./routes/coaching`,
//   `./routes/documentation`,
//   `./routes/franchBuch`,
//   // `./routes/franchiseePackage`,
//   // `./routes/franchiseeProfile`,
//   `./routes/group`,
//   `./routes/headSalesManager`,
//   `./routes/index`,
//   `./routes/integration`,
//   // `./routes/mentor`,
//   // `./routes/mentorProfile`,
//   // `./routes/notifications`,
//   `./routes/package`,
//   `./routes/salesManager`,
//   `./routes/stickers`,
//   // `./routes/support`,
//   `./routes/tale`,
//   `./routes/term`,
//   `./routes/transactions`,
//   `./routes/user`,
// ];
swaggerAutogen(outputFile, endpointsFiles, doc);
// let realPath = fs.openSync(
//   path.join(
//     "/home/netherlander/Projects/fox-nodejs/foxapi-node/Server/routes/index.js"
//   )
// );
// endpointsFiles = realPath.name;
// console.log(endpointsFiles);
