const express = require("express");
const router = express.Router();

const controller = require("../controllers/mentorProfile");


// Создание профиля ментора
router.post(
    "",
    controller.mentorProfilePostMiddleware,
    controller.mentorProfilePostFunction
);


// Информация о менторе по токену
router.get(
    "",
    controller.mentorProfileGetMiddleware,
    controller.mentorProfileGetFunction
);


// Изменение информации о менторе по токену
router.patch(
    "",
    controller.mentorProfilePatchMiddleware,
    controller.mentorProfilePatchFunction
);

// Информация о группах ментора по id
router.get(
    "/groups",
    controller.mentorGroupsGetMiddleware,
    controller.mentorGroupsGetFunction
);


// Информация о менторе по id
router.get(
    "/:id",
    controller.mentorProfileGetDetailMiddleware,
    controller.mentorProfileGetDetailFunction
);


// Изменение информации о менторе по id
router.patch(
    "/:id",
    controller.mentorProfilePatchDetailMiddleware,
    controller.mentorProfilePatchDetailFunction
);




module.exports = router;
