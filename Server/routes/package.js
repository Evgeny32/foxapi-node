const express = require("express");
const router = express.Router();

const controller = require("../controllers/package");

// Отображение тренингов, доступных и еще не взятых родителем
router.get(
  "/agent/:id",
  controller.agentPackagesByIdGetMiddleware,
  controller.agentPackagesByIdGetFunction
);

module.exports = router;
