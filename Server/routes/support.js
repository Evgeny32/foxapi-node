const express = require("express");
const router = express.Router();

const controller = require("../controllers/support");

// Отображение сообщений по токену
router.get(
  "",
  controller.supportMessagesGetMiddleware,
  controller.supportMessagesGetFunction
);

// Отправка сообщений по токену
router.post(
  "",
  controller.supportMessagesPostMiddleware,
  controller.supportMessagesPostFunction
);

// Получении сообщении админом по токену
router.get(
  "/admin/",
  controller.adminSupportMessagesGetMiddleware,
  controller.adminSupportMessagesGetFunction
);

/// Получение тем сообщении админом по id
router.get(
  "/admin/:id",
  controller.adminSupportThemeGetMiddleware,
  controller.adminSupportThemeGetFunction
);

// Отправка тем сообщений админом по id
router.post(
  "/admin/:id/",
  controller.adminSupportThemesPostMiddleware,
  controller.adminSupportThemesPostFunction
);

module.exports = router;
