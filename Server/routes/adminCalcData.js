const express = require("express");
const router = express.Router();

const controller = require("../controllers/adminCalcData");

// TODO Информация о чем?

router.get(
  "",
  controller.adminCalcDataGetMiddleware,
  controller.adminCalcDataGetFunction
);

// TODO Информация о чем?

router.patch(
  "",
  controller.adminCalcDataPatchMiddleware,
  controller.adminCalcDataPatchFunction
);

module.exports = router;
