const express = require("express");
const router = express.Router();

const controller = require("../controllers/branchProfile");


// Создание профиля филиала
router.post(
    "",
    controller.branchProfilePostMiddleware,
    controller.branchProfilePostFunction
);


// Информация о филиале по токену
router.get(
    "",
    controller.branchProfileGetMiddleware,
    controller.branchProfileGetFunction
);


// Изменение информации о филиале по токену
router.patch(
    "",
    controller.branchProfilePatchMiddleware,
    controller.branchProfilePatchFunction
);


// Информация о детей филиала по токену
router.get(
    "/children",
    controller.branchChildrenGetMiddleware,
    controller.branchChildrenGetFunction
);

// Информация об одного ребенка филиала по id
router.get(
    "/children/:id",
    controller.branchChildDetailGetMiddleware,
    controller.branchChildDetailGetFunction
);


// Информация о филиале по id
router.get(
    "/:id",
    controller.branchProfileDetailGetMiddleware,
    controller.branchProfileDetailGetFunction
);


// Изменение информации о филиале по id
router.patch(
    "/:id",
    controller.branchProfileDetailPatchMiddleware,
    controller.branchProfileDetailPatchFunction
);


module.exports = router;
