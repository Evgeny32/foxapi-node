const express = require("express");
const router = express.Router();

const controller = require("../controllers/achievements");

router.post(
  "/init",
  controller.AchievementInitMiddleware,
  controller.AchievementInitFunction
);

router.post(
  "/me",
  controller.AchievementInitMiddleware,
  controller.AchievementInitPostFunction
);

router.get(
  "/receive",
  controller.AchievementPostMiddleware,
  controller.AchievementReceivedGetFunction
);
router.post(
  "/receive",
  controller.AchievementPostMiddleware,
  controller.AchievementPostFunction
);
router.post(
  "/:id",
  controller.AchievementAchievedMiddleware,
  controller.AchievementAchievedGetFunction
);
router.delete(
  "/:id",
  controller.AchievementUnachievedMiddleware,
  controller.AchievementUnachievedGetFunction
);

module.exports = router;
