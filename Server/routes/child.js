const express = require("express");
const router = express.Router();

const controller = require("../controllers/child");

// Отображение детей родителя
router.get(
  "/parent",
  controller.parentChildGetMiddleware,
  controller.parentChildGetFunction
);

// Отображение профиля ребенка для ребенка
router.get(
  "/me",
  controller.childMeGetMiddleware,
  controller.childMeGetFunction
);

// Регистрация ребенка
router.post(
  "/me",
  controller.childProfilePostMiddleware,
  controller.childProfilePostFunction
);

// Отображение ребенка по id
router.get(
  "/:id",
  controller.childProfileGetMiddleware,
  controller.childProfileGetFunction
);
router.get(
  "/user/:id",
  controller.childUserGetMiddleware,
  controller.childUseGetFunction
);
// Изменение ребенка по id
router.patch(
  "/:id",
  controller.childProfilePatchMiddleware,
  controller.childProfilePatchFunction
);

module.exports = router;
