const express = require("express");
const router = express.Router();

const controller = require("../controllers/stickers");

router.get(
  "/pack",
  controller.stickerPacksGetMiddleware,
  controller.stickerPacksGetFunction
);

router.post(
  "/pack",
  controller.stickerPackPostMiddleware,
  controller.stickerPackPostFunction
);

router.get(
  "/me",
  controller.stickerGetMiddleware,
  controller.stickerGetFunction
);

router.get(
  "/:id",
  controller.stickerIdGetMiddleware,
  controller.stickerIdGetFunction
);
router.get(
  "/all",
  controller.stickerGetAllMiddleware,
  controller.stickerGetAllFunction
);

router.post(
  "/me",
  controller.stickerPostMiddleware,
  controller.stickerPostFunction
);

router.post(
  "receveid/:id",
  controller.receivedStickerPostMiddleware,
  controller.receivedStickerPostFunction
);
router.delete(
  "unreceveid/:id",
  controller.unreceivedStickerMiddleware,
  controller.unreceivedStickerFunction
);
module.exports = router;
