const express = require("express");
const router = express.Router();

const controller = require("../controllers/mentor");

// Отображение профиля ментора
router.get(
  "",
  controller.mentorProfileGetMiddleware,
  controller.mentorProfileGetFunction
);

// Создание, изменение профиля ментора
router.post(
  "",
  controller.mentorProfilePostMiddleware,
  controller.mentorProfilePostFunction
);

// Отображение профиля ментора по id
router.get(
  "/:id",
  controller.mentorProfileGetByIdMiddleware,
  controller.mentorProfileGetByIdFunction
);

module.exports = router;
