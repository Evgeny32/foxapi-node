const express = require("express");
const router = express.Router();

const controller = require("../controllers/franchBuch");


// Информация о детей через токен FranchBuch

router.get(
    "/children",
    controller.franchBuchChildrenGetMiddleware,
    controller.franchBuchChildrenGetFunction
);


// Детальная информация про одного ребенка по id

router.get(
    "/child/:id",
    controller.franchBuchChildrenDetailGetMiddleware,
    controller.franchBuchChildrenDetailGetFunction
);


// Полная информация о менторов через токен FranchBuch


router.get(
    "/mentors",
    controller.franchBuchMentorGetMiddleware,
    controller.franchBuchMentorGetFunction
);


// Детальная информация про одного ментора по id


router.get(
    "/mentor/:id",
    controller.franchBuchMentorDetailGetMiddleware,
    controller.franchBuchMentorDetailGetFunction
);


module.exports = router;
