const express = require("express");
const router = express.Router();

const controller = require("../controllers/adminCRM");

// Информация о пакетах токен admin

router.get(
  "/packages",
  controller.franchiseePackageListGetMiddleware,
  controller.franchiseePackageListGetFunction
);

// Информация о пакетах токен admin по id франчайза

router.get(
  "/packages/franchisee/:id",
  controller.franchiseeOtherPackageListGetMiddleware,
  controller.franchiseeOtherPackageListGetFunction
);

// Информация об общем числе агентов и франчайзи

router.get(
  "/count",
  controller.allCountGetMiddleware,
  controller.allCountGetFunction
);

// Информация об общем числе пакетов детей

router.get(
  "/child-package/",
  controller.allChildPackageGetMiddleware,
  controller.allChildPackageGetFunction
);

// Изменение пакета франчайза по id франчайза

router.patch(
  "/packages/:id",
  controller.franchiseePackagePatchMiddleware,
  controller.franchiseePackagePatchFunction
);

// Отправление документов пакета франчайза по email

router.patch(
  "/childpackages/:id",
  controller.franchiseePackageSendPatchMiddleware,
  controller.franchiseePackageSendPatchFunction
);

module.exports = router;
