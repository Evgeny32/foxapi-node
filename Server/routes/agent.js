const express = require("express");
const router = express.Router();

const controller = require("../controllers/agent");

// Отображение профиля агента
router.get(
  "/me",
  controller.agentProfileMeGetMiddleware,
  controller.agentProfileMeGetFunction
);

// Создание, изменение профиля агента
router.post(
  "/me",
  controller.agentProfilePostMeMiddleware,
  controller.agentProfilePostMeFunction
);

// Отображение пакетов, которые приобрели франчайзи, приглашенные агентом
router.get(
  "/packages",
  controller.agentProfileGetPackagesMiddleware,
  controller.agentProfileGetPackagesFunction
);

// // Отображение профиля агента по id
// router.get(
//   "/:id",
//   controller.agentProfileGetByIdMiddleware,
//   controller.agentProfileGetByIdFunction
// );

// // Создание, изменение профиля агента по id
// router.patch(
//   "/:id",
//   controller.agentProfilePostByIdMiddleware,
//   controller.agentProfilePostByIdFunction
// );

module.exports = router;
