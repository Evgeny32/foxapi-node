const express = require("express");
const router = express.Router();

const controller = require("../controllers/franchiseePackage");

// Создания пакета франчайза

router.post(
  "",

  controller.franchiseePackagePostMiddleware,
  controller.franchiseePackagePostFunction
);

// Информация о пакетах франчайза

router.get(
  "",

  controller.franchiseePackageListGetMiddleware,
  controller.franchiseePackageListGetFunction
);

module.exports = router;
