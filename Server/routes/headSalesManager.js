const express = require("express");
const router = express.Router();

const controller = require("../controllers/headSalesManager");

router.get(
  "/",
  controller.headSalesManagerProfileMeGetMiddleware,
  controller.headSalesManagerProfileMeGetFunction
);

// Создание, изменение профиля manger
router.post(
  "/",
  controller.headSalesManagerProfileMePosttMiddleware,
  controller.headSalesManagerProfileMePostFunction
);

router.get(
  "/manager/:id",
  controller.ManagerProfileilMiddleware,
  controller.ManagerProfileGetFunction
);

router.patch(
  "/selectmanager",
  controller.headSalesManagerSelectManagerMiddleware,
  controller.headSalesManagerSelectManagerFunction
);

router.get(
  "/franchisees",
  controller.FranchiseeProfileListMiddleware,
  controller.FranchiseeProfileListGetFunction
);

router.get(
  "/franchisee/:id",
  controller.FranchiseeProfileDetailMiddleware,
  controller.FranchiseeProfileDetailGetFunction
);

router.get(
  "/packages",
  controller.FranchiseePackageListMiddleware,
  controller.FranchiseePackageListViewGetFunction
);

router.get(
  "/agents",
  controller.AgentProfileListListMiddleware,
  controller.AgentProfileListGetFunction
);

router.get(
  "/agent/:id",
  controller.AgentProfileDetailMiddleware,
  controller.AgentProfileDetailGetFunction
);

router.get(
  "/parents",
  controller.ParentsProfileListMiddleware,
  controller.ParentsProfileListGetFunction
);

router.get(
  "/parent/:id",
  controller.ParentProfileDetailMiddleware,
  controller.ParentProfileDetailGetFunction
);

router.get(
  "/mentors",
  controller.MentorProfileListMiddleware,
  controller.MentorProfileListGetFunction
);

router.get(
  "/mentor/:id",
  controller.MentorProfileDetailMiddleware,
  controller.MentorProfileDetailGetFunction
);

module.exports = router;
