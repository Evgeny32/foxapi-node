const express = require("express");
const router = express.Router();

const controller = require("../controllers/childProfile");

// Регистрация ребенка
router.post(
  "",
  controller.childProfilePostMiddleware,
  controller.childProfilePostFunction
);


// Бан/разбан ребенка по id

router.patch(
  "/:id/ban",
  controller.childBanPatchMiddleware,
  controller.childBanPatchFunction
);

module.exports = router;

// Информация о сказках ребенка по id
router.post(
  "/:id/tales/",
  controller.childProfilePostMiddleware,
  controller.childProfilePostFunction
);

module.exports = router;


