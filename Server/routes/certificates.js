const express = require("express");
const router = express.Router();

const controller = require("../controllers/certificates");

router.get(
  "/me",
  controller.cerfictaesGetMiddleware,
  controller.certificatesFunction
);

router.post(
  "/me",
  controller.certificatesPostMiddleware,
  controller.certificatesPostFunction
);

router.get(
  "/:id",
  controller.cerfictaesGetOneMiddleware,
  controller.certificatesGetOneFunction
);

router.patch(
  "/:id",
  controller.certificateDetailPatchMiddleware,
  controller.certificateDetailPatchFunction
);
router.delete(
  "/:id",
  controller.certificateDetailDeleteMiddleware,
  controller.certificateDetailDeleteFunction
);

module.exports = router;
