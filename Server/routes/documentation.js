const router = require("express").Router();
const swaggerUI = require("swagger-ui-express");
// const YAML = require("yamljs");
// const swaggerDocument = YAML.load("./swagger.yaml");
const swaggerDocument = require("../swagger_output.json");
router.use("/", swaggerUI.serve, swaggerUI.setup(swaggerDocument));

module.exports = router;
