const express = require("express");
const router = express.Router();

const controller = require("../controllers/franchiseeProfile");
router.post(
  "/buch",
  controller.franchBuchAccountMiddleware,
  controller.franchBuchAccountFunction
);

router.patch(
  "/buch/:id",
  controller.FranchBuchAccountPatchViewMiddleware,
  controller.FranchBuchAccountPatchView
);
router.post(
  "/branch/admin/",
  controller.BranchAdminListViewPostMiddleware,
  controller.BranchAdminListViewPostFunction
);
router.get(
  "branch/:id",
  controller.BranchViewGetMiddleware,
  controller.BranchViewGetFunction
);
router.post(
  "/branch",
  controller.BranchListViewMiddleware,
  controller.BranchListViewFunction
);
router.get(
  "/branch",
  controller.BranchListViewGetMiddleware,
  controller.BranchListViewGetFunction
);

router.patch(
  "/branch/:id",
  controller.BranchViewPatchMiddleware,
  controller.BranchViewPatchFunction
);
// Создание профиля франчайзи
router.put(
  "",
  controller.franchiseeProfilePutMiddleware,
  controller.franchiseeProfilePutFunction
);

// Информация о франчайзи по токену
router.get(
  "",
  controller.franchiseeProfileGetMiddleware,
  controller.franchiseeProfileGetFunction
);

// Изменение информации о франчайзи по токену
router.put(
  "",
  controller.franchiseeProfilePatchMiddleware,
  controller.franchiseeProfilePatchFunction
);

// Информация о франчайзи по id
router.get(
  "/:id",
  controller.franchiseeProfileDetailGetMiddleware,
  controller.franchiseeProfileDetailGetFunction
);

// Изменение информации о франчайзи по id
router.patch(
  "/:id",
  controller.franchiseeProfileDetailPatchMiddleware,
  controller.franchiseeProfileDetailPatchFunction
);

router.get(
  "/branch/:id/mentor/",
  controller.MentorListViewGetMiddleware,
  controller.MentorListViewGetFunction
);
router.get(
  "branch/mentor/:id/",
  controller.MentorViewGetMiddleware,
  controller.MentorViewGetFunction
);

router.get(
  "/branch/mentor/:id/group/",
  controller.MentorGroupListViewMiddleware,
  controller.MentorGroupListViewGetFunction
);
router.get(
  "/branch/:id/child/",
  controller.BranchChildListViewMiddleware,
  controller.BranchChildListViewGetFunction
);

router.get(
  "/branch/child/:id/",
  controller.BranchChildViewMiddleware,
  controller.BranchChildViewGetFunction
);
router.get(
  "/allinfo/",
  controller.FranchiseeAllInfoViewMiddleware,
  controller.FranchiseeAllInfoViewFunction
);

router.get(
  "/packageallinfo/:id",
  controller.FranchiseeAllInfoDetailViewMiddleware,
  controller.FranchiseeAllInfoDetailViewFunction
);
router.get(
  "package/:id/",
  controller.FranchiseeOtherPackageListViewMiddleware,
  controller.FranchiseeOtherPackageListViewFunction
);
router.get(
  "/package/",
  controller.FranchiseePackageViewMiddleware,
  controller.FranchiseePackageViewFunction
);
router.post(
  "/package/:id/pay/",
  controller.FranchiseePackagePayViewMiddleware,
  controller.FranchiseePackagePayViewFunction
);
module.exports = router;
