const express = require("express");
const router = express.Router();

const controller = require("../controllers/clientManager");

router.get(
  "/me",
  controller.ClientManagerProfileMeGetMiddleware,
  controller.ClientManagerProfileMeGetFunction
);

router.post(
  "/me",
  controller.ClientManagerProfilePostMeMiddleware,
  controller.ClientManagerProfilePostMeFunction
);

router.patch(
  "/me",
  controller.ClientManagerProfilePatchMeMiddleware,
  controller.ClientManagerProfilePatchMeFunction
);

router.get(
  "/franchisees/:id/",
  controller.ClientManagerProfileFranchiseeMeMiddleware,
  controller.ClientManagerProfileFranchiseeGetFunction
);
router.get(
  "/:id/franchisees/",
  controller.FranchiseeByIdMiddleware,
  controller.FranchiseeByIdFunction
);
router.get(
  "/franchisee/:id/",
  controller.FranchiseeDetailMiddleware,
  controller.FranchiseeDetailGetFunction
);
router.patch(
  "/franchisee/:id/",
  controller.FranchiseeDetailMiddleware,
  controller.FranchiseeDetailGetFunction
);

router.get(
  "/franchisees/all/:id",
  controller.ClientManagerProfileReportMiddleware,
  controller.ClientManagerProfileReportFunction
);

router.get(
  "/franchisees/packages/:id/",
  controller.ClientManagerFranchiseePackageMeMiddleware,
  controller.ClientManagerFranchiseePackageGetFunction
);

router.get(
  "/agents/:id/",
  controller.ClientManagerAgentMiddleware,
  controller.ClientManagerAgentGetFunction
);
router.get(
  "/:id/agents",
  controller.AgentsByIdtMiddleware,
  controller.AgentsByIdGetFunction
);
router.get(
  "/agent/:id",
  controller.ClientManagerAgenDetailtMiddleware,
  controller.ClientManagerAgentGetDetailFunction
);

router.get(
  "/parents/:id",
  controller.ClientManagerParentMiddleware,
  controller.ClientManagerParentGetFunction
);

router.get(
  "/:id/parent/",
  controller.ParentsByIdMiddleware,
  controller.ParentsByIdGetFunction
);
router.get(
  "/parent/:id",
  controller.ParentDetailMiddleware,
  controller.ParentDetailGetFunction
);

router.get(
  "/mentors/:id",
  controller.ClientManagerMentorMiddleware,
  controller.ClientManagerAgentGetFunction
);
router.get(
  "/:id/mentors",
  controller.MentorsByIdMiddleware,
  controller.MentorsByIdGetFunction
);
router.get(
  "/mentor/:id",
  controller.MentorDetailMiddleware,
  controller.MentorDetailGetFunction
);
router.patch(
  "/mentor/:id",
  controller.MentorDetailPatchMiddleware,
  controller.MentorDetailPatchFunction
);

module.exports = router;
