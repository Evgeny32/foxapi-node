const express = require("express");
const router = express.Router();

const controller = require("../controllers/admin");

// Создание пользователя админа

router.post(
  "/users",

  controller.adminUserListPostMiddleware,
  controller.adminUserListPostFunction
);

// Информация о пользователях админа

router.get(
  "/users",
  controller.adminUserListGetMiddleware,
  controller.adminUserListGetFunction
);

// Информация о детей админа по токену

router.get(
  "/children",
  controller.adminChildrenListGetMiddleware,
  controller.adminChildrenListGetFunction
);

// Информация о детей админа по id

router.get(
  "/child/:id",
  controller.adminChildDetailGetMiddleware,
  controller.adminChildDetailGetFunction
);

// Информация о филиалов админа по токену

router.get(
  "/branches",
  controller.adminBranchListGetMiddleware,
  controller.adminBranchListGetFunction
);

// Информация о филиалов админа по id франчайза

router.get(
  "/branches/:id",
  controller.adminOtherBranchGetMiddleware,
  controller.adminOtherBranchGetFunction
);

// TODO не понятно что тут происходит

router.patch(
  "/branch/main/:id",
  controller.mainBranchPatchMiddleware,
  controller.mainBranchPatchMiddleware
);

router.get(
  // Информация о агентов по токену

  "/agents",
  controller.adminAgentsListGetMiddleware,
  controller.adminAgentsListGetFunction
);

router.get(
  // Информация о агенте по id

  "/agent/:id",
  controller.adminAgentDetailGetMiddleware,
  controller.adminAgentDetailGetFunction
);

router.patch(
  // Изменение информации об агенте

  "/agent/:id",
  controller.adminAgentPatchMiddleware,
  controller.adminAgentPatchFunction
);

router.get(
  "/franchisee",
  controller.adminFranchiseesListGetMiddleware,
  controller.adminFranchiseesListGetFunction
);

router.get(
  "/franchisee/:id",
  controller.adminFranchiseesDetailGetMiddleware,
  controller.adminFranchiseesDetailGetFunction
);
router.patch(
  "/franchisee/:id",
  controller.adminFranchiseePatchMiddleware,
  controller.adminFranchiseePatchFunction
);

router.get(
  "/package/:id/block/",
  controller.adminFranchiseePackageBlockGetMiddleware,
  controller.adminFranchiseePackageBlockGetFunction
);
router.patch(
  "/package/:id/block/",
  controller.adminFranchiseePackageBlockPatchMiddleware,
  controller.adminFranchiseePackageBlockPatchFunction
);

router.get(
  "/package/:id",
  controller.adminFranchiseePackageViewGetMiddleware,
  controller.adminFranchiseePackageViewGetFunction
);

module.exports = router;
