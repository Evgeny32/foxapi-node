const agent = require("./agent");
const agentProfile = require("./agentProfile");
const branch = require("./branch");
const child = require("./child");
const coaching = require("./coaching");
const group = require("./group");
const mentor = require("./mentor");
const tale = require("./tale");
const term = require("./term");
const user = require("./user");
const package = require("./package");
const support = require("./support");
const apiDocs = require("./documentation");
const admin = require("./admin");
const adminBuch = require("./adminBuch");
const adminCRM = require("./adminCRM");
const adminCalcData = require("./adminCalcData");
const achievements = require("./achievements");
const certificates = require("./certificates");
const clientManager = require("./clientManager");
const cloud = require("./cloud");
const headSalesManager = require("./headSalesManager");
const integration = require("./integration");
const notifications = require("./notifications");
const salesManager = require("./salesManager");
const stickers = require("./stickers");
const transactions = require("./transactions");
const franchiseeProfile = require("./franchiseeProfile");
const franchiseePackage = require("./franchiseePackage");
const mentorProfile = require("./mentorProfile");

module.exports = [
  {
    name: "admin",
    router: admin,
  },
  {
    name: "adminBuch",
    router: adminBuch,
  },
  {
    name: "adminCalcData",
    router: adminCalcData,
  },
  {
    name: "adminCRM",
    router: adminCRM,
  },
  {
    name: "achievements",
    router: achievements,
  },
  {
    name: "agent",
    router: agent,
  },
  {
    name: "agentProfile",
    router: agentProfile,
  },
  {
    name: "mentorProfile",
    router: mentorProfile,
  },
  {
    name: "branch",
    router: branch,
  },
  {
    name: "certificates",
    router: certificates,
  },
  {
    name: "clientManager",
    router: clientManager,
  },
  {
    name: "cloud",
    router: cloud,
  },
  {
    name: "headSalesManager",
    router: headSalesManager,
  },
  {
    name: "franchiseePackage",
    router: franchiseePackage,
  },
  {
    name: "franchiseeProfile",
    router: franchiseeProfile,
  },
  {
    name: "child",
    router: child,
  },
  {
    name: "coaching",
    router: coaching,
  },
  {
    name: "integration",
    router: integration,
  },
  {
    name: "mentor",
    router: mentor,
  },
  {
    name: "notifications",
    router: notifications,
  },
  {
    name: "salesManager",
    router: salesManager,
  },
  {
    name: "stickers",
    router: stickers,
  },
  {
    name: "tale",
    router: tale,
  },
  {
    name: "term",
    router: term,
  },
  {
    name: "user",
    router: user,
  },
  {
    name: "group",
    router: group,
  },
  {
    name: "package",
    router: package,
  },
  {
    name: "support",
    router: support,
  },
  {
    name: "transactions",
    router: transactions,
  },
  {
    name: "api-docs",
    router: apiDocs,
  },
];
