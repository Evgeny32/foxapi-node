const express = require("express");
const router = express.Router();

const controller = require("../controllers/childGroup");


// Создание группы детей
router.post(
    "",
    controller.childGroupPostMiddleware,
    controller.childGroupPostFunction
);


// Информация о группе детей
router.get(
    "",
    controller.childGroupGetMiddleware,
    controller.childGroupGetFunction
);


// Изменение информации о группе детей
router.patch(
    "/:id",
    controller.childGroupPatchMiddleware,
    controller.childGroupPatchFunction
);

module.exports = router;
