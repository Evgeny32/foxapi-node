const express = require("express");
const router = express.Router();

const controller = require("../controllers/branch");

// Отображение профиля филиала
// router.get(
//   "",
//   controller.branchProfileGetMiddleware,
//   controller.branchProfileGetFunction
// );

// Создание, изменение профиля филиала
router.get(
  "/",
  controller.branchProfileGetMiddleware,
  controller.branchProfileGetFunction
);
router.patch(
  "/",
  controller.branchProfilePatchMiddleware,
  controller.branchProfilePatchFunction
);
router.post(
  "/",
  controller.branchProfilePostMiddleware,
  controller.branchProfilePostFunction
);

// Отображение профиля филиала по id
router.get(
  "/:id/",
  controller.branchProfileGetByIdMiddleware,
  controller.branchProfileGetByIdFunction
);

router.post(
  ":id/",
  controller.branchProfilePostByIdMiddleware,
  controller.branchProfilePostByIdFunction
);
router.post(
  "/mentor/",
  controller.MentorListViewPostMiddleware,
  controller.MentorListViewPostFunction
);
router.get(
  "/mentor",
  controller.MentorListViewGetMiddleware,
  controller.MentorListViewGetFunction
);
router.get(
  "/mentor/:id/",
  controller.MentorViewGetMiddleware,
  controller.MentorViewGetFunction
);
router.patch(
  "/mentor/:id/",
  controller.MentorViewPatchMiddleware,
  controller.MentorViewPatchFunction
);
router.get(
  "/mentor/:id/group/",
  controller.MentorGroupListViewGetMiddleware,
  controller.MentorGroupListViewGetFunction
);

router.get(
  "/child/",
  controller.BranchChildListViewGetMiddleware,
  controller.BranchChildListViewGetFunction
);
router.get(
  "/child/:id/",
  controller.BranchChildViewGetMiddleware,
  controller.BranchChildViewGetFunction
);
router.get(
  "/child/:id/tale_log/",
  controller.ChildLogsViewGetMiddleware,
  controller.ChildLogsViewGetFunction
);
router.get(
  "/child/:id/tales/",
  controller.ChildTaleListViewGetMiddleware,
  controller.ChildTaleListViewGetFunction
);
router.get(
  "/child/tales/:id",
  controller.ChildTaleViewGetMiddleware,
  controller.ChildTaleViewGetFunction
);
router.get(
  "/child/ban/:id/",
  controller.BanChildViewPatchMiddleware,
  controller.BanChildViewPatchFunction
);
module.exports = router;
