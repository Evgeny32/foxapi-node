const express = require("express");
const router = express.Router();

const controller = require("../controllers/notifications");

router.get(
  "/me",
  controller.notificationGetMeMiddleware,
  controller.notificationGetMeFunction
);

router.post(
  "me",
  controller.notificationPostMiddleware,
  controller.notificationPostFunction
);

router.get(
  "/last",
  controller.lastNotificationGetMeMiddleware,
  controller.lastNotificationGetMeGetFunction
);

router.get(
  "/:id",
  controller.notificationGetIdMeMiddleware,
  controller.notificationGetIdFunction
);

router.patch(
  "/:id",
  controller.notificationPatchMiddleware,
  controller.notificationDetailPatchFunction
);
router.delete(
  "/:id",
  controller.notificationDeleteMiddleware,
  controller.notificationlDeleteFunction
);

module.exports = router;
