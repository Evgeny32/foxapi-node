const express = require("express");
const router = express.Router();

const controller = require("../controllers/agentProfile");


// Создание профиля агента
router.post(
    "",
    controller.agentProfilePostMiddleware,
    controller.agentProfilePostFunction
);


// Информация о агенте по токену
router.get(
    "",
    controller.agentProfileGetMiddleware,
    controller.agentProfileGetFunction
);


// Изменение информации о агенте по токену
router.patch(
    "",
    controller.agentProfilePatchMiddleware,
    controller.agentProfilePatchFunction
);


// Информация о агенте по id
router.get(
    "/:id",
    controller.agentProfileGetDetailMiddleware,
    controller.agentProfileGetDetailFunction
);


// Изменение информации о агенте по id
router.patch(
    "/:id",
    controller.agentProfilePatchDetailMiddleware,
    controller.agentProfilePatchDetailFunction
);

module.exports = router;
