const express = require("express");
const router = express.Router();
const controller = require("../controllers/integration");

router.get(
  "/organisation",
  controller.organisationMiddleware,
  controller.organisationGetFunction
);

router.get(
  "/partner",
  controller.partnerMiddleware,
  controller.partnerGetFunction
);

router.get(
  "/bankAccount",
  controller.bankAccountsMiddleware,
  controller.bankAccountsGetFunction
);
router.get(
  "/service",
  controller.ServicesMiddleware,
  controller.ServicesGetFunction
);
router.get(
  "/invoice",
  controller.invoicesMiddleware,
  controller.invoicesGetFunction
);
router.delete(
  "/realisation",
  controller.RealizationMiddleware,
  controller.RealizationGetFunction
);

router.get(
  "/receipt",
  controller.receiptsMiddleware,
  controller.receiptsGetFunction
);
router.post(
  "/payementOrder",
  controller.PaymentOrdersMiddleware,
  controller.PaymentOrdersGetFunction
);

router.get(
  "/salesReport",
  controller.SalesReportsMiddleware,
  controller.SalesReportsGetFunction
);

router.get(
  "/income",
  controller.IncomesMiddleware,
  controller.IncomesGetFunction
);
router.post(
  "/income",
  controller.IncomesPostMiddleware,
  controller.IncomesPostFunction
);

router.get(
  "payement",
  controller.PaymentsMiddleware,
  controller.PaymentsGetFunction
);

router.post(
  "/payement",
  controller.payementPostMiddleware,
  controller.payementPostFunction
);
router.get(
  "/catalog",
  controller.CatalogMiddleware,
  controller.CatalogGetFunction
);

router.get(
  "document",
  controller.DocumentsMiddleware,
  controller.DocumentsGetFunction
);

router.get(
  "/documentOutcome",
  controller.DocumentsOutcomeMiddleware,
  controller.DocumentsOutcomeFunction
);
router.post(
  "documentOutcome",
  controller.DocumentsOutPostcomeMiddleware,
  controller.DocumentsOutcomePostFunction
);

module.exports = router;
