const express = require("express");
const router = express.Router();

const controller = require("../controllers/group");

// Отображение групп ментора
router.get(
  "/mentor/:id",
  controller.mentorGroupsGetMiddleware,
  controller.mentorGroupsGetFunction
);

// Создание новой группы
router.post("", controller.groupsPostMiddleware, controller.groupsPostFunction);

// Отображение группы по id
router.get(
  "/:id",
  controller.groupGetByIdMiddleware,
  controller.groupGetByIdFunction
);

// Изменение группы по id
router.patch(
  "/:id",
  controller.groupPatchByIdMiddleware,
  controller.groupPatchByIdFunction
);

module.exports = router;
