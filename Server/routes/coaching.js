const express = require("express");
const router = express.Router();

const controller = require("../controllers/coaching");

// Создание нового тренинга ментором по токену
router.post(
  "",
  controller.coachingPostMiddleware,
  controller.coachingPostFunction
);

// Отображение тренингов, доступных и еще не взятых родителем
router.get(
  "/parent/free",
  controller.coachingParentFreeGetMiddleware,
  controller.coachingParentFreeGetFunction
);

// Отображение тренингов, взятых родителем
router.get(
  "/parent/my",
  controller.coachingParentMyGetMiddleware,
  controller.coachingParentMyGetFunction
);

// Записаться на тренинг по токену
router.post(
  "/:id/signup",
  controller.coachingSignUpPostMiddleware,
  controller.coachingSignUpPostFunction
);

module.exports = router;
