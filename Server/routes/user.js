const express = require("express");
const router = express.Router();

const controller = require("../controllers/user");

// Отправка кода на телефон
router.post(
  "/code",
  controller.codePostMiddleware,
  controller.codePostFunction
);

// Регистрация
router.post(
  "/signup",
  controller.signupPostMiddleware,
  controller.signupPostFunction
);

// Вход
router.post(
  "/login",
  controller.loginPostMiddleware,
  controller.loginPostFunction
);

// Информация о пользователе по токену
router.get("/me", controller.meGetMiddleware, controller.meGetFunction);

// Изменение информации о пользователе по токену
router.patch("/me", controller.mePatchMiddleware, controller.mePatchFunction);

// Информация о пользователе по id
router.get("/:id", controller.userGetMiddleware, controller.userGetFunction);

// Изменение информации о пользователе по id
router.patch(
  "/:id",
  controller.userPatchMiddleware,
  controller.userPatchFunction
);

module.exports = router;
