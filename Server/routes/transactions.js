const express = require("express");
const router = express.Router();

const controller = require("../controllers/transactions");

router.get(
  "/",
  controller.transactionGetMiddleware,
  controller.transactionsGetFunction
);

router.post(
  "/",
  controller.transactionsPostMiddleware,
  controller.transactionsPostFunction
);

router.get(
  "/:id",
  controller.transactionsGetOneMiddleware,
  controller.transactionsGetOneFunction
);

router.patch(
  "/:id",
  controller.certificateDetailPatchMiddleware,
  controller.certificateDetailPatchFunction
);

router.delete(
  "/:id",
  controller.transactionsDetailDeleteMiddleware,
  controller.transactionsDetailDeleteFunction
);
router.patch(
  "/fill/:id",
  controller.transactionsPathMiddleware,
  controller.transactionsPathFunction
);
router.post(
  "/pay",
  controller.transactionsPayMiddleware,
  controller.transactionsPayFunction
);

module.exports = router;
