const express = require("express");
const router = express.Router();
const controller = require("../controllers/cloud");

router.get(
  "/setting",
  controller.CloudSettingsGetMiddleware,
  controller.CloudSettingsGetFunction
);

router.post(
  "/setting",
  controller.CloudSettingsPatchMiddleware,
  controller.CloudSettingslPatchFunction
);

router.get(
  "/:id",
  controller.userCloudGetMiddleware,
  controller.userCloudGetFunction
);
router.post(
  "/:id",
  controller.CloudPostMiddleware,
  controller.CloudPostFunction
);
router.patch(
  "/:id",
  controller.CloudPatchMiddleware,
  controller.CloudPatchFunction
);
router.delete(
  "/:id",
  controller.CloudDeleteMiddleware,
  controller.CloudDeleteFunction
);

router.get(
  "/token",
  controller.CloudTokenGetMiddleware,
  controller.CloudTokenGetFunction
);
router.post(
  "/token",
  controller.CloudTokenPostMiddleware,
  controller.CloudTokenPostFunction
);

router.patch(
  "/token",
  controller.CloudTokenPatchMiddleware,
  controller.CloudTokenPatchFunction
);

router.delete(
  "/token",
  controller.CloudTokenDeleteMiddleware,
  controller.CloudTokenDeleteFunction
);
router.post(
  "/info",
  controller.CloudInfoGetMiddleware,
  controller.CloudInfoGetFunction
);

router.get(
  "info/:id",
  controller.CloudInfoGetByIdMiddleware,
  controller.CloudInfoGetByIdFunction
);

router.get(
  "/file",
  controller.CloudFileTokenMiddleware,
  controller.CloudFileTokenFunction
);
router.post(
  "/file",
  controller.cloudFilesPostMiddleware,
  controller.cloudFilesFunction
);

router.get(
  "file/:id",
  controller.CloudFilesGetMiddleware,
  controller.CloudFilesGetFunction
);

router.post(
  "/file/:id",
  controller.cloudFilesPostMiddleware,
  controller.cloudFilesFunction
);
router.get(
  "/file/all",
  controller.CloudFileGetMiddleware,
  controller.CloudFileGetFunction
);

router.delete(
  "/file",
  controller.CloudFileDeleteMiddleware,
  controller.CloudFileDeleteFunction
);
module.exports = router;
