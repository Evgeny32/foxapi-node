const express = require("express");
const router = express.Router();

const controller = require("../controllers/tale");

// Отображение сказок ребенка
router.get(
  "/child/:id",
  controller.childTalesGetMiddleware,
  controller.childTalesGetFunction
);
router.get("", controller.TalesGetMiddleware, controller.TalesGetFunction);

router.post(
  "/:taleNumber/:childId/",
  controller.TaleCreatePostMiddleware,
  controller.TaleCreatePostFunction
);
router.get("/:id", controller.TaleGetMiddleware, controller.TaleGetFunction);
router.get(
  "/tale/:id",
  controller.TaleGetViewMiddleware,
  controller.TaleGetViewFunction
);

router.post("/:id", controller.TalePostMiddleware, controller.TalePostFunction);
router.delete(
  "/:id",
  controller.TaleDeleteMiddleware,
  controller.TaleDeleteFunction
);
router.patch(
  "/:id",
  controller.TalePatchMiddleware,
  controller.TalePatchFunction
);
router.get(
  "/tasks/:id",
  controller.getTaksMiddleware,
  controller.getTasksFunction
);
router.get(
  "/task/:id",
  controller.TaskGetMiddleware,
  controller.TaskGetFunction
);
router.patch(
  "/task/:id",
  controller.TaskPatchMiddleware,
  controller.TaskPatchFunction
);

router.get(
  "/answer/:id",
  controller.AnswerGetMiddleware,
  controller.AnswerGetFunction
);
router.get(
  "/tasks/answer/:id",
  controller.AnswerTaskGetMiddleware,
  controller.AnswerTaskGetFunction
);
router.patch(
  "/answer/:id",
  controller.AnswerPatchMiddleware,
  controller.AnswerPatchFunction
);

router.patch(
  "/changetale/app",
  controller.TaleChangeViewMiddleware,
  controller.TaleChangeViewFunction
);
// проверка задач мантором
router.patch(
  "/task/mentor/:id",
  controller.TaleMentorPatchMiddleware,
  controller.TaleMentorPatchFunction
);

// // Создание нового термина
// router.post("", controller.termsPostMiddleware, controller.termsPostFunction);

module.exports = router;
