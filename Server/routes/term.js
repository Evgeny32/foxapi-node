const express = require("express");
const router = express.Router();

const controller = require("../controllers/term");

// Отображение терминов ментора
router.get(
  "/mentor/:id",
  controller.mentorTermsGetMiddleware,
  controller.mentorTermsGetFunction
);

// Отображение терминов ребенка
router.get(
  "/child/:id",
  controller.childTermsGetMiddleware,
  controller.childTermsGetFunction
);

// Создание нового термина
router.post("", controller.termsPostMiddleware, controller.termsPostFunction);

router.patch(
  "/:id",
  controller.termPatchByIdMiddleware,
  controller.termPatchByIdFunction
);

module.exports = router;
