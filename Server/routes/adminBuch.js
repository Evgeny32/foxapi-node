const express = require("express");
const router = express.Router();

const controller = require("../controllers/adminBuch");

// Информация о детей через токен AdminBuch

router.get(
  "/children",
  controller.adminBuchChildrenGetMiddleware,
  controller.adminBuchChildrenGetFunction
);

// Детальная информация про одного ребенка по id

router.get(
  "/child/:id",
  controller.adminBuchChildDetailGetMiddleware,
  controller.adminBuchChildDetailGetFunction
);

// Информация об агентах через токен AdminBuch

router.get(
  "/agents",
  controller.adminBuchAgentsGetMiddleware,
  controller.adminBuchAgentsGetFunction
);

// Детальная информация про одного агента по id

router.get(
  "/agent/:id",
  controller.adminBuchAgentDetailGetMiddleware,
  controller.adminBuchAgentDetailGetFunction
);

// Информация о франчайзи через токен AdminBuch

router.get(
  "/franchisee",
  controller.adminBuchFranchiseeGetMiddleware,
  controller.adminBuchFranchiseeGetFunction
);

// Детальная информация про одного франчайза по id

router.get(
  "/franchisee/:id",
  controller.adminBuchFranchiseeDetailGetMiddleware,
  controller.adminBuchFranchiseeDetailGetFunction
);

// Информация о менторов через токен AdminBuch

router.get(
  "/mentors",
  controller.adminBuchMentorsGetMiddleware,
  controller.adminBuchMentorsGetFunction
);

// Детальная информация про одного ментора по id

router.get(
  "/mentor/:id",
  controller.adminBuchMentorDetailGetMiddleware,
  controller.adminBuchMentorDetailGetFunction
);

module.exports = router;
