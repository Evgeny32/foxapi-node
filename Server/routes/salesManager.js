const express = require("express");
const router = express.Router();

const controller = require("../controllers/salesManager");

router.get(
  "/me",
  controller.SalesManagerProfileDetailsGetMiddleware,
  controller.SalesManagerProfileDetailsGetFunction
);
router.get(
  "/me",
  controller.SalesManagerProfileMeGetMiddleware,
  controller.SalesManagerProfileMeGetFunction
);

router.post(
  "/me",
  controller.SalesManagerProfilePostMeMiddleware,
  controller.SalesManagerProfilePostMeFunction
);

router.patch(
  "/me",
  controller.SalesManagerProfilePatchMiddleware,
  controller.SalesManagerProfilePatchFunction
);

router.get(
  "/franchisees/:id",
  controller.SalesManagerProfileFranchiseeMeMiddleware,
  controller.SalesManagerProfileFranchiseeGetFunction
);

router.get(
  "/:id/franchisees",
  controller.SalesManagerFranchiseeByIdMiddleware,
  controller.SalesManagerFranchiseeByIdGetFunction
);
router.get(
  "/franchisee/:id",
  controller.SalesManagerFranchiseeDetailMiddleware,
  controller.SalesManagerFranchiseeDetaildGetFunction
);

router.get(
  "/packages/:id",
  controller.SalesManagerFranchiseePackageMeMiddleware,
  controller.SalesManagerFranchiseePackageGetFunction
);
router.get(
  "/:id/packages/",
  controller.FranchiseePackageListByIdViewMeMiddleware,
  controller.FranchiseePackageListByIdViewGetFunction
);
router.patch(
  "/packages/:id",
  controller.FranchiseePackagePatchViewMiddleware,
  controller.FranchiseePackagePatchViewFunction
);

router.get(
  "/:id/agents",
  controller.AgentDetailViewGetMiddleware,
  controller.AgentDetailViewGetFunction
);
router.get(
  "/all",
  controller.AllCountViewMiddleware,
  controller.AllCountViewFunction
);
router.get(
  "/:id/all",
  controller.AllCountViewMiddleware,
  controller.AllCountViewFunction
);
router.get(
  "/agents/:id",
  controller.SalesManagerAgentMiddleware,
  controller.SalesManagerAgentGetFunction
);
router.patch(
  "/agent/:id",
  controller.AgentDetailViewPatchMiddleware,
  controller.AgentDetailViewPatchFunction
);
module.exports = router;
