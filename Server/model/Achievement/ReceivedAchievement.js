const mongoose = require("mongoose");

// Полученные достижения
const ReceivedAchievementSchema = mongoose.Schema({
    // Пользователь, получивший достижение
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
//   Достижение, которое получил пользователь
  achievement: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "achievement",
    required: true,
  },
});

module.exports = mongoose.model(
  "receivedAchievement",
  ReceivedAchievementSchema
);
