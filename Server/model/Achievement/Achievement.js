const mongoose = require("mongoose");

// Достижения
const AchievementSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  img: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("achievement", AchievementSchema);
