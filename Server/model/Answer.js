const mongoose = require("mongoose");

const AnswerSchema = mongoose.Schema({
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "task",
    required: true,
  },

  name: {
    type: String,
    default: "Название",
  },

  answer: {
    type: String,
  },

  text_answer: {
    type: String,
  },
  doneAnswer: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("answer", AnswerSchema);
