const mongoose = require("mongoose");

const AgentProfileSchema = mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true,
    },

    birthday: {
        type: Date,        
    },

    citizen: {
        type: String,
    },

    education: {
        type: String,
    },

    emailBuch: {
        type: String,
    },

    passport: {
        type: String,
    },

    passportWho: {
        type: String,
    },

    passportWhen: {
        type: Date,        
    },

    country: {
        type: String,
    },

    adress: {
        type: String,
    },

    factAdress: {
        type: String,
    },

    inn: {
        type: Number,
    },

    snils: {
        type: Number,
    },

    passportPDF: {
        type: String,
    },

    snilsPDF: {
        type: String,
    },

    innPDF: {
        type: String,
    },

    resumePDF: {
        type: String,
    },
    clients: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: "user",
    }

});

AgentProfileSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
  }


module.exports = mongoose.model("AgentProfile", AgentProfileSchema);