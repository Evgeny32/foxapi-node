const mongoose = require("mongoose");
const { transformUser } = require("../scripts/transform");
const UserSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: [
      "admin",
      "parent",
      "child",
      "mentor",
      "agent",
      "branch",
      "Franchisee",
      "BuhFranch",
      "adminBuch",
      "HeadSalesManager",
      "SalesManager",
      "ClientManager",
    ],
  },
  surName: {
    type: String,
  },
  name: {
    type: String,
  },
  patronymic: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  isBaned: {
    type: Boolean,
    default: false,
  },
  banReason: {
    type: String,
  },
  img: {
    type: String,
  },
});

UserSchema.options.toJSON = {
  transform: transformUser,
};

module.exports = mongoose.model("user", UserSchema);
