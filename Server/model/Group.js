const mongoose = require("mongoose");
const { transform } = require("../scripts/transform");
const GroupSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  age: {
    type: String,
    required: true,
    enum: ["5-7", "8-12", "13-16"],
  },
  mentor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  childList: [{ type: mongoose.Schema.Types.ObjectId, ref: "childProfile" }],
  requests: [{ type: mongoose.Schema.Types.ObjectId, ref: "childProfile" }],
  deny: [{ type: mongoose.Schema.Types.ObjectId, ref: "childProfile" }],
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  online: {
    type: Boolean,
    default: false,
  },
});

GroupSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("group", GroupSchema);
