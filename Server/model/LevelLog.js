const mongoose = require("mongoose");
const { childRankEnum, childStepEnum } = require("../enums/child");

// Ежедневные логи ребенка
const LevelLogSchema = mongoose.Schema({
  child: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
  rank: {
    type: String,
    enum: childRankEnum,
  },
  tale: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tale",
  },
  program: {
    type: String,
    enum: childStepEnum,
  },
});

module.exports = mongoose.model("levelLog", LevelLogSchema);
