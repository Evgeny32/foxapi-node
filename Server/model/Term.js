const mongoose = require("mongoose");
const { transform } = require("../scripts/transform");
// Термины ребенка
const TermSchema = mongoose.Schema({
  isActive: {
    type: Boolean,
    default: false,
  },
  mentor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  child: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "childProfile",
  },
  requestedUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  title: {
    type: String,
    required: true,
  },
  descriptionGeneral: {
    type: String,
  },
  description1: {
    type: String,
  },
  description2: {
    type: String,
  },
  description3: {
    type: String,
  },
});

TermSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("term", TermSchema);
