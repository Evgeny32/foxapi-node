const mongoose = require("mongoose");

// Облако пользователя
const UserCloudSchema = mongoose.Schema({
  // Владелец облака
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  //   Вместимость
  capacity: {
    type: Number,
    default: 314572800,
  },
  //   Использованное пространство
  usedCapacity: {
    type: Number,
    default: 0,
  },
  //   Дата создания
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("userCloud", UserCloudSchema);
