const mongoose = require("mongoose");

// Настройка облака
const CloudSchema = mongoose.Schema({
  // Начальная вместимость облака
  initialCapacity: {
    type: Number,
    default: 314572800,
    min: 0,
  },
});

module.exports = mongoose.model("cloud", CloudSchema);
