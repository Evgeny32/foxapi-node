const mongoose = require("mongoose");

// Файл в облаке
const CloudFileSchema = mongoose.Schema({
  cloud: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "userCloud",
    required: true,
  },
  file: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  size: {
    type: Number,
    min: 1,
    required: true,
  },
  type: {
    type: String,
    enum: ["image", "document", "sound", "video", "other"],
    required: true,
  },
});

module.exports = mongoose.model("cloudFile", CloudFileSchema);
