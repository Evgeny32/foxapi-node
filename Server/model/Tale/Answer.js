const mongoose = require("mongoose");
const { emojiEnum, childRankEnum } = require("../../enums/child");
// Сказка
const AnswerSchema = mongoose.Schema({
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "task",
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ["text", "video", "audio", "img", "other"],
  },
  answer: {
    type: String,
  },
});

module.exports = mongoose.model("answer", AnswerSchema);
