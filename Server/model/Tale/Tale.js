const mongoose = require("mongoose");
const { emojiEnum, childRankEnum } = require("../../enums/child");
// Сказка
const TaleSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  child: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "childProfile",
  },
  pages: {
    type: Number,
  },
  number: {
    type: Number,
  },
  emoji: {
    type: String,
    enum: emojiEnum,
  },
  success: {
    type: Boolean,
    default: false,
  },
  rank: {
    type: String,
    enum: childRankEnum,
  },
  status: {
    type: String,
    required: true,
    enum: [
      "not available",
      "available",
      "paid",      
    ],
  },
  cost: {
    type: String,
    required: true,    
  },
  endDate: {
    type: Date,
  },
});

module.exports = mongoose.model("tale", TaleSchema);
