const mongoose = require("mongoose");
// Сказка
const TaleTextSchema = mongoose.Schema({
  tale: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tale",
    required: true,
  },
  sound: {
    type: String,
    default: false,
  },
  order: {
    type: String,
    default: false,
  },
  text: {
    type: String,
    default: false,
  },
});

module.exports = mongoose.model("taleText", TaleTextSchema);
