const mongoose = require("mongoose");
const { emojiEnum, childRankEnum } = require("../../enums/child");
// Сказка
const TaskSchema = mongoose.Schema({
  lesson: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "lesson",
    required: true,
  },
  answers: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "answer",
    //required: true,
  },
  name: {
    type: String,
    required: true,
  },
  success: {
    type: Boolean,
    default: false,
  },
  target: {
    type: Number,
    min: 0,
    required: true,
  },
  coins: {
    type: Number,
    default: 0,
  },
  page: {
    type: Number,
  },
  img: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("task", TaskSchema);
