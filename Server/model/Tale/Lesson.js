const mongoose = require("mongoose");
// Сказка
const LessonSchema = mongoose.Schema({
  tale: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tale",
    required: true,
  },
  success: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("lesson", LessonSchema);
