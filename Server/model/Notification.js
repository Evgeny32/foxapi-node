const mongoose = require("mongoose");

// Профиль агента
const NotificationSchema = mongoose.Schema({
  // Аккаунт
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: String,
  type: String,
  link: String,
  date: {
    type: Date,
    default: Date.now(),
  },
  new: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model("notification", NotificationSchema);
