const mongoose = require("mongoose");
const { transform } = require("../scripts/transform");

// Пакет Франшизы
const PackageSchema = mongoose.Schema({
  purchaser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "franchiseeProfile",
    required: true,
  },
  /* 
Название франшизы
0: Десткий сад
1: Начальная школа
2: Школа
 */
  name: {
    type: Number,
    enum: [0, 1, 2],
    required: true,
  },
  childCount: {
    type: Number,
    min: 25,
    required: true,
  },
  /* 
  Формат обучение франшизы
  0: Offline
  1: Online
   */
  format: {
    type: Number,
    enum: [0, 1],
    required: true,
  },
  /* 
  Статус франшизы
  0: В обработке
  1: Повторная
  2: В работе
  3: Документы отправлены
  4: Отклонена
  5: Счет оплачен
  6: Заблокирована
   */
  status: {
    type: Number,
    enum: [0, 1, 2, 3, 4, 5, 6],
    default: 0,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
  blockReason: {
    type: String,
  },
  price: {
    type: Number,
    required: true,
  },
  contractPDF: {
    type: String,
  },
  billPDF: {
    type: String,
  },
  actPDF: {
    type: String,
  },
});

PackageSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("package", PackageSchema);
