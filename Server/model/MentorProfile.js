const mongoose = require("mongoose");

const MentorProfileSchema = mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true,
    },

    branch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        // required: true,
    },

    dateWork: {
        type: Date,
        default: Date.now(),
    },

    dateLayoff: {
        type: Date,        
    },

    turnover: {
        type: Number,        
    },

    jobStatus: {
        type: String,
    },

    passport: {
        type: String,
    },

    passportWho: {
        type: String,
    },

    passportWhen: {
        type: Date,        
    },

    adress: {
        type: String,
    },

    passportPDF: {
        type: String,
    },

    snilsPDF: {
        type: String,
    },

    innPDF: {
        type: String,
    },

})

MentorProfileSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
}

module.exports = mongoose.model("mentorProfile", MentorProfileSchema);
