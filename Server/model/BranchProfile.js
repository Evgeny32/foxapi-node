const mongoose = require("mongoose");

const BranchProfileSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  package: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    // required: true,
  },

  name: {
    type: String,
  },

  franchisee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    // required: true,
  },

  region: {
    type: String,
  },

  director: {
    type: String,
  },

  main: {
    type: Boolean,
    default: false,
  },

  registrationSertificate: {
    type: String,
  },

  accountingSertificate: {
    type: String,
  },

  partnerForm: {
    type: String,
  },
});

BranchProfileSchema.options.toJSON = {
  transform: function (doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
    return ret;
  },
};

module.exports = mongoose.model("BranchProfile", BranchProfileSchema);
