const mongoose = require("mongoose");

const ChildGroupSchema = mongoose.Schema({

    name: {
        type: String,
    },
    ageGroup: {
        type: String,
        default: "5-7",
        enum: ["5-7", "8-12", "13-16"],
    },

    mentor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true,
    },

    children: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "childProfile",
        // required: true,
    },

    requests: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "childProfile",
        // required: true,
    },

    deny: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "childProfile",
        // required: true,
    },

    creationDate: {
        type: Date,
        default: Date.now(),
    },

    online: {
        type: Boolean,
        default: false,
    },
})

ChildGroupSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
  }

module.exports = mongoose.model("childgroup", ChildGroupSchema);

