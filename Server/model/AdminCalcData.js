const mongoose = require("mongoose");

const CalcDataSchema = mongoose.Schema({

    kAgentLKCost: {
        type: Number,
        default: 0,
        min: 0,
        max: 1000000
    },   

    kAgentFranchOnlineProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   

    kAgentFranchOfflineProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   

    kAgentProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   


    mentorLKOnlineProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   

    mentorLKOfflineProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   

    mentorCoachProcent: {
        type: Number,
        default: 0,
        min: 0,
        max: 100
    },   

})

CalcDataSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
}

module.exports = mongoose.model("calcData", CalcDataSchema);


