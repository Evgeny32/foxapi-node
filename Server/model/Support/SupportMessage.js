const mongoose = require("mongoose");
const { transform } = require("../../scripts/transform");

// Профиль агента
const SupportMessageSchema = mongoose.Schema({
  theme: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "supportTheme",
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  sendedAt: {
    type: Date,
    default: Date.now(),
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
});

SupportMessageSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("supportMessage", SupportMessageSchema);
