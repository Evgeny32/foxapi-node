const mongoose = require("mongoose");
const { transform } = require("../../scripts/transform");

// Профиль агента
const SupportThemeSchema = mongoose.Schema({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  name: {
    type: String,
    default: "Тема без названия",
  },
  status: {
    type: String,
    enum: ["Новое", "Прочитано"],
    default: "Новое",
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

SupportThemeSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("supportTheme", SupportThemeSchema);
