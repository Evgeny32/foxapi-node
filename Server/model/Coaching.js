const mongoose = require("mongoose");
const { transform } = require("../scripts/transform");

// Тренинг
const CoachingSchema = mongoose.Schema({
  mentor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  cost: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  customers: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
});

CoachingSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("coaching", CoachingSchema);
