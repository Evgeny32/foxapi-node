const mongoose = require("mongoose");

const FranchiseeProfileSchema = mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true,
    },

    agent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        // required: true,
    },

    manager: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        // required: true,
    },   

    name: {
        type: String,
    },

    inn: {
        type: Number,
    },

    oqrn: {
        type: Number,
    },

    kpp: {
        type: Number,
    },

    passport: {
        type: String,
    },

    passportWho: {
        type: String,
    },

    passportWhen: {
        type: Date,
    },

    emailBuch: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        // required: true,
    },    

    director: {
        type: String,
    },    

    directorPost: {
        type: String,
    },    

    ip: {
        type: Boolean,
        default: false,
    },

    organization: {
        type: Boolean,
        default: false,
    },

    urAdress: {
        type: String,
    },

    adress: {
        type: String,
    },

    rs: {
        type: Number,
    },

    cs: {
        type: Number,
    },

    bank: {
        type: String,
    },

    bik: {
        type: String,
    },

    passportPDF: {
        type: String,
    },

    decisionPDF: {
        type: String,
    },

    protocolPDF: {
        type: String,
    },

    organizationPDF: {
        type: String,
    },

    ipPDF: {
        type: String,
    },

    innPDF: {
        type: String,
    },

    oqrnipPDF: {
        type: String,
    },

    resumePDF: {
        type: String,
    },

    contractPDF: {
        type: String,
    },

});

FranchiseeProfileSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
  }

module.exports = mongoose.model("franchiseeProfile", FranchiseeProfileSchema);

