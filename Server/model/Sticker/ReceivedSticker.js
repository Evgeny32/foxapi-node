const mongoose = require("mongoose");

// Профиль агента
const ReceivedStickerSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  sticker: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "sticker",
    required: true,
  },
});

module.exports = mongoose.model("receivedSticker", ReceivedStickerSchema);
