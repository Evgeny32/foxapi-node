const mongoose = require("mongoose");

// Профиль агента
const StickerSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: String,
  img: {
    type: String,
    required: true,
  },
  pack: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "stickerPack",
  },
});

module.exports = mongoose.model("sticker", StickerSchema);
