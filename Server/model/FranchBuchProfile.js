const mongoose = require("mongoose");

const FranchBuchProfileSchema = mongoose.Schema({

  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  name: {
    type: String,
  },

  franchisee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  
});

FranchBuchProfileSchema.options.toJSON = {
  transform: function (doc, ret, options) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.__v;
      return ret;
     }
}

module.exports = mongoose.model("franchBuchProfile", FranchBuchProfileSchema);
