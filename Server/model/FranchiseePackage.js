const mongoose = require("mongoose");

const FranchiseePackageSchema = mongoose.Schema({
  purchaser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  name: {
    type: Number,
    min: 0,
    max: 2,
    enum: [0, 1, 2],
  },

  childCount: {
    type: Number,
    min: 1,
    max: 20,
  },

  format: {
    type: Number,
    min: 0,
    max: 1,
    enum: [0, 1],
  },

  status: {
    type: Number,
    min: 0,
    max: 6,
    enum: [0, 1, 2, 3, 4, 5, 6],
  },

  date: {
    type: Date,
    default: Date.now(),
  },

  blockReason: {
    type: String,
  },

  price: {
    type: Number,
  },

  count: {
    type: Number,
  },

  contractFile: {
    type: String,
  },

  billFile: {
    type: String,
  },

  aktFile: {
    type: String,
  },
});

FranchiseePackageSchema.options.toJSON = {
  transform: function (doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
    return ret;
  },
};

module.exports = mongoose.model("franchiseePackage", FranchiseePackageSchema);
