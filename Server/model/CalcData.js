const mongoose = require("mongoose");

// Настройки оплаты контрагентам и менторам
const CalcDataSchema = mongoose.Schema({
  // Стоимость ЛК для контрагента
  kagentLKCost: {
    type: Number,
    min: 0,
  },
  // Процент франчайзи онлайн для контрагента
  kagentFranchInlineProcent: {
    type: Number,
    min: 0,
  },
  //   Процент франчайзи офлайн для контрагента
  kagentFranchOfflineProcent: {
    type: Number,
    min: 0,
  },
  //   Процент контрагента
  kagentProcent: {
    type: Number,
    min: 0,
  },
  //   Оплата ментору за ЛК онлайн
  mentorLKOnline: {
    type: Number,
    min: 0,
  },
  //   Оплата ментору за ЛК офлайн
  mentorLKOflline: {
    type: Number,
    min: 0,
  },
  //   Процент ментора за тренинг
  mentorCoachProcent: {
    type: Number,
    min: 0,
  },
});

module.exports = mongoose.model("calcData", CalcDataSchema);
