const mongoose = require("mongoose");

const TaskSchema = mongoose.Schema({
  tale: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tale",
    required: true,
  },
  answers: {
    type: [],
    ref: "answer",
  },

  name: {
    type: String,
    default: "Задание",
  },

  success: {
    type: Boolean,
    default: false,
  },
  done: {
    type: Boolean,
    default: false,
  },
  target: {
    type: Number,
    min: 0,
    max: 20,
  },

  coins: {
    type: Number,
    min: 0,
    max: 20,
  },

  page: {
    type: Number,
    min: 0,
    max: 30,
  },

  img: {
    type: String,
  },
});

module.exports = mongoose.model("task", TaskSchema);
