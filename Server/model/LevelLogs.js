const mongoose = require("mongoose");

const LevelLogsSchema = mongoose.Schema({

    child: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "childProfile",
        required: true,
    },

    date: {
        type: Date,
        default: Date.now(),
    },

    rank: {
        type: String,
        default: "Silver",
        enum: ["Silver", "Gold", "Platinum"],
      },

    fox_story: {
        type: String,
    },

    program: {
        type: String,
    },

})

module.exports = mongoose.model("levelLogs", LevelLogsSchema);
