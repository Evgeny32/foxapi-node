const mongoose = require("mongoose");

const TaleSchema = mongoose.Schema({
  name: {
    type: String,
    default: "Как лисенок узнал, что такое домашние дела",
  },

  child: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "childProfile",
    required: true,
  },

  pages: {
    type: Number,
    required: true,
    default: 0,
    min: 0,
    max: 12,
  },

  number: {
    type: Number,
    default: 0,
  },
  text: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "taleText",
  },
  tasks: {
    type: [],
    ref: "task",
  },

  emoji: {
    type: String,
    default: "calm",
    enum: [
      "calm",
      "happy",
      "confuse",
      "grudge",
      "anger",
      "fury",
      "fear",
      "sad",
      "surprise",
    ],
  },

  success: {
    type: Boolean,
    default: false,
  },

  rank: {
    type: String,
    default: "Silver",
    enum: ["Silver", "Gold", "Platinum"],
  },
  cost: {
    type: String,
    default: "0",
  },
  status: {
    type: String,
    default: "not_available",
    enum: [
      "not_available",
      "не доступно",
      "available",
      "доступно",
      "paid",
      "оплачено",
    ],
  },

  endDate: {
    type: Date,
  },
});

TaleSchema.options.toJSON = {
  transform: function (doc, ret, options) {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
    return ret;
  },
};

module.exports = mongoose.model("tale", TaleSchema);
