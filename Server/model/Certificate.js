const mongoose = require("mongoose");

// Настройки оплаты контрагентам и менторам
const CertificateSchema = mongoose.Schema({
  // Название сертификата
  name: {
    type: String,
    require: true,
  },
  //   Владелец сертификата
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  //   Путь до файла сертификата
  file: {
    type: String,
    required: true,
  },
  //   Год обучения
  year: {
    type: Number,
    min: 1,
  },
  //   Тип сертификата
  type: {
    type: String,
    enum: ["certificate", "diplom"],
  },
  //   Дата выдачи сертификата
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("certificate", CertificateSchema);
