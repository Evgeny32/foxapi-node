const mongoose = require("mongoose");

const AdminProfileSchema = mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true,
    },   

})

AdminProfileSchema.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
       }
}

module.exports = mongoose.model("adminProfile", AdminProfileSchema);
