const mongoose = require("mongoose");

// Профиль менеджера по клиентам
const ClientManagerProfileSchema = mongoose.Schema({
  // Аккаунт менеджера
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  //   Клиенты
  clients: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
  //   Франчайзи
  franchisees: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
  //   Агенты
  agents: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
  //   Менторы
  mentors: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
});

module.exports = mongoose.model(
  "clientManagerProfile",
  ClientManagerProfileSchema
);
