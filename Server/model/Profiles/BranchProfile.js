const mongoose = require("mongoose");

// Профиль Филиала
const BranchProfileSchema = mongoose.Schema({
  // Аккаунт
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },

  package: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    // required: true,
  },

  name: {
    type: String,
  },

  franchisee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    // required: true,
  },

  region: {
    type: String,
  },

  director: {
    type: String,
  },

  main: {
    type: Boolean,
    default: false,
  },
  //   Свидетельство о регистрации
  registrationSertificatePDF: {
    type: String,
    required: true,
  },

  //   Свидетельство о постановке на учет
  accountingSertificatePDF: {
    type: String,
    required: true,
  },
  //   Анкета партнера
  partnerFormPDF: {
    type: String,
    required: true,
  },
  //   Является основным филиалом платформы, принимающим онлайн формат обучения
  main: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("branchProfile", BranchProfileSchema);
