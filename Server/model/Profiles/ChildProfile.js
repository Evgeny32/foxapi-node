const mongoose = require("mongoose");
const { childRankEnum, childStepEnum } = require("../../enums/child");
const { transform } = require("../../scripts/transform");

// Профиль ребенка. Доп инфа
const ChildProfileSchema = mongoose.Schema({
  // Родитель ребенка
  parent: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  // Аккаунт ребенка
  child: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  // Ментор ребенка
  mentor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  // Ранг ребенка
  rank: {
    type: String,
    default: "Silver",
    enum: childRankEnum,
  },
  // Сколько монет у ребенка
  coins: {
    type: Number,
    default: 0,
  },
  // Слоган ребенка. Девиз
  slogan: {
    type: String,
  },
  // Формат обучения ребенка
  learnFormat: {
    type: String,
    default: "Offline",
    enum: ["Offline", "Online"],
  },
  // Пол ребенка
  gender: {
    type: String,
    default: "Не выбран",
  },
  // Программа обучения ребенка
  step: {
    type: String,
    default: "First Step",
    enum: childStepEnum,
  },
  // Год обучения ребенка
  year: {
    type: Number,
    default: 1,
    min: 1,
    max: 12,
  },
  // Возраст ребенка
  age: {
    type: Number,
    required: true,
    min: 5,
    max: 16,
  },
  name: {
    type: String,
    default: "",
  },
});

ChildProfileSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("childProfile", ChildProfileSchema);
