const mongoose = require("mongoose");

// Профиль франчайзи
const FranchiseeProfileSchema = mongoose.Schema({
  // Аккаунт профиля
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  //   Наименование франчайзи
  name: {
    type: String,
    required: true,
  },
  //   ИНН Франчайзи
  inn: {
    type: String,
    required: true,
  },
  //   ОГРН франчайзи
  ogrn: {
    type: String,
    required: true,
  },
  //   КПП франчайзи
  kpp: {
    type: String,
    required: true,
  },
  //   Номер пасспорта владельца франчайзи
  passport: {
    type: String,
    required: true,
  },
  //   Кем выдан пасспорт
  passportWho: {
    type: String,
    required: true,
  },
  //   Когда выдан пасспорт
  passportWhen: {
    type: Date,
    required: true,
  },
  //   Бухгалтер франчайзи
  buch: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  //   Директор франчайзи
  director: {
    type: String,
    required: true,
  },
  //   Должность директора
  directorPost: {
    type: String,
    required: true,
  },
  //   Является ИП
  ip: {
    type: Boolean,
    default: false,
  },
  //   Юридический адрес
  urAddress: {
    type: String,
    required: true,
  },
  //   Адрес
  address: {
    type: String,
    required: true,
  },
  //   Рассчетный счет
  rs: {
    type: String,
    required: true,
  },
  //   К/С
  cs: {
    type: String,
    required: true,
  },
  //   Банк название
  bank: {
    type: String,
    required: true,
  },
  //   БИК
  bik: {
    type: String,
    required: true,
  },
  //   Файл пасспорта
  passportPDF: {
    type: String,
  },
  //   Файл решения
  decisionPDF: {
    type: String,
  },
  //   Файл протокола
  pprotocolPDF: {
    type: String,
  },
  //   Файл устава организации
  organizationPDF: {
    type: String,
  },
  //   Файл снилса
  ipPDF: {
    type: String,
  },
  //   Файл инн
  innPDF: {
    type: String,
  },
  //   Файл огрнип
  ogrnipPDF: {
    type: String,
  },
  //   Файл анкеты
  resumePDF: {
    type: String,
  },
  //   Файл договора
  contractPDF: {
    type: String,
  },
});

module.exports =
  mongoose.models.Customer ||
  mongoose.model("FranchiseeProfile", FranchiseeProfileSchema);
