const mongoose = require("mongoose");

// Профиль бухгалтера франчайзи
const BuhFranchProfileSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  franchisee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
});

module.exports = mongoose.model("buhFranchProfile", BuhFranchProfileSchema);
