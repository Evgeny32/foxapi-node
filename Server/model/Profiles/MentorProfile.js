const mongoose = require("mongoose");

// Профиль агента
const MentorProfileSchema = mongoose.Schema({
  // Аккаунт
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  branch: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  workStartDate: {
    type: Date,
    default: Date.now(),
  },
  workEndDate: Date,
  turnover: Number,
  jobStatus: {
    type: String,
    required: true,
  },
  passport: {
    type: String,
    required: true,
  },
  passportWho: {
    type: String,
    required: true,
  },
  passportWhen: {
    type: Date,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  passportPDF: {
    type: String,
    required: true,
  },
  innPDF: {
    type: String,
    required: true,
  },
  snilsPDF: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("MentorProfile", MentorProfileSchema);
