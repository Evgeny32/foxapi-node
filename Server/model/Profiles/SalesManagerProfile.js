const mongoose = require("mongoose");

// Профиль агента
const SalesManagerProfileSchema = mongoose.Schema({
  // Аккаунт
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  agents: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
  franchisees: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
});

module.exports = mongoose.model(
  "salesManagerProfile",
  SalesManagerProfileSchema
);
