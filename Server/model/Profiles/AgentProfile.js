const mongoose = require("mongoose");
const { transform } = require("../../scripts/transform");

// Профиль агента
const AgentProfileSchema = mongoose.Schema({
  // Аккаунт
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  //   День рождения
  birthday: {
    type: Date,
    required: true,
  },
  //   Гражданство
  citizen: {
    type: String,
    required: true,
  },
  //   Образование
  education: {
    type: String,
    required: true,
  },
  //   Почта бухгалтера
  emailBuch: {
    type: String,
    required: true,
    unique: true,
  },
  //   Номер паспорта
  passport: {
    type: String,
    maxLength: 20,
    required: true,
  },
  //   Кем выдан пасспорт
  passportWho: {
    type: String,
    required: true,
  },
  //   Когда выдан пасспорт
  passportWhen: {
    type: Date,
    required: true,
  },
  //   Страна
  country: {
    type: String,
    required: true,
  },
  //   Адрес
  address: {
    type: String,
    required: true,
  },
  //   Фактический адрес
  factAddress: {
    type: String,
    required: true,
  },
  //   ИНН
  inn: {
    type: String,
    required: true,
  },
  //   Снилс
  snils: {
    type: String,
    required: true,
  },
  //   Файл пасспорта
  passportPDF: {
    type: String,
    required: true,
  },
  //   Файл снилса
  snilsPDF: {
    type: String,
    required: true,
  },
  //   Файл ИНН
  innPDF: {
    type: String,
    required: true,
  },
  //   Файл резюме
  resumePDF: {
    type: String,
    required: true,
  },
  // Франчайзи, зарегестрированные агентом
  franchisees: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "user",
  },
  contract: {
    type: String,
  },
  contractId: {
    type: Number,
  },
});

AgentProfileSchema.options.toJSON = {
  transform: transform,
};

module.exports = mongoose.model("agentProfile", AgentProfileSchema);
