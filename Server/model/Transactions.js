const mongoose = require("mongoose");

// Настройки оплаты контрагентам и менторам
const TransactionSchema = mongoose.Schema({
  // Название
  name: {
    type: String,
    require: true,
  },
  // Пользователь
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
   // Пользователь
   child: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",    
  },
   // Пользователь
   taleId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tale",    
  },
  // ID заказа в сбере
  order_id: {
    type: String,
    //required: true,
  },
  // Сумма
  value: {
    type: Number,
    required: true,
  },
  // Оплачено
  paid: {
    type: Boolean,
    default: false,
  },
  // Дата
  date: {
    type: Date,
    default: Date.now(),
  },
  category: {
    type: String,
    required: true,
    enum: ["product", "payment_PA", "training", "tale"],
  },
});

module.exports = mongoose.model("transaction", TransactionSchema);
