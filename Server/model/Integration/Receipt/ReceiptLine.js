const mongoose = require("mongoose");
const { vatRateEnum } = require("../../enums/integration");
// Строка поступления услуг
const ReceiptLineSchema = mongoose.Schema({
  // Id номенклатуры
  Service_Id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Service",
  },
  //   Количество
  Quantity: Number,
  //   Цена
  Price: Number,
  //   Сумма без НДС
  Cost: Number,
  //   Ставка НДС
  VatRate: {
    type: String,
    enum: vatRateEnum,
  },
  //   Сумма НДС
  Vat: Number,
  //   Сумма с НДС
  CostWithVat: Number,
});

module.exports = mongoose.model("receiptLine", ReceiptLineSchema);
