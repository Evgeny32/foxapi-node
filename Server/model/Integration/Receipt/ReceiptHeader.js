const mongoose = require("mongoose");
// Шапка поступления услуг
const ReceiptHeaderSchema = mongoose.Schema({
  // Уникальный код
  Doc_Id: String,
  //   Номер документа
  doc_number: {
    type: String,
    maxLength: 11,
  },
  //   Дата документа
  doc_data: Date,
  //   ID организации
  Id_Org: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   ID Контрагента
  Id_Partner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   ID договора
  Id_dog: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "contract",
  },
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("receiptHeader", ReceiptHeaderSchema);
