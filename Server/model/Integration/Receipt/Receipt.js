const mongoose = require("mongoose");
// Поступление услуг
const ReceiptSchema = mongoose.Schema({
  Header: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "receiptHeader",
  },
  Lines: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "receiptLine",
  },
});

module.exports = mongoose.model("receipt", ReceiptSchema);
