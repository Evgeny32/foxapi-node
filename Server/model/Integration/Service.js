const mongoose = require("mongoose");
// Банковский счет
const ServiceSchema = mongoose.Schema({
  // Уникальный код
  Id: String,
  //   Наименование
  Name: {
    type: String,
  },
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("service", ServiceSchema);
