const mongoose = require("mongoose");
// Поступление денежных средств
const PaymentSchema = mongoose.Schema({
  // Уникальный код
  Doc_Id: String,
  //   Номер документа
  doc_number: {
    type: String,
    maxLength: 11,
  },
  //   Дата документа
  doc_data: Date,
  //   ID организации
  Id_Org: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   Банковский счет организации
  Account: String,
  //   ID Контрагента
  Id_Partner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   ID договора
  Id_dog: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "contract",
  },
  Cost: Number,
  //   Ставка НДС
  VatRate: {
    type: String,
    enum: vatRateEnum,
  },
  //   Сумма НДС
  Vat: Number,
  //  Назначение платежа
  Purpose: String,
  Id_Receipt: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "receipt",
  },
  Id_Order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "paymentOrder",
  },
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("payment", PaymentSchema);
