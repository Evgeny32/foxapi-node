const mongoose = require("mongoose");
// Реализация услуг
const RealizationSchema = mongoose.Schema({
  Header: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "realizationHeader",
  },
  Lines: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "realizationLine",
  },
});

module.exports = mongoose.model("realization", RealizationSchema);
