const mongoose = require("mongoose");
// Шапка счета на оплату
const InvoiceHeaderSchema = mongoose.Schema({
  // Уникальный код
  Doc_Id: String,
  //   Номер документа
  doc_number: {
    type: String,
    maxLength: 11,
  },
  //   Дата документа
  doc_data: Date,
  //   ID организации
  Id_Org: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   ID Контрагента
  Id_Partner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  //   Банковский счет организации
  Account: String,
  //   ID договора
  Id_dog: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "contract",
  },
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("invoiceHeader", InvoiceHeaderSchema);
