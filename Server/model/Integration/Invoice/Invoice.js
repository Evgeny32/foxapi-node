const mongoose = require("mongoose");
// Счет на оплату
const InvoiceSchema = mongoose.Schema({
  Header: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "invoiceHeader",
  },
  Lines: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "invoiceLine",
  },
});

module.exports = mongoose.model("invoice", InvoiceSchema);
