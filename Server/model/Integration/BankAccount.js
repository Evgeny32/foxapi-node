const mongoose = require("mongoose");
const { ownerTypeEnum } = require("../../enums/integration");
// Банковский счет
const PartnerSchema = mongoose.Schema({
  // Уникальный код
  Id: String,
  //   Тип владельца
  OwnerType: {
    type: String,
    enum: ownerTypeEnum,
  },
  // Id владельца
  OnwerID: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
  // Расчетный счет
  Account: {
    type: String,
  },
  // БИК
  BIC: {
    type: String,
  },
  // Признак основного счета
  Main: {
    type: Boolean,
    default: false,
  },
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("partner", PartnerSchema);
