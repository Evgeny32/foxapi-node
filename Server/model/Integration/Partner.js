const mongoose = require("mongoose");
const { partnerTypeEnum } = require("../../enums/integration");
// Партнер / Организация
const PartnerSchema = mongoose.Schema({
  // Уникальный код
  Id: String,
  //   ИНН
  INN: {
    type: String,
    maxLength: 12,
  },
  //   КПП
  KPP: {
    type: String,
    maxLength: 9,
  },
  //   Наименование
  Name: String,
  //   Вид контрагента
  Type: {
    type: String,
    enum: partnerTypeEnum,
  },
  //   Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
  //   Является организацией
  IsOrganization: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("partner", PartnerSchema);
