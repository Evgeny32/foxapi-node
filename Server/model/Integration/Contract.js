const mongoose = require("mongoose");
const { contractTypeEnum } = require("../../enums/integration");
// Банковский счет
const ServiceSchema = mongoose.Schema({
  // Уникальный код
  Cont_Id: String,
  //   ID организации
  Cont_Org: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Partner",
  },
  //   ID контрагента
  Cont_Owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Partner",
  },
  //   Вид договора
  Cont_Type: {
    type: String,
    enum: contractTypeEnum,
  },
  //   Номер договора
  Cont_Number: String,
  //   Дата договора
  Cont_Data: Date,
  //   Наименование договора
  Cont_Name: String,
  // Отметка об удалении
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("service", ServiceSchema);
