const mongoose = require("mongoose");
// Шапка отчета о розничной продаже
const SalesReportHeaderSchema = mongoose.Schema({
  // Уникальный код
  Doc_Id: String,
  //   Номер документа
  doc_number: {
    type: String,
    maxLength: 11,
  },
  //   Дата документа
  doc_data: Date,
  //   ID организации
  Id_Org: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "partner",
  },
});

module.exports = mongoose.model("salesReportHeader", SalesReportHeaderSchema);
