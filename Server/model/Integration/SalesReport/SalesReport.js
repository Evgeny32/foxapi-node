const mongoose = require("mongoose");
// Счет на оплату
const SalesReportSchema = mongoose.Schema({
  Header: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "salesReportHeader",
  },
  Lines: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "salesReportLine",
  },
});

module.exports = mongoose.model("salesReport", SalesReportSchema);
