const mongoose = require("mongoose");
const { vatRateEnum } = require("../../enums/integration");
// Строка отчета о розничной продаже
const SalesReportLineSchema = mongoose.Schema({
  // Id номенклатуры
  Service_Id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Service",
  },
  //   Количество
  Quantity: Number,
  //   Цена
  Price: Number,
  //   Сумма без НДС
  Cost: Number,
  //   Ставка НДС
  VatRate: {
    type: String,
    enum: vatRateEnum,
  },
  //   Сумма НДС
  Vat: Number,
  //   Сумма с НДС
  CostWithVat: Number,
  IsDeleted: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("salesReportLine", SalesReportLineSchema);
