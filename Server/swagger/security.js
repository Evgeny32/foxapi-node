module.exports = {
  securitySchemes: {
    TokenAuth: {
      type: "apiKey",
      in: "header",
      name: "token",
      description:
        'Необходимо ввести токен. Пример - "Token c5fdff024d65346d47fe6a1afbc4f2f6593d8e04"',
    },
  },
};
