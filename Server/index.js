const express = require("express");
const InitiateMongoServer = require("./config/db");
const cors = require("cors");

require("dotenv/config");

// Routes imports
const routes = require("./routes");
// Подключение к Базе данных
InitiateMongoServer();

const app = express();

// Статика
app.use("/uploads", express.static("uploads"));

// Middleware
app.use(express.json());
app.use(cors());
app.disable("etag");
const PORT = process.env.PORT || 5000;

routes.forEach((route) => {
  app.use("/api/v1/" + route.name, route.router);
});

app.listen(PORT, (req, res) => console.log(`App listen on port ${PORT}`));
