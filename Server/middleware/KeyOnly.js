module.exports = function (req, res, next) {
  try {
    const result = req.header("api_key");
    if (!(result === process.env.API_KEY))
      return res.status(401).json({ message: "ApiKey Error" });
    next();
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Invalid api key" });
  }
};
