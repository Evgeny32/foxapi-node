const childRankEnum = [
  "Silver",
  "Gold",
  "Platinum",
  "Basic Business",
  "Advance Business",
  "Profi Business",
  "Super Basic Business",
  "Super Advance Business",
  "Super Profi Business",
];

const childStepEnum = ["First Step", "First Idea", "First Business"];
const emojiEnum = [
  "calm", //Спокойствие
  "happy", //Радость
  "confuse", //Смущение
  "grudge", //Обида
  "anger", //Злость
  "fury", //Ярость
  "fear", //Испуг
  "sad", //Грусть
  "surprise", //Удивление
];

module.exports = { childRankEnum, childStepEnum };
