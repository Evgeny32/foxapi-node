const partnerTypeEnum = ["Юрлицо", "Физлицо"];
const ownerTypeEnum = ["Организация", "Контрагент"];

const contractTypeEnum = ["С покупателем", "С поставщиком"];

const vatRateEnum = ["20%", "10%", "0%", "Без НДС"];

module.exports = {
  partnerTypeEnum,
  ownerTypeEnum,
  contractTypeEnum,
  vatRateEnum,
};
