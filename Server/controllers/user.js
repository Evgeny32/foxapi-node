const { upload, avatarUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");
const FranchiseeProfile = require("../model/FranchiseeProfile");
const AgentProfile = require("../model/AgentProfile");
const Code = require("../model/Code");
const {
  AuthTokenPromotionContext,
} = require("twilio/lib/rest/accounts/v1/authTokenPromotion");

module.exports.codePostMiddleware = [
  check("phone", "Please Enter a Valid Phone").not().isEmpty(),
];
module.exports.codePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let { phone } = req.body;
  try {
    let code = await Code.findOne({
      phone,
    });
    if (code) {
      //let number = randomNumber(4); //TODO:Раскоментировать для работы отправки СМС
      let number = "7777"; //TODO:Закоментировать для работы отправки СМС
      await Code.updateOne({ _id: code._id }, { code: number }, (err) => {
        if (err) {
          console.log(err);
          res.status(500).json({ msg: "Возникла ошибка при отправке кода" });
        }
      });
      // sendSms(phone, "Ваш код подтверждения: " + number) //TODO:Раскоментировать для работы отправки СМС
      res.status(200).json({
        msg: "Код успешно отправлен",
      });
    } else {
      // let number = randomNumber(4) //TODO:Раскоментировать для работы отправки СМС
      let number = "7777"; //TODO:Закоментировать для работы отправки СМС
      let code = Code({
        phone,
        code: number,
      });
      await code.save();
      // sendSms(phone, "Ваш код подтверждения: " + number) //TODO:Раскоментировать для работы отправки СМС
      res.status(200).json({ msg: "Код успешно отправлен" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Возникла ошибка при отправке кода" });
  }
};

module.exports.signupPostMiddleware = [
  check("phone", "Пожалуйста, введите телефон").not().isEmpty(),
  check("email", "Пожалуйста, введите правильную электронную почту").isEmail(),
  check("code", "Пожалуйста, введите код").isLength({ min: 4, max: 4 }),
  check("role", "Пожалуйста, введите правильную роль").custom(
    (value, { req }) =>
      User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
  ),
];
module.exports.signupPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { phone, email, role, code } = req.body;
  try {
    // Проверка на наличие юзера
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(411).json({
        msg: "Электронная почта уже зарегистрирована в системе",
      });
    }
    user = await User.findOne({
      phone,
    });
    if (user) {
      return res.status(412).json({
        msg: "Телефонный номер уже зарегистрирован в системе",
      });
    }
    // Проверка на наличие юзера

    // Проверка смс кода
    let baseCode = Code.findOne({ code, phone });
    if (!baseCode) {
      return res.status(400).json({
        msg: "Неверный код",
      });
    }
    // Проверка смс кода
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      phone,
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    if(req.body.agent) {
      let user = await User.findOne({ email });
      console.log(user);
      let agent = await User.findOne({ _id: req.body.agent });
      franchiseeprofile = new FranchiseeProfile({
        user: user._id,
        agent: agent._id
      });
      let agentprofile = await AgentProfile.findOne({ user: agent._id });
      agentprofile.clients.push(user);

      await franchiseeprofile.save();
      await agentprofile.save();

    }

    if(req.body.manager) {
      let user = await User.findOne({email });
      let manager = await User.findOne({ _id: req.body.manager });
      franchiseeprofile = new FranchiseeProfile({
        user: user._id,
        manager: manager._id
      });

      await franchiseeprofile.save();
    }

    // if(req.body.parentAgent) {
    //   let user = await User.findOne({email });
    //   let agent = await AgentProfile.findOne({ _id: req.body.parentAgent });
    //   agent.clients.push(user._id);

    //   await agent.save();
      
    // }

    sendRegistrationEmail(email, { email: email, password: password });

    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      // {
      //   expiresIn: 10000,
      // },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.loginPostMiddleware = [
  check("email", "Почта неверная").isEmail(),
  check("password", "Пароль неверный").isLength({ min: 4 }),
];
module.exports.loginPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { email, password } = req.body;
  try {
    let user = await User.findOne({
      email,
    });
    if (!user) {
      return res.status(411).json({
        msg: "Пользователь с данной почтой не существует",
      });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(412).json({
        msg: "Неверный пароль",
      });
    }

    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      payload,
      "randomString",
      !req.body.remember
        ? {
            expiresIn: 3600,
          }
        : {},
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).json({
      msg: "Ошибка сервера. Обратитесь к администрации",
    });
  }
};

module.exports.meGetMiddleware = auth;
module.exports.meGetFunction = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);
    res.json(user.toJSON());
  } catch (err) {
    res
      .status(500)
      .json({ msg: "Ошибка на сервере. Обратитесь к администрации" });
  }
};

module.exports.mePatchMiddleware = [
  auth,
  avatarUpload.single("img"),
  check("role", "Please enter a valid role").custom((value, { req }) =>
    User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
  ),
];
module.exports.mePatchFunction = async (req, res) => {
  let userInfo = { ...req.body };

  if (req.file) {
    userInfo.img = req.file.path;
  }

  if (userInfo.password) {
    const salt = await bcrypt.genSalt(10);
    userInfo.password = await bcrypt.hash(req.body.password, salt);
  }

  if (userInfo.email) {
    let oldUser = await User.findOne({ email: userInfo.email });
    if (oldUser && oldUser.id != req.user.id) {
      return res
        .status(411)
        .json({ msg: "User with this email already exist" });
    }
  }

  if (userInfo.phone) {
    let oldUser = await User.findOne({ phone: req.body.phone });
    if (oldUser && oldUser.id != req.user.id) {
      return res
        .status(412)
        .json({ msg: "User with this phone already exist" });
    }
  }

  let query = { _id: req.user.id };

  await User.updateOne(query, userInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: (await User.findById(req.user.id)).toJSON(),
  });
};

module.exports.userGetMiddleware = [auth];
module.exports.userGetFunction = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.json(user.toJSON());
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.userPatchMiddleware = [auth, avatarUpload.single("img")];
module.exports.userPatchFunction = async (req, res) => {
  let userInfo = { ...req.body };

  if (req.file) {
    userInfo.img = req.file.path;
  }

  if (userInfo.password) {
    const salt = await bcrypt.genSalt(10);
    userInfo.password = await bcrypt.hash(req.body.password, salt);
  }

  if (userInfo.email) {
    let oldUser = await User.findOne({ email: userInfo.email });
    if (oldUser && oldUser.id != req.params.id) {
      return res
        .status(411)
        .json({ msg: "User with this email already exist" });
    }
  }

  if (userInfo.phone) {
    let oldUser = await User.findOne({ phone: req.body.phone });
    if (oldUser && oldUser.id != req.params.id) {
      return res
        .status(412)
        .json({ msg: "User with this phone already exist" });
    }
  }

  let query = { _id: req.params.id };

  await User.updateOne(query, userInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
  });
};
