const { upload, avatarUpload } = require("../middleware/files");
const { documentAnswer } = require("../middleware/files");

const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
// const ChildProfile = require("../model/ChildProfile");
const Tale = require("../model/Tale");
const Task = require("../model/Task");
const Answer = require("../model/Answer");
const TaleText = require("../model/Tale/TaleText");
const fs = require("fs");
const transporter = require("../config/mail");
const path = require("path");
const ChildProfile = require("../model/Profiles/ChildProfile");
const { find } = require("../model/User");
//const patthTale = require("../json/tales/taleList");

module.exports.TalesGetMiddleware = auth;

module.exports.TalesGetFunction = async (req, res) => {
  try {
    const child = ChildProfile.findOne({ child: req.user.id }).populate(
      "tasks"
    );
    const tales = Tale.findOne({ child: child.id });

    res.json(tales);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

const emojiCheck = (value, { req }) =>
  Tale.schema.path("emoji").enumValues.indexOf(value) >= 0 ? true : false;

const rankCheck = (value, { req }) =>
  Tale.schema.path("rank").enumValues.indexOf(value) >= 0 ? true : false;

// module.exports.TaleCreatePostMiddleware = [
//   auth,
//   check("emoji", "Please Enter a Valid emoji").custom(emojiCheck),
//   check("rank", "Please Enter a Valid emoji").custom(rankCheck),
// ];

// module.exports.TaleCreatePostFunction = async (req, res) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({
//       errors: errors.array(),
//     });
//   }

//   let taleInfo = { ...req.body };

//   try {
//     tale = new Tale({
//       // tale: talesList['tale' + String(req.params.number)],
//       number: req.params.number,
//       child: req.params.id,
//       ...taleInfo,
//     });

//     await tale.save();

//     let query = { _id: req.user.id };

//     await User.updateOne(query, taleInfo, (err) => {
//       if (err) {
//         console.log(err);
//         res.status(500).json({ error: "Error on saving" });
//       }
//     });

//     res.status(200).json({
//       msg: "Tale created",
//       profile: await Tale.findOne({ child: req.user.id }),
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Error in Saving");
//   }
// };

const getAllDirFiles = function (dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath);

  arrayOfFiles = arrayOfFiles || [];

  files.forEach(function (file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllDirFiles(dirPath + "/" + file, arrayOfFiles);
    } else {
      arrayOfFiles.push(file);
    }
  });

  return arrayOfFiles;
};

module.exports.TaleCreatePostMiddleware = auth;
module.exports.TaleCreatePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  //   const dir = await fs.promises.opendir(path);
  //   let dir = fs.readdir("../json/tales/tale_list", (err, items) => {
  //     for (const dirent of items) {
  //       console.log(dirent);
  //     }
  //   });
  //console.log(dir);
  //   let number_of_files = getAllDirFiles(
  //     __dirname + `/tales/taleList/tale`
  //   ).length;
  //   console.log(number_of_files);
  let allTales = [];
  try {
    for (var i = 0; i <= 8; i++) {
      let realPath = fs.openSync(
        path.join(__dirname + `/tales/taleList/tale${i + 1}.json`)
      );

      let tale = JSON.parse(fs.readFileSync(realPath, "utf8"));
      tale["child"] = req.params.childId;
      //fs.writeFileSync(realPath, JSON.stringify(tale));
      console.log(tale["emoji"]);
      if (tale["emoji"] === null) {
        tale["emoji"] = "calm";
      }
      tales = new Tale({
        number: tale["number"],
        name: tale["name"],
        pages: tale["pages"],
        emoji: tale["emoji"],
        cost: tale["cost"],
        status: tale["status"],
        child: tale["child"],
        //tasks: tale["tasks"],
      });
      await tales.save();

      let tasks = tale.tasks;
      //   console.log(tasks);
      //tasks = tasks.map((task) => task.toJSON());
      let create_task = [];
      let answers = [];
      tales.tasks = [];
      for await (let task of tasks) {
        create_task = task;
        // console.log(create_task);
        //answers = create_task;
        //create_task["tale"] = tale;
        answers = await create_task.answers;
        create_task = new Task({ ...create_task, tale: tales });

        await create_task.save();
        // console.log(answers);
        // for await (let answer of answers) {
        //answer["task"] = created_task;
        create_task.answers = [];
        for await (let answer of answers) {
          ans = new Answer({ ...answer, task: create_task });
          ans.save();
          let ansobj = ans.toJSON();
          delete ansobj.task;
          create_task.answers.push(ansobj);
        }
        
        
        await create_task.save();
        // console.log(create_task.tale);
        let taskobj = create_task.toJSON();
        delete taskobj.tale;
        // console.log(taskobj);
        
        tales.tasks.push(taskobj);
        //}
        await tales.save();
      }
      allTales.push(tales);
    }
    childprofile = ChildProfile.findOne({ _id: req.params.childId });
    parent = User.findOne({ _id: childprofile.parent });

    // const emailData = {
    //   from: '"Business Fox" ' + process.env.SMTP_LOGIN,
    //   to: parent.email,
    //   subject: "Доступно новая сказка",
    //   text: "Ваш ребенок принят в группу. Для Вашего ребенка доступна новая сказка. Для оплаты сказки можете перейти в личный кабинет на сайте b-fox.ru",
    // };

    // transporter.sendMail(emailData);

    res.status(200).json(allTales);
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении");
  }
};

// const Tale = require("../model/Tale/Tale");

module.exports.childTalesGetMiddleware = auth;

module.exports.childTalesGetFunction = async (req, res) => {
  try {
    let tales = await Tale.find({ child: req.params.id });
    tales = tales.map((tale) => tale.toJSON());
    res.json({ msg: "Сказки успешно получены", data: tales });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сказок. Обратитесь к администрации",
    });
  }
};

module.exports.TaleGetMiddleware = auth;

module.exports.TaleGetFunction = async (req, res) => {
  try {
    let tales = await Tale.find({ _id: req.params.id }).populate("tasks");

    tales = tales.map((tale) => tale.toJSON());
    for await (tale of tales) {
      tales.text = TaleText.find({ tale: tale.number }).sort("order");
      let tasks = await Task.find({ tale: tale._id });
      tasks = tasks.map((task) => task.toJSON());
      for await (task of tasks) {
        let answers = Answer.find({ task: task._id });
        task.answers = answers;
      }
      tales.tasks = tasks;
    }
    res.status(200).json(tales);
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сказок. Обратитесь к администрации",
    });
  }
};
module.exports.TaleGetViewMiddleware = auth;

module.exports.TaleGetViewFunction = async (req, res) => {
  try {
    let tales = await Tale.findOne({ _id: req.params.id }).populate("child");

    res.status(200).json(tales);
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сказок. Обратитесь к администрации",
    });
  }
};

module.exports.TalePostMiddleware = auth;
module.exports.TalePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let path = "../json/tales/" + "/tale" + req.params.id;
  try {
    let tale = JSON.parse(fs.readFileSync(path, "utf8"));
    tale.child = 1;
    fs.writeFileSync(path, JSON.stringify(content));
    tales = new Tale({
      number: tale.number,
      name: tale.name,
      pages: tale.pages,
      emoji: tale.emoji,
      child: tale.child,
    });
    await tales.save();
    let tasks = tale.tasks;
    for await (task of tasks) {
      let create_task = task.slice();
      let answers = create_task.answers;
      create_task["tale"] = tale;
      create_task = new Task({ ...create_task });
      await create_task.save();
      for await (let answer of answers) {
        answer["task"] = created_task.splice("answers");
        ans = new Answer(...answer);
        ans.save();
      }
    }
    res.status(200).json(tales);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.TaleDeleteMiddleware = auth;

module.exports.TaleDeleteFunction = async (req, res) => {
  try {
    await Tale.findByIdAndDelete({ _id: req.params.id });
    res.status(200).json({
      msg: "  delete",
    });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сказок. Обратитесь к администрации",
    });
  }
};
module.exports.getTaksMiddleware = auth;

module.exports.getTasksFunction = async (req, res) => {
  try {
    let task = await Task.find({ tale: req.params.id });
    res.status(200).json(task);
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении задании. Обратитесь к администрации",
    });
  }
};

module.exports.TalePatchMiddleware = auth;
module.exports.TalePatchFunction = async (req, res) => {
  let tale = { ...req.body };

  let query = { _id: req.params.id };

  await Tale.updateOne(query, tale, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Сказка изменено",
    data: {
      transaction: await Tale.findOne(query),
    },
  });
};

module.exports.TaskGetMiddleware = auth;

module.exports.TaskGetFunction = async (req, res) => {
  try {
    let task = await Task.findOne({ _id: req.params.id }).populate("answer");

    task.answers = await Answer.find({ task: task });

    res.json(task);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.TaskPatchMiddleware = auth;
module.exports.TaskPatchFunction = async (req, res) => {
  let task = { ...req.body };

  let query = { _id: req.params.id };

  await Task.updateOne(query, task, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " Task Updated",
    data: {
      transaction: await Task.findOne(query),
    },
  });
};

module.exports.AnswerGetMiddleware = auth;

module.exports.AnswerGetFunction = async (req, res) => {
  try {
    
    const answer = await Answer.findOne({ _id: req.params.id });
    res.status(200).json(answer);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Ошибка при получении данных" });
  }
};
module.exports.AnswerTaskGetMiddleware = auth;

module.exports.AnswerTaskGetFunction = async (req, res) => {
  try {
    let answer = await Answer.findOne({ _id: req.params.id });
    res.status(200).json(answer);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Ошибка при получении данных" });
  }
};

module.exports.AnswerPatchMiddleware = [
  auth,
  documentAnswer.fields([{ name: "answer", maxCount: 4 }]),
];
module.exports.AnswerPatchFunction = async (req, res) => {
  let answ = { ...req.body };
 
  //console.log(req.files.Object);

  let query = { _id: req.params.id };
  console.log(req.files)
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        answ[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }
  await Answer.updateOne(query, answ, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Ошибка при сохранении" });
    }
  });
  let an = await Answer.findOne(query);

  let queryTask = { _id: an.task };
  let tas = await Task.findOne(queryTask);

  for await (let asw of tas.answers) {
    if (asw._id == req.params.id) {
      asw = answ;
      asw.task = tas._id;
      asw.doneAnswer = false;
      tas.answers.push(asw);
      console.log();
    }
  }
  tas.save();
  //   //answers: { _id: query }
  //   //   let ta = await Task.findOne(queryTask).populate("answers");
  //   //   if (ta.answers._id === req.params.id) {

  //   //   }
  //   //   await Task.updateOne(queryTask, ans, (err) => {
  //   //     if (err) {
  //   //       console.log(err);
  //   //       res.status(500).json({ error: "Error on saving" });
  //   //     }
  //   //   });
  //   let ta = await Task.findOne(queryTask);
  //   ta.answers.push(an);
  //   ta.save();
  //console.log();
  res.status(200).json({
    msg: "Ответ загружен",
    data: {
      answers: await Answer.findOne(query),
    },
  });
};

module.exports.TaleChangeViewMiddleware = auth;
module.exports.TaleChangeViewFunction = async (req, res) => {
  for await (let child of req.body) {
    console.log(child["id"]);
    let number;
    let tale = Tale.find(
      { child: child["id"], status: "paid" },
      async (err, docs) => {
        console.log(docs);
        for await (doc of docs) {
          number = doc.number + 1;

          console.log(number);

          let query = {
            child: child["id"],
            number: number,
            status: "not_available",
          };

          await Tale.updateOne(query, { status: "available" }, (err) => {
            if (err) {
              console.log(err);
              res.status(500).json({ error: "Ошибка при сохранении" });
            }
          });
        }
      }
    );
    console.log(tale);

    let childprofile = ChildProfile.findOne({ _id: child["id"] });
    parent = User.findOne({ _id: childprofile.parent });
  }
  res.status(200).json({
    msg: "Статус сказок измененены",
  });
};
module.exports.TaleMentorPatchMiddleware = auth;
module.exports.TaleMentorPatchFunction = async (req, res) => {
  console.log(req.body);
  let query = {
    _id: req.params.id,
  };
  await Answer.updateOne(query, { doneAnswer: req.body.doneAnswer }, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Ошибка при сохранении" });
    }
  });
  let an = await Answer.findOne(query);
  let anwers = await Answer.find({ task: an.task._id });

  let j = 0;
  let i = 0;
  console.log(anwers);
  for await (let ans of anwers) {
    if (ans.doneAnswer === true) {
      i++;
    }
    j++;
  }
  console.log(j);
  console.log(i);
  if (i == j) {
    await Task.updateOne({ _id: an.task }, { done: true }, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Ошибка при сохранении" });
      }
    });
  }

  res.status(200).json({
    msg: "Ответ принят",
  });
};
