const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const Group = require("../model/Group");
const ChildProfile = require("../model/Profiles/ChildProfile");

module.exports.mentorGroupsGetMiddleware = auth;

module.exports.mentorGroupsGetFunction = async (req, res) => {
  try {
    let groups = await Group.find({ mentor: req.params.id });
    groups = groups.map((group) => group.toJSON());
    res.json({ msg: "Группы успешно получены", data: groups });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении групп. Обратитесь к администрации",
    });
  }
};

module.exports.groupsPostMiddleware = [
  auth,
  check("name", "Название не может быть пустым").not().isEmpty(),
  check("age", "Пожалуйста, введите правильную возрастную кателгорию").custom(
    (value, { req }) =>
      Group.schema.path("age").enumValues.indexOf(value) >= 0 ? true : false
  ),
  check("mentor", "Термин не может быть создан без ментора").not().isEmpty(),
];

module.exports.groupsPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User
  let groupInfo = {
    name: req.body.name,
    mentor: req.body.mentor,
    age: req.body.age,
  };
  if (req.body.online) {
    groupInfo.online = true;
  }
  try {
    let group = new Group({ ...groupInfo });
    await group.save();

    res.status(200).json({
      msg: "Группа успешно создана",
      data: await Group.findOne(groupInfo),
    });
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Ошибка при создании. Обратитесь к администратору" });
  }
};

module.exports.groupGetByIdMiddleware = auth;

module.exports.groupGetByIdFunction = async (req, res) => {
  try {
    let group = await Group.findById(req.params.id)
      .populate({ path: "childList", populate: { path: "child" } })
      .populate({ path: "requests", populate: { path: "child" } });
    group = group.toJSON();
    res.json({ msg: "Группа успешно получена", data: group });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении группы. Обратитесь к администрации",
    });
  }
};

module.exports.groupPatchByIdMiddleware = auth;

module.exports.groupPatchByIdFunction = async (req, res) => {
  try {
    let query = {
      _id: req.params.id,
    };
    await Group.updateOne(query, req.body, (err) => {
      if (err) {
        res
          .status(500)
          .json({ msg: "Ошибка при сохранении. Обратитесь к администрации" });
      }
    });
    res.json({ msg: "Группа успешно обновлена" });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при обновлении группы. Обратитесь к администрации",
    });
  }
};
