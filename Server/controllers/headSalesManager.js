const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const User = require("../model/User");
const ClientManagerProfile = require("../model/Profiles/ClientManagerProfile");
const SalesManagerProfile = require("../model/Profiles/SalesManagerProfile");
const FranchiseeProfile = require("../model/Profiles/FranchiseeProfile");
const MentorProfile = require("../model/Profiles/MentorProfile");
const FranchiseePackage = require("../model/FranchiseePackage");
const AgentProfile = require("../model/Profiles/AgentProfile");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sendRegistrationEmail = require("../mails/registration");

module.exports.headSalesManagerProfileMeGetMiddleware = auth;

module.exports.headSalesManagerProfileMeGetFunction = async (req, res) => {
  try {
    let users = await User.find({
      role: ["ClientManager", "SalesManager"],
    });

    res.status(200).json(users);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching user");
  }
};

module.exports.headSalesManagerProfileMePosttMiddleware = auth;

module.exports.headSalesManagerProfileMePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { phone, email, name, surName, patronymic } = req.body;
  try {
    // Проверка на наличие юзера
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(411).json({
        msg: "Email Already Registered",
      });
    }
    user = await User.findOne({
      phone,
    });
    if (user) {
      return res.status(412).json({
        msg: "Phone Already Registered",
      });
    }
    // Проверка на наличие юзера

    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      phone,
      email,
      password,
      name,
      surName,
      patronymic,
      role: req.body.role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    let profile = {};

    const manager_account = await User.findOne({ email: req.body.email });
    if (req.body.role === "ClientManager") {
      profile = new ClientManagerProfile({ user: manager_account._id });
    } else {
      profile = new SalesManagerProfile({ user: manager_account._id });
    }
    await profile.save();
    sendRegistrationEmail(email, { email: email, password: password });

    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      // {
      //   expiresIn: 10000,
      // },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.ManagerProfileilMiddleware = auth;

module.exports.ManagerProfileGetFunction = async (req, res) => {
  try {
    let manager = await User.findOne({ _id: req.params.id });
    res.status(200).json(manager);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};
module.exports.headSalesManagerSelectManagerMiddleware = auth;

module.exports.headSalesManagerSelectManagerFunction = async (req, res) => {
  let profile_manager;
  if (req.body.managerrole === "ClientManager") {
    profile_manager = await ClientManagerProfile.findOne({
      user: req.body.managerId,
    });
    console.log(profile_manager);
  } else {
    profile_manager = await SalesManagerProfile.findOne({
      user: req.body.managerId,
    });
  }

  if (req.body.role === "Franchisee") {
    if (req.body.managerrole === "ClientManager") {
      let managers = await ClientManagerProfile.find({
        franchisees: req.body.clientAccountId,
      });
      for await (let manager of managers) {
        manager.franchisees.remove(req.body.clientAccountId);
      }
      franchisee = await User.findOne({ _id: req.body.clientAccountId });
      profile_manager.franchisees.push(franchisee);
    } else {
      let managers = await SalesManagerProfile.find({
        franchisees: req.body.clientAccountId,
      });
      for await (let manager of managers) {
        manager.franchisees.remove(req.body.clientAccountId);
      }
      franchisee = await User.findOne({ _id: req.body.clientAccountId });
      profile_manager.franchisees.push(franchisee);
    }
  }
  if (req.body.role === "agent") {
    if (req.body.managerrole === "ClientManager") {
      managers = await ClientManagerProfile.findOne({
        agents: req.body.clientAccountId,
      });
    } else {
      managers = await SalesManagerProfile.findOne({
        agents: req.body.clientAccountId,
      });
    }
    for await (let manager of managers) {
      manager.agents.remove(req.body.clientAccountId);
    }
    let agent = await User.findOne({ _id: req.body.clientAccountId });
    profile_manager.agents.push(agent);
  }

  if (req.params.role === "parent") {
    let managers = await ClientManagerProfile.findOne({
      clients: req.body.clientAccountId,
    });

    for await (let manager of managers) {
      manager.parents.remove(req.user.id);
    }
    parent = await User.findOne({ user: req.user.id });
    profile_manager.mentors.append(parent);
  }
  if (req.body.role === "mentor") {
    if (req.body.managerrole === "ClientManager") {
      managers = await ClientManagerProfile.findOne({
        mentors: req.body.clientAccountId,
      });
    } else {
      managers = await SalesManagerProfile.findOne({
        mentors: req.body.clientAccountId,
      });
    }
    for await (let manager of managers) {
      manager.mentors.remove(req.body.clientAccountId);
    }
    mentor = await User.findOne({ _id: req.body.clientAccountId });

    profile_manager.mentors.push(mentor);
  }
  profile_manager.save();
  res.status(200).json({
    msg: "",
    data: profile_manager,
  });
};

module.exports.FranchiseeProfileListMiddleware = auth;

module.exports.FranchiseeProfileListGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.find().populate("user");
    res.status(200).json(franchisee);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};

module.exports.FranchiseeProfileDetailMiddleware = auth;

module.exports.FranchiseeProfileDetailGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.findOne({
      _id: req.params.id,
    }).populate("user");
    res.status(200).json(franchisee);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching");
  }
};

module.exports.FranchiseePackageListMiddleware = auth;

module.exports.FranchiseePackageListViewGetFunction = async (req, res) => {
  let packages = await FranchiseePackage.find();

  for await (let package of packages) {
    package.franchisee = await FranchiseeProfile.findOne({
      user: package.purchaser,
    });
    if (!package.franchisee) {
      res.status(404).send("package franchisee not found");
    }
  }
  res.json(packages);
};

module.exports.AgentProfileListListMiddleware = auth;

module.exports.AgentProfileListGetFunction = async (req, res) => {
  let agents = await AgentProfile.find().populate("user");
  res.status(200).json(agents);
};

module.exports.AgentProfileDetailMiddleware = auth;

module.exports.AgentProfileDetailGetFunction = async (req, res) => {
  try {
    let agent = await AgentProfile.findOne({ _id: req.params.id }).populate(
      "user"
    );
    res.status(200).json(agent);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};

module.exports.ParentsProfileListMiddleware = auth;

module.exports.ParentsProfileListGetFunction = async (req, res) => {
  try {
    let parents = await User.find({ role: "parent" }).populate("user");
    res.status(200).json(parents);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};

module.exports.ParentProfileDetailMiddleware = auth;

module.exports.ParentProfileDetailGetFunction = async (req, res) => {
  try {
    let parent = await User.findOne({ _id: req.params.id });
    res.status(200).json(parent);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};

module.exports.MentorProfileListMiddleware = auth;

module.exports.MentorProfileListGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find().populate("user");
    res.status(200).json(mentors);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};

module.exports.MentorProfileDetailMiddleware = auth;

module.exports.MentorProfileDetailGetFunction = async (req, res) => {
  try {
    let mentor = await MentorProfile.findOne({ _id: req.params.id }).populate(
      "user"
    );
    res.status(200).json(mentor);
  } catch {
    console.log(err);
    res.status(500).send("Error in fetching ");
  }
};
