const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken"); const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");

const MentorProfile = require("../model/MentorProfile");
const auth = require("../middleware/auth");
const ChildGroup = require("../model/ChildGroup");

module.exports.mentorProfilePostMiddleware = [
    auth,

    documentUpload.fields([
        { name: 'passportPDF', maxCount: 1 },
        { name: 'snilsPDF', maxCount: 1 },
        { name: 'innPDF', maxCount: 1 },
    ]),
  
    check("dateWork", "Please enter a valid date").isDate(),
    check("dateLayoff", "Please enter a valid date").isDate(),
    // check("passport", "Please enter a valid passport number").isPassportNumber(),
    

];


module.exports.mentorProfilePostFunction = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {        
        return res.status(400).json({
            errors: errors.array(),
        });
    }    

    let mentorInfo = { ...req.body };

    if (req.files) {
        mentorInfo.passportPDF = req.files.passportPDF.path;
        mentorInfo.snilsPDF = req.files.snilsPDF.path;
        mentorInfo.innPDF = req.files.innPDF.path;
    }

    try {

        mentorProfile = new MentorProfile({
            user: req.user.id,
            ...mentorInfo
        });

        await mentorProfile.save();  
        
        let query = { _id: req.user.id };

        await User.updateOne(query, mentorInfo, (err) => {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Error on saving" });
          }
        });
      
          res.status(200).json({
            msg: "profile created", profile: await MentorProfile.findOne({user: req.user.id})
          })

    } catch (err) {
        console.log(err);
        res.status(500).send("Error in Saving");
    }
};

module.exports.mentorProfileGetMiddleware = auth;

module.exports.mentorProfileGetFunction = async (req, res) => {

    try {
        const mentor = await MentorProfile.findOne({ user: req.user.id });
        res.json(mentor);
    } catch (err) {
        res.status(500).json({ msg: "Error in Fetching Mentor Profile" });
    }

};


module.exports.mentorProfilePatchMiddleware = [
    auth,
    documentUpload.fields([
        { name: 'passportPDF', maxCount: 1 },
        { name: 'snilsPDF', maxCount: 1 },
        { name: 'innPDF', maxCount: 1 },
    ]),

];

module.exports.mentorProfilePatchFunction = async (req, res) => {
    let mentorInfo = { ...req.body };

    if (req.files) {
        mentorInfo.passportPDF = req.files.passportPDF.path;
        mentorInfo.snilsPDF = req.files.snilsPDF.path;
        mentorInfo.innPDF = req.files.innPDF.path;
    }
    

    let query = { _id: req.user.id };

    await User.updateOne(query, mentorInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });
    
   
    let mentorQuery = { user: req.user.id };  
  
    await MentorProfile.updateOne(mentorQuery, mentorInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });
    res.status(200).json({
      msg: "Updated",
      data:{ profile: await MentorProfile.findOne(mentorQuery), user:  await User.findOne( query )},
    });







};


module.exports.mentorProfileGetDetailMiddleware = auth;

module.exports.mentorProfileGetDetailFunction = async (req, res) => {

    try {
        const mentor = await MentorProfile.findOne({ user: req.params.id });
        res.json(mentor);
    } catch (err) {
        res.status(500).json({ msg: "Error in Fetching Mentor Profile" });
    }

};


module.exports.mentorProfilePatchDetailMiddleware = [
    auth,
    documentUpload.fields([
        { name: 'passportPDF', maxCount: 1 },
        { name: 'snilsPDF', maxCount: 1 },
        { name: 'innPDF', maxCount: 1 },
    ]),

];

module.exports.mentorProfilePatchDetailFunction = async (req, res) => {
    let mentorInfo = { ...req.body };

    if (req.files) {
        mentorInfo.passportPDF = req.files.passportPDF.path;
        mentorInfo.snilsPDF = req.files.snilsPDF.path;
        mentorInfo.innPDF = req.files.innPDF.path;
    }

    let query = { _id: req.user.id };

    await MentorProfile.updateOne(query, mentorInfo, (err) => {
        if (err) {
            console.log(err);
            res.status(500).json({ error: "Error on saving" });
        }
    });
    res.status(200).json({
        msg: "Updated",
        data: MentorProfile.findOne(req.params.id),
    });
};


module.exports.mentorGroupsGetMiddleware = auth

module.exports.mentorGroupsGetFunction = async (req, res) => {
        
    try {
        // let groups = [];
        // const mentor = await MentorProfile.findOne({ user: req.user.id });
        const group = await ChildGroup.find({mentor: req.user.id});
        // groups += group;
        // mentor.groups = groups;
        res.json(group);
    } catch (err) {
        res.status(500).json({ msg: "Error in Fetching Mentor Profile", err:err });
    }
};


