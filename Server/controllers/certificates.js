const Certificate = require("../model/Certificate");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const { documentUpload } = require("../middleware/files");
module.exports.cerfictaesGetMiddleware = auth;

module.exports.certificatesFunction = async (req, res) => {
  try {
    const certificate = await Certificate.find({});
    res.json(certificate);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching certificate" });
  }
};
module.exports.certificatesPostMiddleware = [
  auth,
  documentUpload.fields([{ name: "file", maxCount: 1 }]),
];

module.exports.certificatesPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let certif = { ...req.body };

  if (req.files) {
    certif.file = req.files.file.path;
  }

  try {
    certificat = new Certificate({
      user: req.user.id,
      ...certif,
    });

    await certificat.save();

    res.status(200).json({
      msg: "certificate created",
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.cerfictaesGetOneMiddleware = auth;

module.exports.certificatesGetOneFunction = async (req, res) => {
  try {
    const certificate = await Certificate.findOne({ _id: req.params.id });
    res.json(certificate);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching certificate" });
  }
};

module.exports.certificateDetailPatchMiddleware = [
  auth,
  documentUpload.fields([{ name: "file", maxCount: 1 }]),
];
module.exports.certificateDetailPatchFunction = async (req, res) => {
  let certif = { ...req.body };

  if (req.files) {
    certif.file = req.files.file.path;
  }
  let query = { _id: req.params.id };

  await Certificate.updateOne(query, certif, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " Certificate Updated",
    data: {
      certificate: await Certificate.findOne(query),
    },
  });
};
module.exports.certificateDetailDeleteMiddleware = auth;

module.exports.certificateDetailDeleteFunction = async (req, res) => {
  await Certificate.findByIdAndDelete({ _id: req.params.id });
  res.status(200).json({
    msg: " Certificate delete",
  });
};

module.exports.cerfictaesUserGetMiddleware = auth;

module.exports.certificatesUserGetOneFunction = async (req, res) => {
  try {
    const certificate = await Certificate.find({ user: req.user.id });
    res.json(certificate);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching certificate" });
  }
};
