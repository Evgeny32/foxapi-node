const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
// const MentorProfile = require("../model/Profiles/MentorProfile");

module.exports.mentorProfileGetMiddleware = auth;

const mentorProfileGet = async (req, res, id) => {
  try {
    let profile = await MentorProfile.findOne({ user: id });
    if (profile) {
      profile.user = await User.findById(id);
      res.json(profile);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.mentorProfileGetFunction = async (req, res) => {
  mentorProfileGet(req, res, req.user.id);
};

module.exports.mentorProfilePostMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
  ]),
  check("phone", "phone is empty").not().isEmpty(),
  check("name", "name is empty").not().isEmpty(),
  check("surName", "surName is empty").not().isEmpty(),
  check("patronymic", "patronymic is empty").not().isEmpty(),
  check("email", "email is invalid").isEmail(),
  check("jobStatus", "jobStatus is empty").not().isEmpty(),
  check("passport", "passport is empty").not().isEmpty(),
  check("passportWho", "passportWho is empty").not().isEmpty(),
  check("passportWhen", "passportWhen is invalid").isDate(),
  check("address", "address is empty").not().isEmpty(),
];

module.exports.mentorProfilePostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User
  const userInfo = {
    name: req.body.name,
    surName: req.body.surName,
    patronymic: req.body.patronymic,
    email: req.body.email,
    phone: req.body.phone,
  };
  //  Инфа для обновления модели MentorProfile
  let agentInfo = {
    jobStatus: req.body.jobStatus,
    passport: req.body.passport,
    passportWho: req.body.passportWho,
    passportWhen: req.body.passportWhen,
    address: req.body.address,
  };
  //  Файлы для обновления модели MentorProfile
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        agentInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  try {
    // Проверка на почту
    let user = await User.findOne({
      email: userInfo.email,
    });
    if (user && user.id != id) {
      return res.status(411).json({
        msg: "Email Existed",
      });
    }

    user = await User.updateOne({ _id: req.user.id }, userInfo, (err) => {
      if (err) {
        return res.status(500).json({ error: "Error on saving" });
      }
    });
    agent = await AgentProfile.findOne({ user: req.user.id });
    let message = "Agent updated";
    if (agent) {
      agent = await AgentProfile({ user: req.user.id }, agentInfo, (err) => {
        if (err) {
          return res.status(500).json({ error: "Error on saving" });
        }
      });
    } else {
      message = "Agent created";
      agent = new AgentProfile({ ...agentInfo, user: req.user.id });
      await agent.save();
    }

    let agentData = await AgentProfile.findOne({ user: req.user.id });
    if (agentData) {
      agentData.user = await User.findById(req.user.id);
    }

    res.status(200).json({
      msg: message,
      data: agentData,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.mentorProfileGetByIdMiddleware = auth;

module.exports.mentorProfileGetByIdFunction = async (req, res) => {
  mentorProfileGet(req, res, req.params.id);
};
