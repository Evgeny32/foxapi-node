const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const moment = require("moment");

const User = require("../model/User");
const Message = require("../model/Support/SupportMessage");
const Theme = require("../model/Support/SupportTheme");

module.exports.supportMessagesGetMiddleware = auth;
module.exports.supportMessagesGetFunction = async (req, res) => {
  try {
    let themes = await Theme.find({ creator: req.user.id }).sort([
      ["createdAt", 1],
    ]);
    let messageList = [];
    for await (let theme of themes) {
      let themeMessages = await Message.find({ theme: theme.id })
        .populate("theme")
        .sort("sendedAt");
      themeMessages = themeMessages.map((msg) => msg.toJSON());
      messageList = [...messageList, ...themeMessages];
    }
    messageList
      .sort((a, b) => {
        moment(b.sendedAt) - moment(a.sendedAt);
      })
      .reverse();
    res.json({ msg: "Сообщения успешно получены", data: messageList });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сообщений. Обратитесь к администрации",
    });
  }
};

module.exports.supportMessagesPostMiddleware = [
  auth,
  check("theme", "Тема не может быть пустой").not().isEmpty(),
  check("role", "Пожалуйста, введите правильную роль").custom(
    (value, { req }) =>
      User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
  ),
  check("text", "Текст не может быть пустым").not().isEmpty(),
];

module.exports.supportMessagesPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  try {
    let messageInfo = {
      text: req.body.text,
      sender: req.user.id,
    };
    let theme = await Theme.findOne({
      creator: req.user.id,
      name: req.body.theme,
    });
    if (theme) {
      messageInfo.theme = theme.id;
    } else {
      theme = new Theme({
        name: req.body.theme,
        creator: req.user.id,
      });
      await theme.save();
      messageInfo.theme = theme.id;
    }
    let message = new Message({ ...messageInfo });
    await message.save();

    res.status(200).json({
      msg: "Сообщение успешно отправлено",
    });
  } catch (err) {
    res
      .status(500)
      .json({ msg: "Ошибка при отправке. Обратитесь к администратору" });
  }
};

// def post(self, request, format=None):
//         request.data['_mutable'] = True
//         request.data['theme']['creator'] = request.user.id
//         try:
//             theme = SupportTheme.objects.get(name=request.data['theme']['name'], creator=request.user.id)
//         except SupportTheme.DoesNotExist:
//             serializer = SupportThemeSerializer(data=request.data['theme'])
//             if serializer.is_valid():
//                 serializer.save()
//                 theme = SupportTheme.objects.get(name=request.data['theme']['name'], creator=request.user.id)
//             else:
//                 return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
//         request.data['msg']['theme'] = theme.id
//         request.data['_mutable'] = False

//         msg_serializer = SupportCreateSerializer(data=request.data['msg'])
//         if msg_serializer.is_valid():
//             msg_serializer.save()
//             return Response({"response": "Данные успешно добавлены."}, status=status.HTTP_201_CREATED)
//         return Response(msg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

module.exports.adminSupportMessagesGetMiddleware = auth;
module.exports.adminSupportMessagesGetFunction = async (req, res) => {
  try {
    let messages = await Message.find()
      .populate(
        {
          path: "theme",
          populate: {
            path: "creator",
            model: "user",
          },
        }

        //    {
        //     path: 'sender',
        //     model: 'user'
        //  }
      )
      .populate("sender");

    res.json({ msg: "Сообщения успешно получены", data: messages });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сообщений. Обратитесь к администрации",
    });
  }
};

module.exports.adminSupportMessagesPostMiddleware = [
  auth,
  check("theme", "Тема не может быть пустой").not().isEmpty(),
  check("role", "Пожалуйста, введите правильную роль").custom(
    (value, { req }) =>
      User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
  ),
  check("text", "Текст не может быть пустым").not().isEmpty(),
];

module.exports.adminSupportMessagesPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  try {
    let messageInfo = {
      text: req.body.text,
      sender: req.user.id,
    };

    let message = new Message({ ...messageInfo });
    await message.save();

    res.status(200).json({
      msg: "Сообщение успешно отправлено",
    });
  } catch (err) {
    res
      .status(500)
      .json({ msg: "Ошибка при отправке. Обратитесь к администратору" });
  }
};

module.exports.adminSupportThemeGetMiddleware = auth;
module.exports.adminSupportThemeGetFunction = async (req, res) => {
  try {
    let theme = await Theme.find({ id: req.params.id });
    let messages = await Message.find({ theme: theme.id });
    theme.messages = messages;
    res.json({ msg: "Сообщения успешно получены", data: theme });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении сообщений. Обратитесь к администрации",
    });
  }
};

module.exports.adminSupportThemesPostMiddleware = [
  auth,
  check("theme", "Тема не может быть пустой").not().isEmpty(),
  check("role", "Пожалуйста, введите правильную роль").custom(
    (value, { req }) =>
      User.schema.path("role").enumValues.indexOf(value) >= 0 ? true : false
  ),
  check("text", "Текст не может быть пустым").not().isEmpty(),
];

module.exports.adminSupportThemesPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  try {
    let theme = await Theme.find({
      creator: req.id,
    });

    theme.status = "Прочитано";

    await theme.save();

    let message = new Message({
      theme: theme,
      text: req.body.text,
      sender: req.user.id,
    });

    await message.save();

    res.status(200).json({
      msg: "Сообщение успешно отправлено",
    });
  } catch (err) {
    res
      .status(500)
      .json({ msg: "Ошибка при отправке. Обратитесь к администратору" });
  }
};

// class AdminSupportThemeView(APIView):
//     permission_classes = [IsAuthenticated]

//     def post(self, request, id, format=None):
//         request.data['_mutable'] = True
//         request.data['user'] = request.user.id
//         request.data['_mutable'] = False
//         theme = SupportTheme.objects.get(id=id)
//         theme.status = 'Прочитано';
//         theme.save()
//         request.data['theme'] = theme.id
//         serializer = SupportCreateSerializer(data=request.data)
//         if serializer.is_valid():
//             serializer.save()
//             return Response({"response": "Данные успешно добавлены."}, status=status.HTTP_201_CREATED)
//         else:
//             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
