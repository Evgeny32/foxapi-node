const SalesManagerProfile = require("../model/Profiles/SalesManagerProfile");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const User = require("../model/User");
const FranchiseeProfile = require("../model/Profiles/FranchiseeProfile");
const FranchiseePackage = require("../model/FranchiseePackage");

module.exports.SalesManagerProfileDetailsGetMiddleware = auth;

module.exports.SalesManagerProfileDetailsGetFunction = async (req, res) => {
  try {
    const profile = await SalesManagerProfile.findOne({
      user: req.params.id,
    }).populate("user");
    if (profile) {
      res.json(profile.toJSON());
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching SalesManagerProfile" });
  }
};
module.exports.SalesManagerProfileMeGetMiddleware = auth;

module.exports.SalesManagerProfileMeGetFunction = async (req, res) => {
  try {
    const profile = await SalesManagerProfile.findOne({
      user: req.user.id,
    }).populate("user");
    if (profile) {
      res.json(profile.toJSON());
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching SalesManagerProfile" });
  }
};
module.exports.SalesManagerProfilePostMeMiddleware = auth;

module.exports.SalesManagerProfilePostMeFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let SalesManager = { ...req.body };

  try {
    let salesManagerProfile = new SalesManagerProfile({
      user: req.user.id,
      ...SalesManager,
    });

    await salesManagerProfile.save();

    res.status(200).json({
      msg: " Saler Manager  создан",
      data: salesManagerProfile,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};
module.exports.SalesManagerProfilePatchMiddleware = auth;

module.exports.SalesManagerProfilePatchFunction = async (req, res) => {
  try {
    makeMainBranchMiddleware();

    let sales = { ...req.body };

    let query = { user: req.user.id };

    await SalesManagerProfile.updateOne(query, sales, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await SalesManagerProfile.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    json.status(500).json({ msg: "Error in patching" });
  }
};

// ОТДЕЛ ФРАНЧАЙЗИ

module.exports.SalesManagerProfileFranchiseeMeMiddleware = auth;

module.exports.SalesManagerProfileFranchiseeGetFunction = async (req, res) => {
  try {
    let franchisees = [];
    let profile = await SalesManagerProfile.findOne({
      user: req.params.id,
    }).populate("franchisees");
    //profile = profile.map((franchisee) => franchisee.toJSON());
    if (profile) {
      for await (franchisee of profile.franchisees) {
        let franchprofile = await FranchiseeProfile.findOne({
          user: franchisee._id,
        });

        franchisees.push(franchprofile);
      }
      res.status(200).json(franchisees);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
module.exports.SalesManagerFranchiseeByIdMiddleware = auth;

module.exports.SalesManagerFranchiseeByIdGetFunction = async (req, res) => {
  try {
    let franchisees = [];
    let profile = await SalesManagerProfile.findOne({ user: req.params.id });
    //profile = profile.map((franchisee) => franchisee.toJSON());
    if (profile) {
      for await (let franchisee of profile.franchisees) {
        let franchprofile = await FranchiseeProfile.findOne({
          user: franchisee._id,
        });
        franchisees.push(franchprofile);
      }
      res.status(200).json({
        msg: "franchess Sales Manager",
        data: franchisees,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
module.exports.SalesManagerFranchiseeDetailMiddleware = auth;
module.exports.SalesManagerFranchiseeDetaildGetFunction = async (req, res) => {
  try {
    let profile = await FranchiseeProfile.findOne({
      _id: req.params.id,
    }).populate("user");

    console.log(profile);

    res.status(200).json(profile);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching" });
  }
};
module.exports.SalesManagerAgentMiddleware = auth;

module.exports.SalesManagerAgentGetFunction = async (req, res) => {
  try {
    let agents = [];
    let profile = await SalesManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let agentId of profile.agents) {
        let agentprofile = await AgentProfile.find({
          user: agentId._id,
        });
        agents.push(agentprofile);
      }
      res.status(200).json(agents);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching " });
  }
};

module.exports.AgentDetailViewGetMiddleware = auth;

module.exports.AgentDetailViewGetFunction = async (req, res) => {
  try {
    let agentprofile = await AgentProfile.find({
      user: req.params.id,
    });
    res.status(200).json({
      msg: "agent",
      data: agentprofile,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.AgentDetailViewPatchMiddleware = auth;

module.exports.AgentDetailViewPatchFunction = async (req, res) => {
  let data = { ...req.body };

  try {
    let child = AgentProfile.findOne({ _id: req.params.id });
    query = { _id: req.params.body };
    await User.updateOne(query, data, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });
    res.status(200).json({
      msg: "Updated",
      data: {
        user: await User.findOne(query),
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({ msg: "child not found ", error: err });
  }
};

module.exports.SalesManagerFranchiseePackageMeMiddleware = auth;

module.exports.SalesManagerFranchiseePackageGetFunction = async (req, res) => {
  try {
    let packages = [];
    let profile = await SalesManagerProfile.findOne({ user: req.params.id });

    if (profile) {
      for await (let franchiseeId of profile.franchisees) {
        let pack = await FranchiseePackage.findOne({
          purchaser: franchiseeId._id,
        });
        packages.push(pack);
      }

      for await (let package of packages) {
        package.franchisee = await FranchiseeProfile.findOne({
          user: package.purchaser,
        });
        package.agent = {};
        if (!package.franchisee) {
          package.agent = await AgentProfile.findOne({
            user: package.purchaser,
          });
          package.franchisee = {};
          if (!package.agent) {
            package.agent = {};
            package.franchisee = {};
          }
        }
      }

      res.status(200).json(packages);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.FranchiseePackageListByIdViewMeMiddleware = auth;

module.exports.FranchiseePackageListByIdViewGetFunction = async (req, res) => {
  try {
    let packages = [];
    let profile = await SalesManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let franchiseeId of profile.franchisees) {
        pack = await FranchiseePackage.findOne({
          purchaser: franchiseeId._id,
        });
        packages.push(pack);
      }

      for await (let package of packages) {
        package.franchisee = await FranchiseeProfile.findOne({
          user: package.purchaser,
        });
        package.agent = {};
        if (!package.franchisee) {
          package.agent = await AgentProfile.findOne({
            user: package.purchaser,
          });
          package.franchisee = {};
          if (!package.agent) {
            package.agent = {};
            package.franchisee = {};
          }
        }
      }

      res.status(200).json({
        msg: "franchess Packages Sales Manager",
        data: packages,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.FranchiseePackagePatchViewMiddleware = auth;

module.exports.FranchiseePackagePatchViewFunction = async (req, res) => {
  let pack = { ...req.body };
  let query = { _id: req.params.id };

  await FranchiseePackage.updateOne(query, pack, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await FranchiseePackage.findOne(query).populate("user"),
    },
  });
};

module.exports.AllCountViewMiddleware = auth;

module.exports.AllCountViewFunction = async (req, res) => {
  try {
    let agents = await User.find({ role: "agent" });
    let franchisees = await User.find({ role: "Franchisee" });

    let count = {};
    count.agents = agents.length;
    count.franchisee = franchisees.length;

    res.status(200).json(count);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};

module.exports.AllCountByIdViewMiddleware = auth;

module.exports.AllCountByIdViewFunction = async (req, res) => {
  try {
    let agents = await User.find({ role: "agent" });
    let franchisees = await User.find({ role: "Franchisee" });

    let count = {};
    count.agents = agents.length;
    count.franchisee = franchisees.length;

    res.status(200).json(count);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};
