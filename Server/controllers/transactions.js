const Transactions = require("../model/Transactions");
const ChildProfile = require("../model/Profiles/ChildProfile");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");

module.exports.transactionGetMiddleware = auth;

module.exports.transactionsGetFunction = async (req, res) => {
  try {
    let transactions = await Transactions.find({ user: req.user.id });
    res.status(200).json(transactions);
  } catch (err) {
    res.status(500).json({ msg: "Ошибка при получении транзакции" });
  }
};
module.exports.transactionsPostMiddleware = [auth];

module.exports.transactionsPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let transaction = { ...req.body };

  try {
    transactions = new Transactions({
      user: req.user.id,
      ...transaction,
    });

    await transactions.save();

    res.status(200).json({
      msg: "Транзакция создан",
      data: transactions,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении");
  }
};

module.exports.transactionsGetOneMiddleware = auth;

module.exports.transactionsGetOneFunction = async (req, res) => {
  try {
    const transaction = await Transactions.findOne({ _id: req.params.id });
    res.json(transaction);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching transaction" });
  }
};

module.exports.certificateDetailPatchMiddleware = auth;
module.exports.certificateDetailPatchFunction = async (req, res) => {
  let transaction = { ...req.body };

  let query = { _id: req.params.id };

  await Transactions.updateOne(query, transaction, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " transaction Updated",
    data: {
      transaction: await Transactions.findOne(query),
    },
  });
};
module.exports.transactionsDetailDeleteMiddleware = auth;

module.exports.transactionsDetailDeleteFunction = async (req, res) => {
  await Transactions.findByIdAndDelete({ _id: req.params.id });
  res.status(200).json({
    msg: " transaction delete",
  });
};

module.exports.transactionsPathMiddleware = auth;

module.exports.transactionsPathFunction = async (req, res) => {
  try {
    const transaction = await Transactions.findOne({ _id: req.params.id });
    if (transaction.paid) {
      res.status(500).json({ msg: "this transaction is already paid" });
    }
    transaction.paid = true;
    transaction.order_id = req.body.order_id;
    const child = await ChildProfile.findOne({ _id: req.body.childId });
    child.foxies += transaction.value;
    await child.save();
    await transaction.save();
    res.status(200).json({
      msg: "Transaction accepted",
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching transaction" });
  }
};

module.exports.transactionsPayMiddleware = auth;

module.exports.transactionsPayFunction = async (req, res) => {
  try {
    const transaction = await Transactions.findOne({
      user: req.user.id,
      paid: false,
      _id: req.body.transaction,
    });
    transaction.paid = true;
    transaction.order_id = req.body.order_id;
    await transaction.save();
    res.status(200).json({
      msg: "Транзакция успешно проведен",
      data: transaction,
    });
    const child = await ChildProfile.findOne({ _id: req.body.child });
    if (child) {
    }
    child.foxies += transaction.value;
    await child.save();
  } catch (err) {
    res.status(500).json({ msg: "Ошибка транзакции" });
  }
};
