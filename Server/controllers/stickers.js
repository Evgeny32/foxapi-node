const Sticker = require("../model/Sticker/Sticker");
const StickerPack = require("../model/Sticker/StickerPack");
const ReceivedSticker = require("../model/Sticker/ReceivedSticker");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const { avatarUpload } = require("../middleware/files");

module.exports.stickerPacksGetMiddleware = auth;

module.exports.stickerPacksGetFunction = async (req, res) => {
  try {
    const stickerPacks = await StickerPack.find({});
    res.status(200).json({
      msg: "Stiker packs received",
      data: stickerPacks,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching StickerPacks" });
  }
};
module.exports.stickerPackPostMiddleware = [auth, avatarUpload.single("image")];
module.exports.stickerPackPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let stickerPacks = { ...req.body };

  if (req.file) {
    stickerPacks.image = req.file.path;
  }

  try {
    stickerPack = new StickerPack({
      ...stickerPacks,
    });

    await stickerPack.save();

    res.status(200).json({
      msg: "stickerPack created",
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.stickerGetMiddleware = auth;

module.exports.stickerGetFunction = async (req, res) => {
  try {
    const pack = await StickerPack.findOne({ _id: req.params.id });
    const stickers = await Sticker.find({ pack: req.params.id });
    for await (let sticker of stickers) {
      const receivedSticker = await ReceivedSticker.findOne({
        user: req.user.id,
        sticker: sticker._id,
      });
      if (!receivedSticker) {
        sticker.received = false;
      } else {
        sticker.received = true;
      }
    }
    pack.stickers = stickers;
    res.status(200).json({
      msg: "Sticker pack received",
      data: pack,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Sticker" });
  }
};
module.exports.stickerIdGetMiddleware = auth;

module.exports.stickerIdGetFunction = async (req, res) => {
  try {
    const sticker = await Sticker.find();
    for await (let sticker of stickers) {
      const receivedSticker = await ReceivedSticker.findOne({
        user: req.user.id,
        sticker: sticker._id,
      });
      if (!receivedSticker) {
        sticker.received = false;
      } else {
        sticker.received = true;
      }
    }
    res.status(200).json({
      msg: "Sticker pack received",
      data: stickers,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Sticker" });
  }
};

module.exports.stickerGetAllMiddleware = auth;

module.exports.stickerGetAllFunction = async (req, res) => {
  try {
    const sticker = await Sticker.find({});
    if (sticker) {
      const receivedSticker = await ReceivedSticker.findOne({
        user: req.user.id,
        sticker: sticker._id,
      });
      if (!receivedSticker) {
        sticker.received = false;
      } else {
        sticker.received = true;
      }

      res.status(200).json({
        msg: "Sticker received",
        data: sticker,
      });
    } else {
      res.status(409).json({ msg: "sticker not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Sticker" });
  }
};

module.exports.stickerPostMiddleware = [auth, avatarUpload.single("img")];

module.exports.stickerPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let stick = { ...req.body };
  if (req.file) {
    stick.img = req.file.path;
  }
  try {
    sticks = new Sticker({
      ...stick,
    });

    await sticks.save();

    res.status(200).json({
      msg: "Sticker created",
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.receivedStickerPostMiddleware = auth;

module.exports.receivedStickerPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    let date = {
      user: req.user.id,
      sticker: req.params.id,
    };
    const receivedSticker = await ReceivedSticker.findOne({
      user: req.user.id,
      sticker: req.params.id,
    });

    if (receivedSticker) {
      res.status(409).json({
        msg: "Sticker already received",
      });
    }

    let received_sticker = new ReceivedSticker({ date });
    await received_sticker.save();
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Sticker" });
  }
};
module.exports.unreceivedStickerMiddleware = auth;
module.exports.unreceivedStickerFunction = async (req, res) => {
  try {
    ReceivedSticker.findAndRemove({
      sticker: req.params.id,
      user: req.user.id,
    });
    res.status(200).json({ msg: "Sticker unreceived" });
  } catch {
    res.status(404).json({ msg: "mote found" });
  }
};
