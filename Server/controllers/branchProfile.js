const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");

const BranchProfile = require("../model/BranchProfile");
const FranchiseeProfile = require("../model/FranchiseeProfile");
const MentorProfile = require("../model/MentorProfile");
const ChildProfile = require("../model/ChildProfile");
const Tale = require("../model/Tale");
const auth = require("../middleware/auth");
const ChildGroup = require("../model/ChildGroup");
const { get } = require("mongoose");

module.exports.branchProfilePostMiddleware = [
  auth,

  documentUpload.fields([
    { name: "registrationSertificate", maxCount: 1 },
    { name: "accountingSertificate", maxCount: 1 },
    { name: "partnerForm", maxCount: 1 },
  ]),
];

module.exports.branchProfilePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let branchInfo = { ...req.body };

  if (req.files) {
    branchInfo.registrationSertificate = req.files.registrationSertificate.path;
    branchInfo.accountingSertificate = req.files.accountingSertificate.path;
    branchInfo.partnerForm = req.files.partnerForm.path;
  }

  try {
    branchProfile = new BranchProfile({
      user: req.user.id,
      ...branchInfo,
    });

    await branchProfile.save();

    let query = { _id: req.user.id };

    await User.updateOne(query, branchInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "profile created",
      profile: await BranchProfile.findOne({ user: req.user.id }),
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.branchProfileGetMiddleware = auth;

module.exports.branchProfileGetFunction = async (req, res) => {
  try {
    const branch = await BranchProfile.findOne({ user: req.user.id });
    res.json(branch);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Branch Profile" });
  }
};

module.exports.branchProfilePatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificate", maxCount: 1 },
    { name: "accountingSertificate", maxCount: 1 },
    { name: "partnerForm", maxCount: 1 },
  ]),
];

module.exports.branchProfilePatchFunction = async (req, res) => {
  let branchInfo = { ...req.body };

  if (req.files) {
    branchInfo.registrationSertificate = req.files.registrationSertificate.path;
    branchInfo.accountingSertificate = req.files.accountingSertificate.path;
    branchInfo.partnerForm = req.files.partnerForm.path;
  }

  let query = { _id: req.user.id };

  await User.updateOne(query, branchInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let branchQuery = { user: req.user.id };

  await BranchProfile.updateOne(branchQuery, branchInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await BranchProfile.findOne(branchQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.branchProfileDetailGetMiddleware = auth;

module.exports.branchProfileDetailGetFunction = async (req, res) => {
  try {
    const branch = await BranchProfile.findById({ user: req.params.id });
    res.json(branch);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Branch Profile" });
  }
};

module.exports.branchProfileDetailPatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificate", maxCount: 1 },
    { name: "accountingSertificate", maxCount: 1 },
    { name: "partnerForm", maxCount: 1 },
  ]),
];

module.exports.branchProfileDetailPatchFunction = async (req, res) => {
  let branchInfo = { ...req.body };

  if (req.files) {
    branchInfo.registrationSertificate = req.files.registrationSertificate.path;
    branchInfo.accountingSertificate = req.files.accountingSertificate.path;
    branchInfo.partnerForm = req.files.partnerForm.path;
  }

  let query = { _id: req.params.id };

  await User.updateOne(query, branchInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let branchQuery = { user: req.params.id };

  await BranchProfile.updateOne(branchQuery, branchInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await BranchProfile.findOne(branchQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.branchChildrenGetMiddleware = auth;

module.exports.branchChildrenGetFunction = async (req, res) => {
  try {
    let children = [];
    let mentors = await MentorProfile.find({ branch: req.user.id });
    for await (let mentor of mentors) {
      let childList = await ChildProfile.find({ mentor: mentor.user });
      for await (let child of childList) {
        let tale = await Tale.find({ child: child.id }).sort("number")[0];
        if (tale) {
          child = { ...child._doc, tale: tale.name };
        } else {
          child = { ...child._doc, tale: "Сказка не найдена" };
        }
        children = [...children, child];
      }
    }
    res.json(children);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching Branch Children" });
  }
};

module.exports.branchChildDetailGetMiddleware = auth;

module.exports.branchChildDetailGetFunction = async (req, res) => {
  try {
    let child = await ChildProfile.findOne({ child: req.params.id });
    if (!child) return res.status(404).json({ msg: "Child profile not found" });
    let mentor = await MentorProfile.findOne({ user: child.mentor });
    if (!mentor)
      return res.status(404).json({ msg: "Mentor profile not found" });
    let branch = await BranchProfile.findOne({ user: mentor.branch });
    if (!branch)
      return res.status(404).json({ msg: "Branch profile not found" });
    let franchisee = await FranchiseeProfile.findOne({
      user: branch.franchisee,
    });
    if (!franchisee)
      return res.status(404).json({ msg: "Franchisee profile not found" });
    let group = await ChildGroup.findOne({ children: { $in: child.id } });
    let childAccount = await User.findById(child.child);
    let tale = await Tale.find({ child: child.id }).sort("number")[0];
    if (tale) {
      child.tale = tale.name;
    } else {
      child.tale = "Сказка не найдена";
    }
    child = {
      ...child._doc,
      group,
      mentor,
      branch,
      franchisee,
      child: childAccount,
    };
    res.json(child);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
