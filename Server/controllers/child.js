const { upload, avatarUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");
const Group = require("../model/Group");
const ChildProfile = require("../model/Profiles/ChildProfile");

module.exports.parentChildGetMiddleware = auth;
module.exports.parentChildGetFunction = async (req, res) => {
  try {
    let children = await ChildProfile.find({ parent: req.user.id }).populate(
      "child"
    );
    if (children.length > 0) {
      children = children.map((child) => child.toJSON());
    }
    res.json(children);
  } catch (err) {
    res.status(500).json({ msg: "Ошибка сервера. Обратитесь к администрации" });
  }
};

const userCheck = async (value, { req }) => {
  let user = await User.findById(value);
  if (user) {
    return true;
  }
  return false;
};

const rankCheck = (value) =>
  ChildProfile.schema.path("rank").enumValues.indexOf(value) >= 0
    ? true
    : false;
const learnFormatCheck = (value) =>
  ChildProfile.schema.path("learnFormat").enumValues.indexOf(value) >= 0
    ? true
    : false;

const getAgeFormat = (age) => {
  if (age < 8) return "5-7";
  if (age < 13) return "8-12";
  return "13-16";
};

module.exports.childProfilePostMiddleware = [
  auth,
  check("email", "Пожалуйста, введите действующую почту").isEmail(),
  check("parent", "Пожалуйста, введите валидный ID родителя").custom(userCheck),
  check("learnFormat", "Пожалуйста, введите правильный формат обучения").custom(
    learnFormatCheck
  ),
  check("age", "Пожалуйста, введите возраст от 5 до 16 лет").if(
    (value) => value >= 5 && value <= 16
  ),
  check("surName", "Пожалуйста, введите фамилию").not().isEmpty(),
  check("name", "Пожалуйста, введите имя").not().isEmpty(),
  check("patronymic", "Пожалуйста, введите отчество").not().isEmpty(),
];
module.exports.childProfilePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let userInfo = {
    email: req.body.email,
    surName: req.body.surName,
    name: req.body.name,
    patronymic: req.body.patronymic,
  };
  let childInfo = {
    learnFormat: req.body.learnFormat,
    age: req.body.age,
    parent: req.body.parent,
    rank: req.body.rank,
  };
  try {
    // Проверка на наличие юзера
    let user = await User.findOne({
      email: userInfo.email,
    });
    if (user) {
      return res.status(411).json({
        msg: "Почта уже зарегистрирована в системе",
      });
    }

    // let password = randomString(8);
    let password = "7777";
    user = new User({
      email: userInfo.email,
      password,
      role: "child",
      surName: userInfo.surName,
      name: userInfo.name,
      patronymic: userInfo.patronymic,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    childInfo.child = user.id;
    childInfo.year = childInfo.age - 4;
    let ageFormat = getAgeFormat(req.body.age); // Возрастная группа ребенка для фильтра групп
    if (childInfo.learnFormat === "Offline") {
      let { mentor, group } = req.body;
      let childProfile = ChildProfile({
        ...childInfo,
        mentor: mentor,
      });
      await childProfile.save();
      await Group.findByIdAndUpdate(
        group,
        { $addToSet: { requests: childProfile.id } },
        (err) => {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Ошибка регистрации" });
          }
        }
      );
    } else {
      let onlineGroup = null;
      await Group.find(
        {
          online: true,
          age: ageFormat,
        },
        (err, groups) => {
          onlineGroup = groups.find((group) => group.childList.length < 50);
        }
      );
      if (onlineGroup) {
        let childProfile = ChildProfile({
          ...childInfo,
          mentor: onlineGroup.mentor,
        });
        await childProfile.save();
        await Group.findByIdAndUpdate(
          onlineGroup.id,
          { $addToSet: { requests: childProfile.id } },
          (err) => {
            if (err) {
              res.status(500).json({ error: "Error on saving" });
            }
          }
        );
      } else {
        let onlineMentor = await User.findOne(
          { role: "mentor" },
          (err, docs) => {
            if (err) {
              console.log("Ошибка при поиске ментора", err);
              res.status(500).json({ msg: "Ошибка при сохранении" });
            }
          }
        );
        let onlineGroups = await Group.find({
          online: true,
          age: ageFormat,
        });
        let childProfile = ChildProfile({
          ...childInfo,
          mentor: onlineMentor.id,
        });
        await childProfile.save();
        let newOnlineGroup = Group({
          mentor: onlineMentor.id,
          online: true,
          requests: [childProfile.id],
          age: ageFormat,
          name: `Онлайн группа №${onlineGroups.length} (${ageFormat} лет)`,
        });
        await newOnlineGroup.save();
      }
    }

    sendRegistrationEmail(userInfo.email, {
      email: userInfo.email,
      password: password,
    });

    res.json({ msg: "Ребенок успешно зарегистрирован" });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.childProfileGetMiddleware = auth;
module.exports.childProfileGetFunction = async (req, res) => {
  try {
    const child = await ChildProfile.findById(req.params.id);
    child.parent = await User.findById(child.parent);
    child.mentor = await User.findById(child.mentor);
    child.child = await User.findById(child.child);

    res.json(child);
  } catch (err) {
    res.status(500).json({ msg: "Ошибка получение данных" });
  }
};
module.exports.childUserGetMiddleware = auth;
module.exports.childUseGetFunction = async (req, res) => {
  try {
    let childUser = await User.findById(req.params.id);
    let child = await ChildProfile.findOne({ child: childUser });
    console.log(child);
    res.status(200).json(child);
  } catch (err) {
    res.status(500).json({ msg: "Ошибка получение данных" });
  }
};

module.exports.childProfilePatchMiddleware = [auth];
module.exports.childProfilePatchFunction = async (req, res) => {
  let childInfo = { ...req.body };
  let query = { _id: req.params.id };

  if (childInfo.rank && !rankCheck(childInfo.rank)) {
    return res.status(400).json({ msg: "Not valid rank" });
  }
  if (childInfo.learnFormat && !learnFormatCheck(childInfo.learnFormat)) {
    return res.status(400).json({ msg: "Not valid learn format" });
  }

  await ChildProfile.updateOne(query, childInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Изменено",
    data: await ChildProfile.findById(req.params.id),
  });
};

module.exports.childMeGetMiddleware = auth;
module.exports.childMeGetFunction = async (req, res) => {
  try {
    const child = await ChildProfile.findOne({ child: req.user.id }).populate(
      "child"
    );
    res.json(child.toJSON());
  } catch (err) {
    res.status(500).json({ msg: "Ошибка при получении данных" });
  }
};
