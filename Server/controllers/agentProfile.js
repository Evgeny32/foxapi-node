const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");
const AgentProfile = require("../model/AgentProfile");
const auth = require("../middleware/auth");

module.exports.agentProfilePostMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
  ]),

  check("birthday", "Please enter a valid date").isDate(),
  check("emailBuch", "Please enter a valid email").isEmail(),
  // check("passport", "Please enter a valid email").isPassportNumber()
];

module.exports.agentProfilePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let agentInfo = { ...req.body };

  if (req.files) {
    agentInfo.passportPDF = req.files.passportPDF.path;
    agentInfo.snilsPDF = req.files.snilsPDF.path;
    agentInfo.innPDF = req.files.innPDF.path;
    agentInfo.resumePDF = req.files.resumePDF.path;
  }

  try {
    agentProfile = new AgentProfile({
      user: req.user.id,
      ...agentInfo,
    });

    await agentProfile.save();

    let query = { _id: req.user.id };

    await User.updateOne(query, agentInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "profile created",
      profile: await AgentProfile.findOne({ user: req.user.id }),
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.agentProfileGetMiddleware = auth;

module.exports.agentProfileGetFunction = async (req, res) => {
  try {
    const agent = await AgentProfile.findOne({ user: req.user.id });
    res.json(agent);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Agent Profile" });
  }
};

module.exports.agentProfilePatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
  ]),
];

module.exports.agentProfilePatchFunction = async (req, res) => {
  let agentInfo = { ...req.body };

  if (req.files) {
    agentInfo.passportPDF = req.files.passportPDF.path;
    agentInfo.snilsPDF = req.files.snilsPDF.path;
    agentInfo.innPDF = req.files.innPDF.path;
    agentInfo.resumePDF = req.files.resumePDF.path;
  }

  let query = { _id: req.user.id };

  await User.updateOne(query, agentInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let agentQuery = { user: req.user.id };

  await AgentProfile.updateOne(agentQuery, agentInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await AgentProfile.findOne(agentQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.agentProfileGetDetailMiddleware = [auth];

module.exports.agentProfileGetDetailFunction = async (req, res) => {
  try {
    const agent = await AgentProfile.findOne({ user: req.params.id }).populate(
      "user"
    );
    res.json(agent);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Agent Profile" });
  }
};

module.exports.agentProfilePatchDetailMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
  ]),
];

module.exports.agentProfilePatchDetailFunction = async (req, res) => {
  let agentInfo = { ...req.body };

  if (req.files) {
    agentInfo.passportPDF = req.files.passportPDF.path;
    agentInfo.snilsPDF = req.files.snilsPDF.path;
    agentInfo.innPDF = req.files.innPDF.path;
    agentInfo.resumePDF = req.files.resumePDF.path;
  }

  let query = { _id: req.params.id };

  await User.updateOne(query, agentInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let agentQuery = { user: req.params.id };

  await AgentProfile.updateOne(agentQuery, agentInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await AgentProfile.findOne(agentQuery),
      user: await User.findOne(query),
    },
  });
};
