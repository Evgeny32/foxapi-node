const Notification = require("../model/Notification");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

module.exports.notificationGetMeMiddleware = auth;

module.exports.notificationGetMeFunction = async (req, res) => {
  try {
    const notification = await Notification.find({ user: req.user.id });
    res.json(notification.json());
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching StickerPacks" });
  }
};
module.exports.notificationPostMiddleware = [auth];

module.exports.notificationPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let notification = { ...req.body };

  try {
    const notifications = new Notification({
      user: req.user.id,
      ...notification,
    });

    await notifications.save();

    res.status(200).json({
      msg: "notifications created",
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};
module.exports.lastNotificationGetMeMiddleware = auth;

module.exports.lastNotificationGetMeGetFunction = async (req, res) => {
  try {
    const notification = await Notification.find({ user: req.user.id })
      .limit(5)
      .sort({ date: -1 });
    res.json(notification);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching StickerPacks" });
  }
};
module.exports.notificationGetIdMeMiddleware = auth;
module.exports.notificationGetIdFunction = async (req, res) => {
  try {
    const notification = await Notification.findOne({ _id: req.params.id });
    res.status(200).json({
      msg: "Notification updated",
      data: notification,
    });
  } catch (err) {
    res.status(409).json({ msg: "not found" });
  }
};

module.exports.notificationPatchMiddleware = auth;
module.exports.notificationDetailPatchFunction = async (req, res) => {
  let notification = { ...req.body };

  let query = { _id: req.params.id };

  await Notification.updateOne(query, notification, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " notification Updated",
    data: {
      transaction: await Notification.findOne(query),
    },
  });
};
module.exports.notificationDeleteMiddleware = auth;

module.exports.notificationlDeleteFunction = async (req, res) => {
  await Notification.findByIdAndDelete({ _id: req.params.id });
  res.status(200).json({
    msg: " Notification delete",
  });
};
