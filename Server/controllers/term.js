const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const Term = require("../model/Term");

module.exports.mentorTermsGetMiddleware = auth;

module.exports.mentorTermsGetFunction = async (req, res) => {
  try {
    let terms = await Term.find({ mentor: req.params.id });
    res.json({ msg: "Термины успешно получены", data: terms });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении терминов. Обратитесь к администрации",
    });
  }
};

module.exports.childTermsGetMiddleware = auth;

module.exports.childTermsGetFunction = async (req, res) => {
  try {
    let terms = await Term.find({ child: { $in: req.params.id } });
    terms = terms.map((term) => term.toJSON());
    res.json({ msg: "Термины успешно получены", data: terms });
  } catch (err) {
    res.status(500).json({
      msg: "Ошибка при получении терминов. Обратитесь к администрации",
    });
  }
};

module.exports.termsPostMiddleware = [
  auth,
  check("title", "Название не может быть пустым").not().isEmpty(),
  check("mentor", "Термин не может быть создан без ментора").not().isEmpty(),
];

module.exports.termsPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User
  let termInfo = {
    title: req.body.title,
    mentor: req.body.mentor,
  };
  let descriptions = [
    "descriptionGeneral",
    "description1",
    "description2",
    "description3",
  ];
  for await (let desc of descriptions) {
    if (req.body[desc]) {
      termInfo[desc] = req.body[desc];
    }
  }
  //   Проверка на наличие описания у термина
  if (Object.entries(termInfo).length < 3) {
    res.status(400).json({ msg: "Нельзя создать термин без описания" });
  }

  try {
    let term = new Term({ ...termInfo });
    await term.save();

    res.status(200).json({
      msg: "Термин успешно создан",
      data: await Term.findOne(termInfo),
    });
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Ошибка при сохранении. Обратитесь к администратору" });
  }
};

module.exports.termPatchByIdMiddleware = auth;

module.exports.termPatchByIdFunction = async (req, res) => {
  try {
    Term.findByIdAndUpdate(req.params.id, req.body, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Термин успешно обновлен",
    });
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Ошибка при обновлении. Обратитесь к администратору" });
  }
};
