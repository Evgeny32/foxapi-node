const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");

const User = require("../model/User");
const FranchiseeProfile = require("../model/FranchiseeProfile");
const BranchProfile = require("../model/Profiles/BranchProfile");

const auth = require("../middleware/auth");
const transporter = require("../config/mail");
const fs = require("fs");
const path = require("path");
const MentorProfile = require("../model/Profiles/MentorProfile");
const FranchiseePackage = require("../model/FranchiseePackage");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sendRegistrationEmail = require("../mails/registration");
const Tale = require("../model/Tale");
const ChildGroup = require("../model/ChildGroup");
const ChildProfile = require("../model/Profiles/ChildProfile");
const Coaching = require("../model/Coaching");
const Group = require("../model/Group");

module.exports.franchBuchAccountMiddleware = auth;
module.exports.franchBuchAccountFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { email, role } = req.body;

  let user = await User.findOne({
    email: email,
  });
  if (user) {
    return res.status(411).json({
      msg: "Email Already Registered",
    });
  }

  try {
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    sendRegistrationEmail(email, { email: email, password: password });
    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      // {
      //   expiresIn: 10000,
      // },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.franchiseeProfilePutMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "decisionPDF", maxCount: 1 },
    { name: "protocolPDF", maxCount: 1 },
    { name: "organizationPDF", maxCount: 1 },
    { name: "ipPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "oqrnipPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
    { name: "contractPDF", maxCount: 1 },
  ]),
  // check("passport", "Please enter a valid passport number").isPassportNumber(),
  //check("passportWhen", "Please enter a valid date").isDate(),
  // check("emailBuch", "Please enter a valid email").isEmail()
];

// module.exports.franchiseeProfilePostFunction = async (req, res) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({
//       errors: errors.array(),
//     });
//   }

//   let franchiseeInfo = { ...req.body };

//   // if (req.files) {
//   //   franchiseeInfo.passportPDF = req.files.passportPDF.path;
//   //   franchiseeInfo.decisionPDF = req.files.decisionPDF.path;
//   //   // franchiseeInfo.protocolPDF = req.files.protocolPDF.path;
//   //   // franchiseeInfo.organizationPDF = req.files.organizationPDF.path;
//   //   franchiseeInfo.ipPDF = req.files.ipPDF.path;
//   //   franchiseeInfo.innPDF = req.files.innPDF.path;
//   //   franchiseeInfo.oqrnipPDF = req.files.oqrnipPDF.path;
//   //   franchiseeInfo.resumePDF = req.files.resumePDF.path;
//   //   franchiseeInfo.contractPDF = req.files.contractPDF.path;
//   // }
//   if (req.files) {
//     for await (let fileItem of Object.entries(req.files)) {
//       if (fileItem[1].length) {
//         franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
//       }
//     }
//   }
//   let buh=User.findOne((email = req.body.emailBuch));
//   franchiseeInfo.emailBuch=buh;

//   try {
//     franchiseeProfile = new FranchiseeProfile({
//       user: req.user.id,
//       ...franchiseeInfo,
//     });

//     await franchiseeProfile.save();

//     let query = { _id: req.user.id };

//     await User.updateOne(query, franchiseeInfo, (err) => {
//       if (err) {
//         console.log(err);
//         res.status(500).json({ error: "Error on saving" });
//       }
//     });

//     res.status(200).json({
//       msg: "profile created",
//       profile: await FranchiseeProfile.findOne({ user: req.user.id }),
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Error in Saving");
//   }
// };

module.exports.franchiseeProfilePutFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  // Инфа для обновления модели User
  const userInfo = {
    name: req.body.name,
    email: req.body.email,
  };
  //  Инфа для обновления модели AgentProfile

  // let franchiseeInfo = {
  //   inn: req.body.inn,
  //   oqrn: req.body.oqrn,
  //   kpp: req.body.kpp,
  //   emailBuch: req.body.emailBuch,
  //   passport: req.body.passport,
  //   passportWho: req.body.passportWho,
  //   passportWhen: req.body.passportWhen,
  //   director: req.body.director,
  //   address: req.body.address,
  //   directorPost: req.body.directorPost,
  //   urAdress: req.body.urAdress,
  //   adress: req.body.adress,
  //   rs: req.body.cs,
  //   cs: req.body.cs,
  //   bik: req.body.bik,
  //   bank: req.body.bank,
  // };
  //   let buh=User.findOne((email = req.body.emailBuch))
  //  franchiseeInfo.emailBuch=buh
  let franchiseeInfo = { ...req.body };
  console.log(franchiseeInfo.emailBuch);
  buh = await User.findOne({ email: franchiseeInfo.emailBuch });
  console.log(franchiseeInfo);
  if (buh) {
    console.log(franchiseeInfo.emailBuch);
    franchiseeInfo.emailBuch = buh._id;
  }

  // console.log(franchiseeInfo.emailBuch)
  //  Файлы для обновления модели AgentProfile
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  try {
    // Проверка на почту
    // let user = await User.findOne({
    //   email: franchiseeInfo.email,
    // });
    // if (user && user.id != req.user.id) {
    //   return res.status(411).json({
    //     msg: "Email Existed",
    //   });
    // }
    // Проверка на почту бухгалтераbuh
    // User.findOne((email = req.body.emailBuch));

    // let franchisee = await FranchiseeProfile.findOne({
    //   buch: franchiseeInfo.emailBuch,
    // });
    // if (franchisee && franchisee.user != req.user.id) {
    //   return res.status(411).json({
    //     msg: "Buch Email Existed",
    //   });
    // }

    // user = await User.updateOne({ _id: req.user.id }, userInfo, (err) => {
    //   if (err) {
    //     return res.status(500).json({ error: "Error on saving" });
    //   }
    // });

    franchisee = await FranchiseeProfile.findOne({ user: req.user.id });
    let message = "franchisee updated";
    if (franchisee) {
      let franhiseeQuery = { user: req.user.id };

      await FranchiseeProfile.updateOne(
        franhiseeQuery,
        franchiseeInfo,
        (err) => {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Error on saving" });
          }
        }
      );
    } else {
      message = "franchisee created";

      franchisee = new FranchiseeProfile({
        ...franchiseeInfo,
        user: req.user.id,
      });
      await franchisee.save();
      console.log(3);
    }

    // let message = "franchisee updated";
    // if (franchisee) {
    //   franchisee = await FranchiseeProfile(
    //     { user: req.user.id },
    //     { ...franchiseeInfo },
    //     (err) => {
    //       if (err) {
    //         return res.status(500).json({ error: "Error on saving" });
    //       }
    //     }
    //   );
    //   console.log(1);
    //   console.log(franchisee);
    //   console.log(franchiseeInfo);

    //   await franchisee.save();
    //   console.log(2);
    //   console.log(franchiseeInfo);

    // } else {
    //   message = "franchisee created";

    //   franchisee = new FranchiseeProfile({
    //     ...franchiseeInfo,
    //     user: req.user.id,
    //   });
    //   await franchisee.save();
    //   console.log(3);

    // }

    let franchiseeData = await FranchiseeProfile.findOne({ user: req.user.id });
    if (franchiseeData) {
      franchiseeData.user = await User.findById(req.user.id);
    }

    res.status(200).json({
      msg: message,
      data: franchiseeData,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.franchiseeProfileGetMiddleware = auth;

module.exports.franchiseeProfileGetFunction = async (req, res) => {
  try {
    const franchisee = await FranchiseeProfile.findOne({ user: req.user.id });
    res.status(200).json(franchisee);
    console.log(franchisee);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching Franchisee Profile" });
  }
};

module.exports.franchiseeProfilePatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "decisionPDF", maxCount: 1 },
    { name: "protocolPDF", maxCount: 1 },
    { name: "organizationPDF", maxCount: 1 },
    { name: "ipPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "oqrnipPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
    { name: "contractPDF", maxCount: 1 },
  ]),
];

module.exports.franchiseeProfilePatchFunction = async (req, res) => {
  let franchiseeInfo = { ...req.body };

  buh = User.findOne((email = req.body.emailBuch));
  franchiseeInfo.emailBuch = buh._id;
  // if (req.files) {
  //   franchiseeInfo.passportPDF = req.files.passportPDF.path;
  //   franchiseeInfo.decisionPDF = req.files.decisionPDF.path;
  //   franchiseeInfo.protocolPDF = req.files.protocolPDF.path;
  //   franchiseeInfo.organizationPDF = req.files.organizationPDF.path;
  //   franchiseeInfo.ipPDF = req.files.ipPDF.path;
  //   franchiseeInfo.innPDF = req.files.innPDF.path;
  //   franchiseeInfo.oqrnipPDF = req.files.oqrnipPDF.path;
  //   franchiseeInfo.resumePDF = req.files.resumePDF.path;
  //   franchiseeInfo.contractPDF = req.files.contractPDF.path;
  // }
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }
  let query = { _id: req.user.id };

  await User.updateOne(query, franchiseeInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let franchiseeQuery = { user: req.user.id };

  let front = FranchiseeProfile.updateOne(franchiseeQuery, franchiseeInfo);
  if (!front) {
    franchiseeProfile = new FranchiseeProfile({
      user: req.user.id,
      ...franchiseeInfo,
    });

    await franchiseeProfile.save();
  }
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await FranchiseeProfile.findOne(franchiseeQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.franchiseeProfileDetailGetMiddleware = auth;

module.exports.franchiseeProfileDetailGetFunction = async (req, res) => {
  try {
    const franchisee = await FranchiseeProfile.findById({
      user: req.params.id,
    });
    res.json(franchisee);
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Error in Fetching Franchisee Profile", error: err });
  }
};

module.exports.franchiseeProfileDetailPatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "decisionPDF", maxCount: 1 },
    { name: "protocolPDF", maxCount: 1 },
    { name: "organizationPDF", maxCount: 1 },
    { name: "ipPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "oqrnipPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
    { name: "contractPDF", maxCount: 1 },
  ]),
];

module.exports.franchiseeProfileDetailPatchFunction = async (req, res) => {
  let franchiseeInfo = { ...req.body };
  buh = User.findOne((email = req.body.emailBuch));
  franchiseeInfo.emailBuch = buh._id;
  if (req.files) {
    franchiseeInfo.passportPDF = req.files.passportPDF.path;
    franchiseeInfo.decisionPDF = req.files.decisionPDF.path;
    franchiseeInfo.protocolPDF = req.files.protocolPDF.path;
    franchiseeInfo.organizationPDF = req.files.organizationPDF.path;
    franchiseeInfo.ipPDF = req.files.ipPDF.path;
    franchiseeInfo.innPDF = req.files.innPDF.path;
    franchiseeInfo.oqrnipPDF = req.files.oqrnipPDF.path;
    franchiseeInfo.resumePDF = req.files.resumePDF.path;
    franchiseeInfo.contractPDF = req.files.contractPDF.path;
  }
  // if (req.files) {
  //   for await (let fileItem of Object.entries(req.files)) {
  //     if (fileItem[1].length) {
  //       franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
  //     }
  //   }
  // }
  let query = { _id: req.user.id };

  await User.updateOne(query, franchiseeInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let franchiseeQuery = { user: req.user.id };

  await FranchiseeProfile.updateOne(franchiseeQuery, franchiseeInfo, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await FranchiseeProfile.findOne(franchiseeQuery),
      user: await User.findOne(query),
    },
  });
};
module.exports.BranchListViewMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificatePDF", maxCount: 1 },
    { name: "accountingSertificatePDF", maxCount: 1 },
    { name: "partnerFormPDF", maxCount: 1 },
  ]),
];

module.exports.BranchListViewFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const branch = { ...req.body };
  console.log(branch);
  const role = "branch";
  const email = branch.email;

  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        branch[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  let user = await User.findOne({
    email: email,
  });
  if (user) {
    return res.status(411).json({
      msg: "Email Already Registered",
    });
  }
  console.log(branch);
  try {
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    console.log(user._id);

    sendRegistrationEmail(email, { email: email, password: password });
    // const data = {
    //   user: {
    //     id: user.id,
    //   },
    // };
    profile = new BranchProfile({ user: user._id, ...branch });
    await profile.save();

    // jwt.sign(
    //   data,
    //   "randomString",
    //   // {
    //   //   expiresIn: 10000,
    //   // },
    //   (err, token) => {
    //     if (err) throw err;
    //     res.status(200).json({
    //       token,
    //     });
    //   }
    // );
    res.status(200).json({
      user: user,
      profile: profile,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};
module.exports.BranchListViewGetMiddleware = auth;

module.exports.BranchListViewGetFunction = async (req, res) => {
  try {
    let branches = await BranchProfile.find({
      franchisee: req.user.id,
    }).populate("user");
    branches = branches.map((branch) => branch.toJSON());
    for await (branch of branches) {
      console.log(branch.user.id);
      let mentorAll = await MentorProfile.find({ branch: branch.user.id });
      console.log(mentorAll);
      let childrenAll = 0;
      let groupAll = 0;

      mentorAll = mentorAll.map((mentor) => mentor.toJSON());

      for await (mentor of mentorAll) {
        console.log(branch);
        let children = await ChildProfile.find({ mentor: mentor.user });

        children = children.map((child) => child.toJSON());
        console.log(children);
        childrenAll += children.length;

        let groups = await Group.find({ mentor: mentor.user });

        groups = groups.map((group) => group.toJSON());
        console.log(groups);
        groupAll += groups.length;
      }

      branch.mentors = mentorAll.length;
      branch.children = childrenAll;
      branch.groups = groupAll;
      branch.all = 0;
      console.log(branch.children);
    }
    // for await (let branch of branches) {
    //   let mentor_all = await MentorProfile.find({ branch: branch.user._id });
    //   let children_all;
    //   let group_all;
    //   for await (let mentor of mentor_all) {
    //     let children = await ChildProfile.find({ mentor: mentor.user._id });
    //     children_all += children.length;
    //     let groups = await ChildGroup.find({ mentor: mentor.user._id });
    //     group_all += groups.length;
    //     branch.mentors = mentor_all.length;
    //     branch.children = children_all;
    //     branch.groups = group_all;
    //     branch.all = 0;
    //   }
    // }
    res.json(branches);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.BranchAdminListViewMiddleware = auth;
module.exports.BranchAdminListViewFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const {
    email,
    name,
    franchisee,
    region,
    director,
    registrationSertificatePDF,
    accountingSertificatePDF,
    partnerFormPDF,
  } = req.body;
  try {
    // Проверка на наличие юзера

    //let franchiseeAccount = await User.findOne({ _id: franchisee });

    // Проверка на наличие юзера

    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email: req.body.email,
      password,
      role: "Branch",
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    let branchAccount = await User.findOne({ email: email });
    let packages = await User.findOne({ _id: package });
    let profile = new BranchProfile({
      region,
      director,
      registrationSertificatePDF,
      accountingSertificatePDF,
      partnerFormPDF,
      name,
      //franchisee: franchiseeAccount,
      user: branchAccount,
    });
    await profile.save();
    sendRegistrationEmail(email, { email: email, password: password });
    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      // {
      //   expiresIn: 10000,
      // },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};
module.exports.BranchViewGetMiddleware = auth;

module.exports.BranchViewGetFunction = async (req, res) => {
  try {
    const branch = await BranchProfile.findById({
      user: req.params.id,
    });
    res.status(200).json(branch);
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Error in Fetching branch Profile", error: err });
  }
};

module.exports.BranchViewPatchMiddleware = [auth];

module.exports.BranchViewPatchFunction = async (req, res) => {
  let branch = { ...req.body };

  let query = { _id: req.user.id };

  await User.updateOne(query, branch, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let branchQuery = { user: req.user.id };

  await BranchProfile.updateOne(branchQuery, branch, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await BranchProfile.findOne(branchQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.franchBuchAccountMiddleware = auth;
module.exports.franchBuchAccountFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { email, role } = req.body;

  let user = await User.findOne({
    email: email,
  });
  if (user) {
    return res.status(411).json({
      msg: "Email Already Registered",
    });
  }

  try {
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    sendRegistrationEmail(email, { email: email, password: password });
    const data = {
      user: {
        id: user.id,
      },
    };

    // jwt.sign(
    //   data,
    //   "randomString",
    //   // {
    //   //   expiresIn: 10000,
    //   // },
    //   (err, token) => {
    //     if (err) throw err;
    //     res.status(200).json({
    //       token,
    //     });
    //   }
    // );
    res.status(200).json({
      data: user,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.BranchViewPatchMiddleware = [auth];

module.exports.BranchViewPatchFunction = async (req, res) => {
  let branch = { ...req.body };

  let query = { _id: req.user.id };

  await User.updateOne(query, branch, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let branchQuery = { user: req.user.id };

  await BranchProfile.updateOne(branchQuery, branch, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await BranchProfile.findOne(branchQuery),
      user: await User.findOne(query),
    },
  });
};

module.exports.BranchAdminListViewPostMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificatePDF", maxCount: 1 },
    { name: "accountingSertificatePDF", maxCount: 1 },
    { name: "partnerFormPDF", maxCount: 1 },
  ]),
  check("name", "name is empty").not().isEmpty(),
  check("region", "region is empty").not().isEmpty(),
  check("director", "director is empty").not().isEmpty(),
  check("email", "email is invalid").isEmail(),
];
module.exports.BranchAdminListViewPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const branch = { ...req.body };
  const role = "branch";
  const email = branch.email;

  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        branch[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  let user = await User.findOne({
    email: email,
  });
  if (user) {
    return res.status(411).json({
      msg: "Email Already Registered",
    });
  }
  console.log(branch);
  try {
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    console.log(user._id);

    sendRegistrationEmail(email, { email: email, password: password });
    // const data = {
    //   user: {
    //     id: user.id,
    //   },
    // };
    profile = new BranchProfile({ user: user._id, ...branch });
    await profile.save();

    // jwt.sign(
    //   data,
    //   "randomString",
    //   // {
    //   //   expiresIn: 10000,
    //   // },
    //   (err, token) => {
    //     if (err) throw err;
    //     res.status(200).json({
    //       token,
    //     });
    //   }
    // );
    res.status(200).json({
      user: user,
      profile: profile,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};
module.exports.FranchBuchAccountPatchViewMiddleware = [auth];

module.exports.FranchBuchAccountPatchView = async (req, res) => {
  let buch = { ...req.body };

  let query = { email: req.params.email };

  await User.updateOne(query, buch, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  res.status(200).json({
    msg: "Updated",
    data: {
      user: await User.findOne(query),
    },
  });
};

module.exports.MentorListViewGetMiddleware = auth;

module.exports.MentorListViewGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find({
      branch: req.params.id,
    }).populate("user");

    mentors = mentors.map((mentors) => mentors.toJSON());
    for await (mentor of mentors) {
      //console.log(mentor.user.id);
      let groups = Group.find({ mentor: mentor.user.id });
      let children = 0;
      for await (let grp of groups) {
        let groupChild = grp.childList;

        children += groupChild.length;

        mentor.children = children;
        trainings = await Coaching.find({
          mentor: mentor.user._id,
        });
        mentor.training = trainings.length;
        mentor.groups = (await groups).length;
        console.log((await groups).length);

        mentor.all = 0;
        mentor.children = children;
      }
    }

    res.json(mentors);
  } catch (err) {
    console.log(err);
    res
      .status(500)
      .json({ msg: "Error in Fetching Franchisee Profile", error: err });
  }
};

module.exports.MentorViewGetMiddleware = auth;

module.exports.MentorViewGetFunction = async (req, res) => {
  try {
    const mentor = await MentorProfile.findOne({
      branch: req.params.id,
    });
    res.json(mentor);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.MentorGroupListViewMiddleware = auth;

module.exports.MentorGroupListViewGetFunction = async (req, res) => {
  try {
    const mentors = await MentorProfile.find({
      branch: req.params.id,
    });
    mentors = mentors.map((mentor) => mentor.toJSON());
    for await (mentor of mentors) {
      groups = ChildGroup.find({ mentor: mentor.user._id });
    }

    res.json(groups);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.BranchChildListViewMiddleware = auth;

module.exports.BranchChildListViewGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find({
      branch: req.params.id,
    });
    children = [];
    mentors = mentors.map((mentor) => mentor.toJSON());
    for await (mentor of mentors) {
      let child_list = await ChildProfile.find({
        mentor: mentor.user._id,
      }).populate("child");
      //(await groups).length;
      console.log(child_list);
      child_list = child_list.map((child) => child.toJSON());
      for await (child of child_list) {
        let tale = Tale.find({ child: child._id }).sort({ number: -1 });
        if (tale) {
          tale = tale.name;
        } else {
          tale = "Сказка не найдена";
        }
      }
      children.push(child_list);
    }

    res.json(children);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};
module.exports.BranchChildViewMiddleware = auth;

module.exports.BranchChildViewGetFunction = async (req, res) => {
  try {
    const branch = await BranchProfile.find({
      user: req.user.id,
    });

    const child = ChildProfile.findOne({ mentor: mentor.user._id });

    const tale = Tale.find({ child: child._id }).sort({ number: -1 });
    if (tale) {
      tale = tale.name;
    } else {
      tale = "Сказка не найдена";
    }
    const group = await BranchProfile.findOne({
      children__in: child.id,
    });
    if (group) {
      group = group.name;
    } else {
      group = "Группа не найдена";
    }
    child.branch = branch;
    child.tale = tale;
    child.group = group;

    res.json(child);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.FranchiseeAllInfoViewMiddleware = auth;

module.exports.FranchiseeAllInfoViewFunction = async (req, res) => {
  try {
    let packages = await FranchiseePackage.find();

    for await (let package of packages) {
      try {
        package.franchisee = await FranchiseeProfile.findOne({
          user: package.purchaser,
        });
      } catch (err) {
        package.franchisee = null;
      }
    }

    res.json(packages);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.FranchiseeAllInfoDetailViewMiddleware = auth;

module.exports.FranchiseeAllInfoDetailViewFunction = async (req, res) => {
  try {
    const package = await FranchiseePackage.findById({ _id: req.params.id });

    try {
      package.franchisee = FranchiseeProfile.findOne({
        user: package.purchaser,
      });
    } catch (err) {
      package.franchisee = null;
    }

    res.json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.FranchiseeOtherPackageListViewMiddleware = auth;

module.exports.FranchiseeOtherPackageListViewFunction = async (req, res) => {
  try {
    const package = await FranchiseePackage.find({ purchaser: req.params.id });
    res.json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.FranchiseePackageViewMiddleware = auth;

module.exports.FranchiseePackageViewFunction = async (req, res) => {
  try {
    const package = await FranchiseePackage.findById({ _id: req.params.id });

    try {
      const franchisee = FranchiseeProfile.findOne({
        user: package.purchaser._id,
      });
      package.franchisee = franchisee;
    } catch (err) {
      res.status(500).json({ msg: "Error in doesnt exist ", error: err });
    }

    res.status(200).json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.FranchiseePackagePayViewMiddleware = auth;

module.exports.FranchiseePackagePayViewFunction = async (req, res) => {
  try {
    let query = { _id: req.params.id };
    let packageInfo = { ...req.body };
    let package = await FranchiseePackage.updateOne(query, packageInfo);
    package.status = 2;
    package = await FranchiseePackage.updateOne(query, package);
    //let package = await FranchiseePackage.findOne({ _id: req.params.id });
    console.log(packageInfo);
    let emailHtml = fs.readFileSync(
      path.join(__dirname + `/index.html`),
      "utf-8"
    );

    emailHtml = emailHtml
      .replace("#contractFile", package.contractFile)
      .replace("#billFile", package.billFile)
      .replace("#aktFile", package.aktFile);

    emailText = "Документы для оплаты счета";

    const emailData = {
      from: '"Business Fox" ' + process.env.SMTP_LOGIN,
      to: "tafzo@hotmail.ru",
      subject: "Документы успешно отправились!",
      text: "Отправляем документы для по полаченному счету. Документы необходимо заполнить и отправить почтой в ПинКод.",
      html: emailHtml,
      attachments: [
        // {
        //   filename: "Договор",
        //   path: fs.readFile(package.contractFile, (err, data) => {
        //     if (err) throw err;
        //     console.log(package);
        //   }),
        // },
        // {
        //   filename: "Акт",
        //   path: fs.readFile(package.aktFile, (err, data) => {
        //     if (err) throw err;
        //     console.log(package);
        //   }),
        // },
        // {
        //   filename: "Счет",
        //   path: fs.readFile(package.billFile, (err, data) => {
        //     if (err) throw err;
        //     console.log(package);
        //   }),
        // },
      ],
    };

    transporter.sendMail(emailData);
    res.status(200).json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};
