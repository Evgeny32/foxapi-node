const auth = require("../middleware/auth");

const AdminCalcData = require("../model/AdminCalcData");
const User = require("../model/User");
const AgentProfile = require("../model/AgentProfile");
const FranchiseePackage = require("../model/FranchiseePackage");

module.exports.adminCalcDataGetMiddleware = auth;

module.exports.adminCalcDataGetFunction = async (req, res) => {
  try {
    let data = await AdminCalcData.find().sort()[0];

    res.status(200).json(data);
  } catch (err) {
    console.log(err);
    res.status(500).json("");
  }
};

module.exports.adminCalcDataPatchMiddleware = auth;

module.exports.adminCalcDataPatchFunction = async (req, res) => {
  try {
    let calcData = { ...req.body };

    let data = await AdminCalcData.find().sort()[0];

    if (data) {
      await AdminCalcData.updateOne(data, calcData, (err) => {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Error on saving" });
        }
      });
    }

    res.status(200).json({
      msg: "Updated",
      data: { profile: await AdminCalcData.findOne(data) },
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching calc data", error: err });
  }
};
