const { check, validationResult } = require("express-validator");

const auth = require("../middleware/auth");
const ChildGroup = require("../model/ChildGroup");

module.exports.childGroupPostMiddleware = [
    auth,

    check("ageGroup", "Please enter a valid age group").custom((value, { req }) =>
    ChildGroup.schema.path("ageGroup").enumValues.indexOf(value) >= 0 ? true : false),

];


module.exports.childGroupPostFunction = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {        
        return res.status(400).json({
            errors: errors.array(),
        });
    }

    let childGroupInfo = { ...req.body };
    
    try {
        // Проверка на наличие группы

        childGroup = new ChildGroup({
            ...childGroupInfo,
            mentor: req.user.id

        });


        await childGroup.save();

        res.send(childGroup)

    } catch (err) {
        console.log(err);
        res.status(500).send("Error in Saving");
    }
};

module.exports.childGroupGetMiddleware = auth;

module.exports.childGroupGetFunction = async (req, res) => {

    try {
        const childGroup = await ChildGroup.find();
        res.json(childGroup);
    } catch (err) {
        res.status(500).json({ msg: "Error in Fetching Child Group" });
    }

};


module.exports.childGroupPatchMiddleware = [
    auth,   

];

module.exports.childGroupPatchFunction = async (req, res) => {
    let childGroupInfo = { ...req.body };    

    let query = { _id: req.params.id };

    await ChildGroup.updateOne(query, childGroupInfo, (err) => {
        if (err) {
            console.log(err);
            res.status(500).json({ error: "Error on saving" });
        }
    });
    res.status(200).json({
        msg: "Updated",
        data: await ChildGroup.findOne(query),
    });
};
