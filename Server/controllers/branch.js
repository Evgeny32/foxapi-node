const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const BranchProfile = require("../model/Profiles/BranchProfile");
const MentorProfile = require("../model/Profiles/MentorProfile");
const LevelLogs = require("../model/LevelLogs");
const ChildProfile = require("../model/Profiles/ChildProfile");
const Tale = require("../model/Tale");
const Task = require("../model/Task");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const sendRegistrationEmail = require("../mails/registration");
const ChildGroup = require("../model/ChildGroup");
const Coaching = require("../model/Coaching");
const Group = require("../model/Group");

module.exports.branchProfileGetMiddleware = auth;

const branchProfileGet = async (req, res, id) => {
  try {
    const profile = await BranchProfile.findOne({ user: id });
    if (profile) {
      profile.user = await User.findById(id);
      res.json(profile);
    } else {
      res.status(404).json({ msg: "Not found test" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.branchProfileGetFunction = async (req, res) => {
  branchProfileGet(req, res, req.user.id);
};

module.exports.branchProfilePostMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificatePDF", maxCount: 1 },
    { name: "accountingSertificatePDF", maxCount: 1 },
    { name: "partnerFormPDF", maxCount: 1 },
  ]),
  check("name", "name is empty").not().isEmpty(),
  check("region", "region is empty").not().isEmpty(),
  check("director", "director is empty").not().isEmpty(),
  check("email", "email is invalid").isEmail(),
];

const branchProfilePost = async (req, res, id) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User
  const userInfo = {
    email: req.body.email,
  };
  //  Инфа для обновления модели BranchProfile
  let branchInfo = {
    region: req.body.region,
    director: req.body.director,
    name: req.body.name,
  };
  //  Файлы для обновления модели BranchProfile
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        branchInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  try {
    // Проверка на почту
    let user = await User.findOne({
      email: userInfo.email,
    });
    if (user && user.id != id) {
      return res.status(411).json({
        msg: "Email Existed",
      });
    }
    user = await User.updateOne({ _id: id }, userInfo, (err) => {
      if (err) {
        return res.status(500).json({ error: "Error on saving" });
      }
    });
    branch = await BranchProfile.findOne({ user: id });
    let message = "Branch updated";
    if (branch) {
      branch = await BranchProfile.updateOne(
        { user: id },
        branchInfo,
        (err) => {
          if (err) {
            return res.status(500).json({ error: "Error on saving" });
          }
        }
      );
    } else {
      message = "Branch created";
      branch = await new BranchProfile({ ...branchInfo, user: id });
      await branch.save();
    }

    let branchData = await BranchProfile.findOne({ user: id });
    if (branchData) {
      branchData.user = await User.findById(id);
    }

    res.status(200).json({
      msg: message,
      data: branchData,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.branchProfilePatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificatePDF", maxCount: 1 },
    { name: "accountingSertificatePDF", maxCount: 1 },
    { name: "partnerFormPDF", maxCount: 1 },
  ]),
];

module.exports.branchProfilePatchFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User

  //  Инфа для обновления модели BranchProfile
  let branchInfo = {
    region: req.body.region,
    director: req.body.director,
    name: req.body.name,
  };
  //  Файлы для обновления модели BranchProfile
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        branchInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  try {
    // Проверка на почту

    let branchQuery = { user: req.user.id };
    branch = await BranchProfile.updateOne(branchQuery, branchInfo, (err) => {
      if (err) {
        return res.status(500).json({ error: "Error on saving" });
      }
    });

    let branchData = await BranchProfile.findOne(branchQuery);

    res.status(200).json({
      msg: "update",
      data: branchData,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.branchProfilePostFunction = async (req, res) => {
  branchProfilePost(req, res, req.user.id);
};

module.exports.branchProfileGetByIdMiddleware = auth;
module.exports.branchProfileGetByIdFunction = async (req, res) => {
  branchProfileGet(req, res, req.params.id);
};

module.exports.branchProfilePostByIdMiddleware = [
  auth,
  documentUpload.fields([
    { name: "registrationSertificatePDF", maxCount: 1 },
    { name: "accountingSertificatePDF", maxCount: 1 },
    { name: "partnerFormPDF", maxCount: 1 },
  ]),
  check("name", "name is empty").not().isEmpty(),
  check("region", "region is empty").not().isEmpty(),
  check("director", "director is empty").not().isEmpty(),
  check("email", "email is invalid").isEmail(),
];

module.exports.branchProfilePostByIdFunction = async (req, res) => {
  branchProfilePost(req, res, req.params.id);
};
// mentor otel
module.exports.MentorListViewPostMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
  ]),
];
module.exports.MentorListViewPostFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let mentInfo = { ...req.body };
  const { email, phone, surName, name, patronymic } = req.body;
  const role = "mentor";
  console.log(mentInfo);
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        mentInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }
  let user = await User.findOne({
    email: email,
  });
  if (user) {
    return res.status(411).json({
      msg: "Email Already Registered",
    });
  }

  try {
    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      email,
      password,
      role,
      phone,
      surName,
      name,
      patronymic,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();
    mentor = new MentorProfile({
      user: user._id,
      ...mentInfo,
    });
    await mentor.save();
    sendRegistrationEmail(email, { email: email, password: password });
    let mentorData = await MentorProfile.findOne({ user: user._id });
    if (mentorData) {
      mentorData.user = await User.findById(user._id);
    }

    res.status(200).json({
      msg: "create mentor",
      data: mentorData,
    });

    // const data = {
    //   user: {
    //     id: user.id,
    //   },
    // };

    // jwt.sign(
    //   data,
    //   "randomString",
    //   // {
    //   //   expiresIn: 10000,
    //   // },
    //   (err, token) => {
    //     if (err) throw err;
    //     res.status(200).json({
    //       token,
    //     });
    //   }
    // );
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.MentorListViewGetMiddleware = auth;

module.exports.MentorListViewGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find({
      branch: req.user.id,
    }).populate("user");

    mentors = mentors.map((mentors) => mentors.toJSON());
    for await (mentor of mentors) {
      //console.log(mentor.user.id);
      console.log(mentor);
      let groups = await Group.find({ mentor: mentor.user.id });

      let children = 0;
      if (groups) {
        for await (let grp of groups) {
          let groupChild = grp.childList;

          children += groupChild.length;

          mentor.children = children;
          trainings = await Coaching.find({
            mentor: mentor.user._id,
          });
          mentor.training = trainings.length;
          mentor.groups = groups.length;
          console.log(groups.length);
          mentor.all = 0;
          mentor.children = children;
        }
      } else {
        mentor.all = 0;
        console.log(mentor.all);
        mentor.children = 0;
        mentor.training = 0;
        mentor.groups = 0;
        console.log(mentor.groups);
      }
    }

    res.status(200).json(mentors);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching mentor " });
  }
};

module.exports.MentorViewGetMiddleware = auth;

module.exports.MentorViewGetFunction = async (req, res) => {
  try {
    console.log(req.params.id);
    let mentors = await MentorProfile.findOne({
      user: req.params.id,
    }).populate("user");
    console.log(mentors);
    res.json(mentors);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.MentorViewPatchMiddleware = [auth];

module.exports.MentorViewPatchFunction = async (req, res) => {
  let mentor = { ...req.body };

  let query = { _id: req.params.id };

  await User.updateOne(query, mentor, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });

  let mentorQuery = { user: req.user.id };

  await MentorProfile.updateOne(mentorQuery, mentor, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await MentorProfile.findOne(branchQuery),
      user: await User.findOne(query),
    },
  });
};
module.exports.MentorGroupListViewGetMiddleware = auth;

module.exports.MentorGroupListViewGetFunction = async (req, res) => {
  try {
    let groups = await Group.find({
      mentor: req.params.id,
    }).populate("childList");
    if (groups) {
      res.status(200).json(groups);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};
//end mentor otel
// Children

module.exports.BranchChildListViewGetMiddleware = auth;

module.exports.BranchChildListViewGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find({
      branch: req.user.id,
    });
    let children = [];
    for await (let mentor of mentors) {
      let child_list = await ChildProfile.find({ mentor: mentor.user._id });
      for await (let child of child_list) {
        let tale = Tale.find({ child: child._id });
        if (tale) {
          child.tale = tale.name;
        } else {
          child.tale = "Сказка не найдена";
        }
        children += child_list.length;
      }
    }

    res.json(children);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.BranchChildViewGetMiddleware = auth;

module.exports.BranchChildViewGetFunction = async (req, res) => {
  try {
    try {
      const child = await ChildProfile.findOne({
        child: req.params.id,
      });
      try {
        const mentor_profile = await MentorProfile.findOne({
          mentor: child.mentor._id,
        });
        try {
          const branch = await BranchProfile.findOne(
            (user = mentor_profile.branch._id)
          );
          let franchisee_account_id = branch.franchisee.id;
          branch = branch.name;
          try {
            franchisee = FranchiseeProfile.findOne({
              user: franchisee_account_id,
            });
            franchisee = franch.name;
          } catch {
            branch = "";
            franchisee = "Франчайз еще не добавил свои данные";
          }
        } catch {
          branch = "";
          franchisee = "Франчайз еще не добавил свои данные";
        }
      } catch {
        branch = "";
        franchisee = "Франчайз еще не добавил свои данные";
      }
      let tale = Tale.find({ child: child._id });
      if (tale) {
        tale = tale.name;
      } else {
        tale = "Сказка не найдена";
      }
      try {
        let group = await ChildGroup.findOne({ children__in: child._id }); //Достаем группу ребенка
        group = group.name;
      } catch {
        group = "Группа не найдена";
      }

      let user = User.findOne((_id = child.user._id));
      for await (let item of user) {
        child.ban = item.ban;
      }

      // Присваиваем все
      child.branch = branch;
      child.franchisee = franchisee;
      child.tale = tale;
      child.group = group;
    } catch {
      res.status(404).json({
        title: "Не найден",
        message: "Профиль ребенка не был найден, error: err ",
      });
    }

    res.json(child);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.ChildLogsViewGetMiddleware = auth;

module.exports.ChildLogsViewGetFunction = async (req, res) => {
  try {
    logs = await LevelLogs.findOne({ child: req.params.id });
    res.status(200).json(logs);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};
module.exports.ChildTaleListViewGetMiddleware = auth;

module.exports.ChildTaleListViewGetFunction = async (req, res) => {
  try {
    let child_profile = ChildProfile.findOne({ child: req.params.id });
    let tales = Tale.find({ child: child_profile });
    res.status(200).json(tales);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};
module.exports.ChildTaleViewGetMiddleware = auth;

module.exports.ChildTaleViewGetFunction = async (req, res) => {
  try {
    let tale = Tale.findOne({ _id: req.params.id });
    let tasks = Task.find({ tale: tale._id });
    for await (let task of tasks) {
      task.answer = Answer.find({ task: task._id });
    }
    tale.tasks = tasks;
    res.status(200).json(tale);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in Fetching ", error: err });
  }
};

module.exports.BanChildViewPatchMiddleware = auth;

module.exports.BanChildViewPatchFunction = async (req, res) => {
  let data = { ...req.body };

  try {
    let child = ChildProfile.findOne({ _id: req.params.id });
    query = { _id: child.child.id };
    await User.updateOne(query, data, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });
    res.status(200).json({
      msg: "Updated",
      data: {
        user: await User.findOne(query),
      },
    });
  } catch (err) {
    console.log(err);
    res.status(400).json({ msg: "child not found ", error: err });
  }
};
