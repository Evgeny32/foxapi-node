const KeyOnly = require("../middleware/KeyOnly");
// const Partner = require("../model/Integration/Partner");
// const BankAccount = require("../model/Integration/BankAccount");
// const Service = require("../model/Integration/Service");
const Contract = require("../model/Integration/Contract");
const Invoice = require("../model/Integration/Invoice/Invoice");
const Realization = require("../model/Integration/Realization/Realization");
const Receipt = require("../model/Integration/Receipt/Receipt");
// Организация
module.exports.organisationMiddleware = KeyOnly;
module.exports.organisationGetFunction = async (req, res) => {
  const orgs = Partner.find({ IsOrganization: true });
  const Id = req.body.Id;
  if (Id === "") {
    orgs = orgs.find({ Id: Id });
  }

  res.status(200).json({
    msg: "",
    data: orgs,
  });
};
// Партнеры
module.exports.partnerMiddleware = KeyOnly;
module.exports.partnerGetFunction = async (req, res) => {
  const partners = Partner.find({ IsOrganization: false });
  const Id = req.body.Id;
  if (Id === "") {
    partners = partners.find({ Id: Id });
  }

  res.status(200).json({
    msg: "",
    data: partners,
  });
};
// Банковские счета
module.exports.bankAccountsMiddleware = KeyOnly;
module.exports.bankAccountsGetFunction = async (req, res) => {
  const bank_accounts = BankAccount.find({});
  const OwnerId = req.body.OwnerId;
  if (OwnerId === "") {
    partners = partners.find({ OwnerId: OwnerId });
  }

  res.status(200).json({
    msg: "",
    data: bank_accounts,
  });
};
// Номенклатура (услуги)
module.exports.ServicesMiddleware = KeyOnly;
module.exports.ServicesGetFunction = async (req, res) => {
  const services = Service.find({});
  const Id = req.body.Id;
  if (Id === "") {
    services = services.find({ Id: Id });
  }

  res.status(200).json({
    msg: "",
    data: services,
  });
};
// Договоры
module.exports.ContractsMiddleware = KeyOnly;
module.exports.ContractsGetFunction = async (req, res) => {
  const contracts = Contract.find({});
  const Cont_Id = req.body.Cont_Id;
  const Cont_Org = req.body.Cont_Org;
  const Cont_Owner = req.body.Cont_Owner;

  if (Cont_Id === "") {
    contracts = contracts.find({ Cont_Id: Cont_Id });
  }
  if (Cont_Org === "") {
    contracts = contracts.find({ Cont_Org: Cont_Org });
  }
  if (Cont_Owner === "") {
    contracts = contracts.find({ Cont_Owner: Cont_Owner });
  }

  res.status(200).json({
    msg: "",
    data: contracts,
  });
};
// Счета на оплату
module.exports.invoicesMiddleware = KeyOnly;
module.exports.invoicesGetFunction = async (req, res) => {
  const invoices = Invoice.find({});
  const Doc_Id = req.body.Doc_Id;
  const doc_data1 = req.body.doc_data1;
  const doc_data2 = req.body.doc_data2;
  const Id_Org = req.body.Id_Org;
  const Id_Partner = req.body.Id_Partner;
  if (Doc_Id === "") {
    invoices = invoices.find({ "Header.Doc_Id": Doc_Id });
  }
  if (doc_data1 === "") {
    invoices = invoices.find({ "Header.doc_data1": doc_data1 });
  }
  if (doc_data2 === "") {
    invoices = invoices.find({ "Header.doc_data2": doc_data2 });
  }

  if (doc_data2 === "") {
    invoices = invoices.find({ "Header.doc_data2": doc_data2 });
  }
  if (Id_Org === "") {
    invoices = invoices.find({ "Header.Id_Org": Id_Org });
  }
  if (Id_Partner === "") {
    invoices = invoices.find({ "Header.Id_Partner": Id_Partner });
  }
  const res_data = {
    Invoices: [],
    Services: [],
    Organizations: [],
    Partners: [],
    Contracts: [],
  };
  res_data.Invoices = new list(invoices);
  for await (let invoice of res_data.Invoices) {
    res_data.Organizations.push(invoice.Header.Id_Org);
    res_data.Partners.push(invoice.Header.Id_Partner);
    res_data.Contracts.push(invoice.Header.Id_dog);
    for await (let line of invoice.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};
// Реализации услуг
module.exports.RealizationMiddleware = KeyOnly;
module.exports.RealizationGetFunction = async (req, res) => {
  const realizations = Realization.find({});
  const Doc_Id = req.body.Doc_Id;
  const doc_data1 = req.body.doc_data1;
  const doc_data2 = req.body.doc_data2;
  const Id_Org = req.body.Id_Org;
  const Id_Partner = req.body.Id_Partner;
  if (Doc_Id === "") {
    realizations = realizations.find({ "Header.Doc_Id": Doc_Id });
  }
  if (doc_data1 === "") {
    realizations = realizations.find({ "Header.doc_data1": doc_data1 });
  }
  if (doc_data2 === "") {
    realizations = realizations.find({ "Header.doc_data2": doc_data2 });
  }

  if (doc_data2 === "") {
    realizations = realizations.find({ "Header.doc_data2": doc_data2 });
  }
  if (Id_Org === "") {
    realizations = realizations.find({ "Header.Id_Org": Id_Org });
  }
  if (Id_Partner === "") {
    realizations = realizations.find({ "Header.Id_Partner": Id_Partner });
  }
  const res_data = {
    Realizations: [],
    Invoices: [],
    Services: [],
    Organizations: [],
    Partners: [],
    Contracts: [],
  };
  res_data.Realizations = new list(realizations);
  for await (let realization of res_data.Realizations) {
    res_data.Organizations.push(realization.Header.Id_Org);
    res_data.Partners.push(realization.Header.Id_Partner);
    res_data.Contracts.push(realization.Header.Id_dog);
    res_data.Invoices.push(realization.Header.Id_Invoice);

    es_data.Organizations.push(realization.Header.Id_Invoice.Header.Id_Org);
    res_data.Partners.push(realization.Header.Id_Invoice.Header.Id_Partner);
    res_data.Contracts.push(realization.Header.Id_Invoice.Header.Id_dog);
    res_data.Invoices.push(realization.Header.Id_Invoice);
    for await (let line of realization.Header.Id_Invoice.Lines) {
      res_data.Services.push(line.Service_Id);
    }

    for await (let line of realization.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};

//Поступления услуг

module.exports.receiptsMiddleware = KeyOnly;
module.exports.receiptsGetFunction = async (req, res) => {
  const receipts = Receipt.find({});
  const Doc_Id = req.body.Doc_Id;
  const doc_data1 = req.body.doc_data1;
  const doc_data2 = req.body.doc_data2;
  const Id_Org = req.body.Id_Org;
  const Id_Partner = req.body.Id_Partner;
  if (Doc_Id === "") {
    receipts = receipts.find({ "Header.Doc_Id": Doc_Id });
  }
  if (doc_data1 === "") {
    receipts = receipts.find({ "Header.doc_data1": doc_data1 });
  }
  if (doc_data2 === "") {
    receipts = receipts.find({ "Header.doc_data2": doc_data2 });
  }

  if (doc_data2 === "") {
    receipts = receipts.find({ "Header.doc_data2": doc_data2 });
  }
  if (Id_Org === "") {
    receipts = receipts.find({ "Header.Id_Org": Id_Org });
  }

  const res_data = {
    Receipts: [],
    Services: [],
    Organizations: [],
    Partners: [],
    Contracts: [],
  };
  res_data.Receipts = new list(receipts);
  for await (let receipt of res_data.Receipts) {
    res_data.Organizations.push(receipt.Header.Id_Org);
    res_data.Partners.push(receipt.Header.Id_Partner);
    res_data.Contracts.push(receipt.Header.Id_dog);
    for await (let line of receipt.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};
//Платежные поручения
module.exports.PaymentOrdersMiddleware = KeyOnly;
module.exports.PaymentOrdersGetFunction = async (req, res) => {
  const payment_orders = PaymentOrder.find({});
  const Doc_Id = req.body.Doc_Id;
  const doc_data1 = req.body.doc_data1;
  const doc_data2 = req.body.doc_data2;
  const Id_Org = req.body.Id_Org;
  const Id_Partner = req.body.Id_Partner;
  const Account = req.body.Account;

  if (Doc_Id === "") {
    payment_orders = payment_orders.find({ Doc_Id: Doc_Id });
  }
  if (doc_data1 === "") {
    payment_orders = payment_orders.find({ doc_data1: doc_data1 });
  }
  if (doc_data2 === "") {
    payment_orders = payment_orders.find({ doc_data2: doc_data2 });
  }

  if (doc_data2 === "") {
    payment_orders = payment_orders.find({ doc_data2: doc_data2 });
  }
  if (Id_Org === "") {
    payment_orders = payment_orders.find({ Id_Org: Id_Org });
  }
  if (Account === "") {
    payment_orders = payment_orders.find({ Id_Org: Account });
  }
  const res_data = {
    PaymentOrders: [],
    Organizations: [],
    Partners: [],
    Contracts: [],
  };
  res_data.PaymentOrders = new list(payment_orders);
  for await (let payment_order of res_data.PaymentOrders) {
    res_data.Organizations.push(receipt.Header.Id_Org);
    res_data.Partners.push(receipt.Header.Id_Partner);
    res_data.Contracts.push(receipt.Header.Id_dog);
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};
//Отчеты о розничных продажах
module.exports.SalesReportsMiddleware = KeyOnly;
module.exports.SalesReportsGetFunction = async (req, res) => {
  const sales_reports = SalesReport.find({});
  const Doc_Id = req.body.Doc_Id;
  const doc_data1 = req.body.doc_data1;
  const doc_data2 = req.body.doc_data2;
  const Id_Org = req.body.Id_Org;

  if (Doc_Id === "") {
    sales_reports = sales_reports.find({ Doc_Id: Doc_Id });
  }
  if (doc_data1 === "") {
    sales_reports = sales_reports.find({ doc_data1: doc_data1 });
  }
  if (doc_data2 === "") {
    sales_reports = sales_reports.find({ doc_data2: doc_data2 });
  }

  if (doc_data2 === "") {
    sales_reports = sales_reports.find({ doc_data2: doc_data2 });
  }
  if (Id_Org === "") {
    sales_reports = sales_reports.find({ Id_Org: Id_Org });
  }
  if (Account === "") {
    sales_reports = sales_reports.find({ Id_Org: Account });
  }
  const res_data = {
    SalesReports: [],
    Organizations: [],
    Services: [],
  };
  res_data.SalesReports = new list(sales_reports);
  for await (let sales_report of res_data.SalesReports) {
    res_data.Organizations.push(sales_report.Header.Id_Org);
    res_data.Partners.push(sales_report.Header.Id_Partner);
    res_data.Contracts.push(sales_report.Header.Id_dog);
    for await (let line of sales_report.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};

module.exports.IncomesMiddleware = KeyOnly;
module.exports.IncomesGetFunction = async (req, res) => {
  const incomes = Income.find({});

  const res_data = {
    Incomes: [],
    Organizations: [],
    Services: [],
    Partners: [],
    Invoices: [],
    Contracts: [],
  };
  res_data.Incomes = new list(incomes);
  for await (let income of res_data.Incomes) {
    res_data.Organizations.push(income.Id_Org);
    res_data.Partners.push(income.Id_Partner);
    res_data.Contracts.push(income.Id_dog);
    res_data.Invoices.push(income.Id_Invoice);
    for await (let line of income.Id_Invoice.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};
module.exports.IncomesPostMiddleware = KeyOnly;
module.exports.IncomesPostFunction = async (req, res) => {
  const incomes = { ...req.body };
  const res_incomes = [];
  try {
    for await (let income of incomes) {
      try {
        const income_in_db = Income.findOne({ Doc_id: income.Doc_Id });
        const inc = new Income({ income_in_db, ...income });
        await inc.save();
      } catch {
        const inc = new Income(payement);
        await inc.save();
      }
      res_incomes.push(inc);
    }
    res.status(200).json({
      msg: "",
      data: inc,
    });
  } catch {
    res.status(400).json({
      msg: "BAD REQUEST",
    });
  }
};

// Списания денежных средств
module.exports.PaymentsMiddleware = KeyOnly;
module.exports.PaymentsGetFunction = async (req, res) => {
  const payments = Payment.find({});

  const res_data = {
    Payments: [],
    Organizations: [],
    Services: [],
    Partners: [],
    Receipts: [],
    Contracts: [],
    PaymentOrders: [],
  };
  res_data.Payments = new list(payments);
  for await (let payment of res_data.Payments) {
    res_data.Organizations.push(payment.Id_Org);
    res_data.Partners.push(payment.Id_Partner);
    res_data.Contracts.push(payment.Id_dog);
    res_data.Receipts.push(payment.Id_Receipt);

    res_data.Organizations.push(payment.Id_Receipt.Header.Id_Org);
    res_data.Partners.push(payment.Id_Receipt.Header.Id_Partner);
    res_data.Contracts.push(payment.Id_Receipt.Header.Id_dog);
    res_data.PaymentOrders.push(payment.Id_Order);

    res_data.Organizations.push(payment.Id_Order.Id_Org);
    res_data.Partners.push(payment.Id_Order.Id_Partner);
    res_data.Contracts.push(payment.Id_Order.Id_dog);

    for await (let line of payment.Id_Receipt.Lines) {
      res_data.Services.push(line.Service_Id);
    }
  }
  res.status(200).json({
    msg: "",
    data: res_data,
  });
};
module.exports.payementPostMiddleware = KeyOnly;
module.exports.payementPostFunction = async (req, res) => {
  const payments = { ...req.body };
  const res_payments = [];
  try {
    for await (let payment of payments) {
      try {
        const payment_in_db = Payment.findOne({ Doc_id: payment.Doc_Id });
        const inc = new Payment({ payment_in_db, ...payment });
        await inc.save();
      } catch {
        const inc = new Payment({ payement });
        await inc.save();
      }
      res_payments.push(inc);
    }
    res.status(200).json({
      msg: "created",
      data: inc,
    });
  } catch {
    res.status(400).json({
      msg: "BAD REQUEST",
    });
  }
};

module.exports.CatalogMiddleware = KeyOnly;
module.exports.CatalogGetFunction = async (req, res) => {
  const data = {
    Organizations: Partner.find({ IsOrganization: true }),
    Services: Service.find(),
    Partners: Partner.find({ IsOrganization: false }),
    BankAccounts: BankAccount.find(),
    Contracts: Contract.find(),
  };

  res.status(200).json({
    msg: "",
    data: data,
  });
};

module.exports.DocumentsMiddleware = KeyOnly;
module.exports.DocumentsGetFunction = async (req, res) => {
  const data = {
    Invoices: Invoice.find({}),
    Realizations: Realization.find(),
    Receipts: Receipt.find({}),
    PaymentOrders: PaymentOrder.find(),
    SalesReports: SalesReport.find(),
  };

  res.status(200).json({
    msg: "",
    data: data,
  });
};

module.exports.DocumentsOutcomeMiddleware = KeyOnly;
module.exports.DocumentsOutcomeFunction = async (req, res) => {
  const data = {
    Incomes: Income.find({}),
    Payments: Payment.find(),
  };

  res.status(200).json({
    msg: "",
    data: data,
  });
};

module.exports.DocumentsOutPostcomeMiddleware = KeyOnly;
module.exports.DocumentsOutcomePostFunction = async (req, res) => {
  try {
    const data = { ...req.body };
    const doc = new this.DocumentsOutcome({ ...data });
    res.status(200).json({
      msg: "",
      data: doc,
    });
  } catch {
    res.status(400).json({
      msg: "BAD REQUEST",
    });
  }
};
