const auth = require("../middleware/auth");
const { json } = require("express");
const { documentUpload } = require("../middleware/files");
const sendDocs = require("../mails/docs");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const FranchiseeProfile = require("../model/FranchiseeProfile");
const User = require("../model/User");
const AgentProfile = require("../model/Profiles/AgentProfile");
const FranchiseePackage = require("../model/FranchiseePackage");
const ChildProfile = require("../model/Profiles/ChildProfile");
const ChildGroup = require("../model/ChildGroup");
const Tale = require("../model/Tale");
const MentorProfile = require("../model/Profiles/MentorProfile");
const BranchProfile = require("../model/Profiles/BranchProfile");
const Group = require("../model/Group");
const { SWAGGER_TAG } = require("swagger-autogen/src/statics");

module.exports.adminUserListPostMiddleware = [
  auth,
  check("phone", "Please Enter a Valid Phone").not().isEmpty(),
  check("email", "Please enter a valid email").isEmail(),
];

module.exports.adminUserListPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { phone, email, role } = req.body;
  try {
    // Проверка на наличие юзера
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(411).json({
        msg: "Email Already Registered",
      });
    }
    user = await User.findOne({
      phone,
    });
    if (user) {
      return res.status(412).json({
        msg: "Phone Already Registered",
      });
    }
    // Проверка на наличие юзера

    //let password = randomString(8); //TODO: Раскоментировать, для генерации пароля
    let password = "7777"; //TODO: Закоментировать, для генерации пароля
    user = new User({
      phone,
      email,
      password,
      role,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    sendRegistrationEmail(email, { email: email, password: password });

    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      // {
      //   expiresIn: 10000,
      // },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.adminUserListGetMiddleware = auth;

module.exports.adminUserListGetFunction = async (req, res) => {
  try {
    let users = await User.find({ role: "adminBuch" });

    res.status(200).json(users);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching user");
  }
};

module.exports.adminChildrenListGetMiddleware = auth;

module.exports.adminChildrenListGetFunction = async (req, res) => {
  try {
    let children = await ChildProfile.find();

    children = children.map((child) => child.toJSON());

    for await (child of children) {
      let tale = await Tale.find({ child: child.user }).sort("number")[0];
      if (tale) child.tale = tale.name;
      else child.tale = "Сказка не найдена";

      let mentorProfile = await MentorProfile.find({
        mentor: child.mentor.user,
      });
      if (!mentorProfile) {
        child.isMy = false;
        mentorProfile = null;
      }

      let branchProfile = await BranchProfile.find({
        user: mentorProfile.branch,
      });
      if (!branchProfile) {
        child.isMy = false;
        branchProfile = null;
      }

      if (branchProfile.franchisee == req.user.id) {
        child.isMy = true;
      } else {
        child.isMy = false;
      }
    }

    res.status(200).json(children);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching children");
  }
};

module.exports.adminChildDetailGetMiddleware = auth;

module.exports.adminChildDetailGetFunction = async (req, res) => {
  try {
    let child = await ChildProfile.findOne({ child: req.params.id });
    if (!child) return res.status(404).json({ msg: "Child not found" });

    child = child.toJSON();

    let mentor = await MentorProfile.findOne({ user: child.mentor });

    let branch = await BranchProfile.findOne({ user: mentor.branch });
    if (!branch) return (branch = "");
    if (!mentor) return (branch = "");

    let tale = await Tale.find({ child: child.user }).sort("number")[0];
    if (tale) child.tale = tale.name;
    else child.tale = "Сказка не найдена";

    let group = await ChildGroup.findOne({ children: { $in: child.id } });
    if (group) child.group = group.name;
    else child.group = "Груп не найден";

    let user = await User.findOne({ _id: child.child });

    child.ban = user.isBaned;

    child.branch = branch;

    res.status(200).json(child);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching child");
  }
};

module.exports.adminBranchListGetMiddleware = auth;

module.exports.adminBranchListGetFunction = async (req, res) => {
  try {
    let branches = await BranchProfile.find().populate("user");
    branches = branches.map((branch) => branch.toJSON());

    for await (branch of branches) {
      console.log(branch.user.id);
      let mentorAll = await MentorProfile.find({ branch: branch.user.id });
      console.log(mentorAll);
      let childrenAll = 0;
      let groupAll = 0;

      mentorAll = mentorAll.map((mentor) => mentor.toJSON());

      for await (mentor of mentorAll) {
        console.log(branch);
        let children = await ChildProfile.find({ mentor: mentor.user });

        children = children.map((child) => child.toJSON());
        console.log(children);
        childrenAll += children.length;

        let groups = await Group.find({ mentor: mentor.user });

        groups = groups.map((group) => group.toJSON());
        console.log(groups);
        groupAll += groups.length;
      }

      branch.mentors = mentorAll.length;
      branch.children = childrenAll;
      branch.groups = groupAll;
      branch.all = 0;
      console.log(branch.children);
    }
    res.status(200).json(branches);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching branch");
  }
};

module.exports.adminOtherBranchGetMiddleware = auth;

module.exports.adminOtherBranchGetFunction = async (req, res) => {
  try {
    let branches = await BranchProfile.find({ franchisee: req.params.id });

    branches = branches.map((branch) => branch.toJSON());

    for await (branch of branches) {
      let mentorAll = await MentorProfile.find({ branch: branch.user });
      let childrenAll = 0;
      let groupAll = 0;

      mentorAll = mentorAll.map((mentor) => mentor.toJSON());

      for await (mentor of mentorAll) {
        let children = await ChildProfile.find({ mentor: mentor.user });

        children = children.map((child) => child.toJSON());
        console.log(children);
        childrenAll += children.length;

        let groups = await ChildGroup.find({ mentor: mentor.user });

        groups = groups.map((group) => group.toJSON());
        console.log(groups);
        groupAll += groups.length;
      }

      branch.mentors = mentorAll.length;
      branch.children = childrenAll;
      branch.groups = groupAll;
      branch.all = 0;
    }

    res.status(200).json(branches);
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in fetching branch");
  }
};

module.exports.makeMainBranchMiddleware = auth;

module.exports.makeMainBranchFunction = async (req, res) => {
  try {
    let mainBranch = null;

    let branch = await BranchProfile.find({ main: true });

    if (branch) {
      mainBranch = branch.main = false;
    }

    res.json(mainBranch);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in making main branch" });
  }
};

module.exports.mainBranchPatchMiddleware = auth;

module.exports.mainBranchPatchFunction = async (req, res) => {
  // TODO Тут не понятно надо выяснить

  try {
    makeMainBranchMiddleware();

    let getBranch = await BranchProfile.find({ user: req.params.id });
    if (!getBranch) return res.status(404).json({ msg: "Branch not found" });

    let branchData = { ...req.body };

    let query = { user: req.params.id };

    await BranchProfile.updateOne(getBranch, branchData, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await BranchProfile.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    json.status(500).json({ msg: "Error in patching main branch" });
  }
};

module.exports.adminAgentsListGetMiddleware = auth;

module.exports.adminAgentsListGetFunction = async (req, res) => {
  try {
    let agent = await AgentProfile.find();
    if (!agent) return res.status(404).json({ msg: "Agent profile not found" });

    res.status(200).json(agent);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching agent" });
  }
};

module.exports.adminAgentDetailGetMiddleware = auth;

module.exports.adminAgentDetailGetFunction = async (req, res) => {
  try {
    let agent = await AgentProfile.findOne({ user: req.params.id });
    if (!agent) return res.status(404).json({ msg: "Agent profile not found" });

    res.status(200).json(agent);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching agent" });
  }
};

module.exports.adminAgentPatchMiddleware = [
  auth,
  documentUpload.fields([
    check("birthday", "Please enter a valid date").isDate(),
    check("emailBuch", "Please enter a valid email").isEmail(),
    // check("passport", "Please enter a valid passport number").isPassportNumber(),

    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
  ]),
];

module.exports.adminAgentPatchFunction = async (req, res) => {
  try {
    let agentInfo = { ...req.body };

    if (req.files) {
      for await (let fileItem of Object.entries(req.files)) {
        if (fileItem[1].length) {
          agentInfo[fileItem[0]] = fileItem[1][0].path;
        }
      }
    }

    let query = { user: req.params.id };

    await AgentProfile.updateOne(query, agentInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await AgentProfile.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching agent" });
  }
};

module.exports.adminFranchiseesListGetMiddleware = auth;

module.exports.adminFranchiseesListGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.find();
    if (!franchisee)
      return res.status(404).json({ msg: "Franchisee profile not found" });

    res.status(200).json(franchisee);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching franchisee" });
  }
};

module.exports.adminFranchiseesDetailGetMiddleware = auth;

module.exports.adminFranchiseesDetailGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.findOne({
      user: req.params.id,
    }).populate("user");
    if (!franchisee)
      return res.status(404).json({ msg: "Franchisee profile not found" });

    res.status(200).json(franchisee);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching franchisee" });
  }
};
// module.exports.adminFranchiseeListGetMiddleware = auth;

// module.exports.adminFranchiseeListGetFunction = async (req, res) => {
//   try {
//     let franchisee = await FranchiseeProfile.find();
//     if (!franchisee) return res.status(404).json({ msg: "franchisee profile not found" });

//     res.status(200).json(franchisee);
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ msg: "Error in fetching franchisee" });
//   }
// };

module.exports.adminFranchiseePatchMiddleware = [
  auth,
  documentUpload.fields([
    check("birthday", "Please enter a valid date").isDate(),
    check("emailBuch", "Please enter a valid email").isEmail(),
    // check("passport", "Please enter a valid passport number").isPassportNumber(),

    { name: "passportPDF", maxCount: 1 },
    { name: "decisionPDF", maxCount: 1 },
    { name: "protocolPDF", maxCount: 1 },
    { name: "organizationPDF", maxCount: 1 },
    { name: "ipPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "oqrnipPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
    { name: "contractPDF", maxCount: 1 },
  ]),
];

module.exports.adminFranchiseePatchFunction = async (req, res) => {
  try {
    let franchiseeInfo = { ...req.body };

    if (req.files) {
      for await (let fileItem of Object.entries(req.files)) {
        if (fileItem[1].length) {
          franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
        }
      }
    }

    let query = { user: req.params.id };

    await FranchiseeProfile.updateOne(query, franchiseeInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await FranchiseeProfile.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching Franchisee" });
  }
};
module.exports.adminFranchiseePackageBlockGetMiddleware = auth;

module.exports.adminFranchiseePackageBlockGetFunction = async (req, res) => {
  try {
    let package = await FranchiseePackage.findOne({ purchaser: req.params.id });
    if (!package)
      return res.status(404).json({ msg: "Franchisee profile not found" });

    res.status(200).json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching franchisee" });
  }
};

module.exports.adminFranchiseePackageBlockPatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "contractFile", maxCount: 1 },
    { name: "billFile", maxCount: 1 },
    { name: "aktFile", maxCount: 1 },
  ]),
];

module.exports.adminFranchiseePackageBlockPatchFunction = async (req, res) => {
  try {
    let franchiseePakageInfo = { ...req.body };

    if (req.files) {
      for await (let fileItem of Object.entries(req.files)) {
        if (fileItem[1].length) {
          franchiseeInfo[fileItem[0]] = fileItem[1][0].path;
        }
      }
    }

    let query = { user: req.params.id };

    await FranchiseePackage.updateOne(query, franchiseePakageInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await FranchiseePackage.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching " });
  }
};

module.exports.adminFranchiseePackageViewGetMiddleware = auth;

module.exports.adminFranchiseePackageViewGetFunction = async (req, res) => {
  try {
    let packages = await FranchiseePackage.find({ purchaser: req.params.id });
    packages = packages.map((package) => package.toJSON());
    for await (package of packages) {
      let franchisee = FranchiseeProfile.findOne({ user: package.purchaser });
      package.franchisee = franchisee;
      package.agent = null;
    }

    res.status(200).json(packages);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching franchisee" });
  }
};
