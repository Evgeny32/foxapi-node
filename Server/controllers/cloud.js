const UserCloud = require("../model/Cloud/UserCloud");
const CloudFile = require("../model/Cloud/CloudFile");
const Cloud = require("../model/Cloud/Cloud");
const User = require("../model/User");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { documentUpload } = require("../middleware/files");
module.exports.CloudSettingsGetMiddleware = [auth, admin];

module.exports.CloudSettingsGetFunction = async (req, res) => {
  try {
    const cloud_settings = await Cloud.find({}).limit(1).sort({ date: -1 });
    if (!cloud_settings) {
      cloud_settings = new Cloud();
      cloud_settings.save();
    }

    res.status(200).json({
      msg: "Cloud settings received",
      data: cloud_settings,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud settings" });
  }
};

module.exports.CloudSettingsPatchMiddleware = [auth, admin];
module.exports.CloudSettingslPatchFunction = async (req, res) => {
  let cloud_setting = { ...req.body };

  const cloud_settings = await Cloud.find({}).limit(1).sort({ date: -1 });
  if (!cloud_settings) {
    cloud_settings = new Cloud();
    cloud_settings.save();
  }

  await Cloud.updateOne(cloud_settings, cloud_setting, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " Cloud settings updated",
    data: {
      cloud_setting: await Cloud.find({}),
    },
  });
};

module.exports.userCloudGetMiddleware = auth;

module.exports.userCloudGetFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.params.id });

    res.status(200).json({
      msg: "Cloud  received",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudPostMiddleware = auth;
module.exports.CloudPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let cloud_setting = { ...req.body };
  try {
    const old_cloud = await UserCloud.findOne({ user: req.params.id });
    if (old_cloud) {
      res.status(406).json({ msg: "Cloud already exist" });
    }

    const cloud_settings = await Cloud.find({}).limit(1).sort({ date: -1 });
    if (!cloud_settings) {
      cloud_settings = new Cloud();
      cloud_settings.save();
    }

    const cloud = new UserCloud({
      capacity: cloud_settings.initial_capacity,
      user: req.params.id,
      ...cloud_setting,
    });
    cloud.save();
    res.status(200).json({
      msg: "Cloud created'",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudPatchMiddleware = auth;
module.exports.CloudPatchFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let cloud_setting = { ...req.body };
  let query = { _id: req.params.id };

  await UserCloud.updateOne(query, cloud_setting, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " Cloud Updated",
    data: {
      transaction: await Notification.findOne(query),
    },
  });
};
module.exports.CloudDeleteMiddleware = auth;

module.exports.CloudDeleteFunction = async (req, res) => {
  await UserCloud.findByIdAndDelete({ _id: req.params.id });
  res.status(200).json({
    msg: " Cloud delete",
  });
};

module.exports.CloudTokenGetMiddleware = auth;

module.exports.CloudTokenGetFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.user.id });

    res.status(200).json({
      msg: "Cloud  received",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudTokenPostMiddleware = auth;
module.exports.CloudTokenPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  try {
    let cloud_setting = { ...req.body };
    let old_cloud = await UserCloud.findOne({ user: req.user.id });
    if (old_cloud) {
      res.status(406).json({ msg: "Cloud already exist" });
    }

    let cloud_settings = await Cloud.find({}).limit(1).sort({ date: -1 });
    if (!cloud_settings) {
      cloud_settings = new Cloud();
      cloud_settings.save();
    }

    const cloud = new UserCloud({
      capacity: cloud_settings.initial_capacity,
      user: req.user.id,
      ...cloud_setting,
    });
    cloud.save();
    res.status(200).json({
      msg: "Cloud created'",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudTokenPatchMiddleware = auth;
module.exports.CloudTokenPatchFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let cloud_setting = { ...req.body };
  let query = { _id: req.user.id };

  await UserCloud.updateOne(query, cloud_setting, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " Cloud Updated",
    data: {
      transaction: await Notification.findOne(query),
    },
  });
};
module.exports.CloudTokenDeleteMiddleware = auth;

module.exports.CloudTokenDeleteFunction = async (req, res) => {
  await UserCloud.findByIdAndDelete({ _id: req.params.id });
  res.status(200).json({
    msg: " Cloud delete",
  });
};

module.exports.CloudInfoGetMiddleware = auth;

module.exports.CloudInfoGetFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.user.id });
    const cloud_files = await UserCloud.findOne({ cloud: cloud._id });
    cloud.files = cloud_files;
    res.status(200).json({
      msg: "Cloud  received",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
module.exports.CloudInfoGetByIdMiddleware = auth;

module.exports.CloudInfoGetByIdFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.params.id });
    const cloud_files = await UserCloud.findOne({ cloud: cloud._id });
    cloud.files = cloud_files;
    res.status(200).json({
      msg: "Cloud  received",
      data: cloud,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudFileTokenMiddleware = auth;

module.exports.CloudFileTokenFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.user.id });
    const files = await CloudFile.find({ cloud: cloud._id });

    res.status(200).json({
      msg: "Files  received",
      data: files,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
module.exports.cloudFilesPostMiddleware = [
  auth,
  documentUpload.fields([{ name: "file", maxCount: 1 }]),
];
module.exports.cloudFilesFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    const cloud = await UserCloud({ user: req.user.id });

    if (req.files) {
      let capacity = cloud.used_capacity + req.files.file.size;
      if (capacity < cloud.capacity) {
        let file_type = req.files.file.mimetype;
        if (file_type === "application") {
          file_type = "other";
        }

        req.body.type = file_type;
        req.body.cloud = cloud._id;
        req.body.name = req.files.file.filename;
        req.body.size = req.files.file.size;
      }
      let filer = { ...req.body };

      const filee = new CloudFile({
        ...filer,
      });
      filee.save();
      res.status(200).json({
        msg: "File saved",
        data: filee,
      });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};

module.exports.CloudFilesGetMiddleware = auth;

module.exports.CloudFilesGetFunction = async (req, res) => {
  try {
    const cloud = await UserCloud.findOne({ user: req.params.id });
    const files = await CloudFile.find({ cloud: cloud._id });

    res.status(200).json({
      msg: "Files  received",
      data: files,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
module.exports.cloudFilesPostMiddleware = [
  auth,
  documentUpload.fields([{ name: "file", maxCount: 1 }]),
];
module.exports.cloudFilesFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    const cloud = await UserCloud({ user: req.params.id });

    if (req.files) {
      let capacity = cloud.used_capacity + req.files.file.size;
      if (capacity < cloud.capacity) {
        let file_type = req.files.file.mimetype;
        if (file_type === "application") {
          file_type = "other";
        }

        req.body.type = file_type;
        req.body.cloud = cloud._id;
        req.body.name = req.files.file.filename;
        req.body.size = req.files.file.size;
      }
      let filer = { ...req.body };

      const filee = new CloudFile({
        ...filer,
      });
      filee.save();
      res.status(200).json({
        msg: "File saved'",
        data: filee,
      });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
module.exports.CloudFileGetMiddleware = auth;

module.exports.CloudFileGetFunction = async (req, res) => {
  try {
    const files = await CloudFile.find({ _id: req.params.id });

    res.status(200).json({
      msg: "Files  received",
      data: files,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
// module.exports.CloudFileRenamedMiddleware = auth;

// module.exports.CloudFileRenamedFunction = async (req, res) => {
//   try {
//     const file = await CloudFile.findByIdAndDelete({ _id: req.params.id });
//     initial_path = file.file.path;
//     new_path = file.file.path.replace(
//       file.file.filename,
//       req.file.file.filename
//     );
//     new_name = file.file.filename.replace(
//       file.file.filename,
//       req.file.file.filename
//     );

//     res.status(200).json({
//       msg: "Files  deleted",
//       data: file,
//     });
//   } catch (err) {
//     res.status(500).json({ msg: "Error in Fetching Cloud " });
//   }
// };

module.exports.CloudFileDeleteMiddleware = auth;

module.exports.CloudFileDeleteFunction = async (req, res) => {
  try {
    const file = await CloudFile.findOne({ _id: req.params.id });
    const cloud = await UserCloud.findOne({ _id: file.cloud._id });
    cloud.used_capacity -= file.size;
    if (cloud.used_capacity >= 0) {
      await cloud.save();
    }
    const files = await CloudFile.findByIdAndDelete({ _id: req.params.id });
    res.status(200).json({
      msg: "Files  deleted",
      data: files,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Cloud " });
  }
};
