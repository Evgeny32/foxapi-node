const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const Achievement = require("../model/Achievement/Achievement");
const ReceivedAchievement = require("../model/Achievement/ReceivedAchievement");
const { upload, avatarUpload } = require("../middleware/files");
module.exports.AchievementInitMiddleware = [auth, avatarUpload.single("image")];

module.exports.AchievementInitFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  achievement = await Achievement.findOne({
    name: req.body.name,
  });
  if (!achievement) {
    let achiev = { ...req.body };
    if (req.file) {
      achiv.image = req.file.path;
    }
    achievement = new Achievement(...achiev);
    await achievement.save();
  }
};

module.exports.AchievementInitMiddleware = auth;

module.exports.AchievementInitPostFunction = async (req, res) => {
  const fs = require("fs-extra");
  try {
    let achievemens = [];
    let achievements = await fs.readJson("../achievements/achievements.json");
    for await (let achievement of achievements) {
      let ach = await Achievement.findOne({
        name: achievement.name,
      });
      if (!ach) {
        achiev = new Achievement(achievement);
        achiev.save();
        achievemens.push(achiev);
      }
    }
    res.status(200).json({
      data: achievements,
    });
  } catch (err) {
    res.status(404).json({ msg: "Error in Fetching " });
  }
};

module.exports.AchievementReceivedMiddleware = auth;

module.exports.AchievementReceivedGetFunction = async (req, res) => {
  try {
    const achievements = await Achievement.find();
    if (achievements) {
      for await (let achievement of achievements) {
        let receivedAchievement = await ReceivedAchievement.findOne({
          achievement: achievement._id,
          user: req.user.id,
        });
        if (receivedAchievement) {
          achievement.achieved = true;
        } else {
          achievement.achieved = false;
        }
      }
      res.status(200).json({
        msg: "Achievement received",
        data: achievements,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Achievement" });
  }
};

module.exports.AchievementPostMiddleware = auth;

module.exports.AchievementPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    achievement = new Achievement({
      name: req.body.name,
      description: req.body.description,
    });

    await achievement.save();
    res.status(200).json({
      msg: "Achievement created",
      data: achievement,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Achievement" });
  }
};
//  Получение, отмена получения достижений

module.exports.AchievementAchievedMiddleware = auth;

module.exports.AchievementAchievedGetFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  try {
    let receivedAchievement = await ReceivedAchievement.findOne({
      achievement: req.params.id,
      user: req.user.id,
    });
    if (receivedAchievement) {
      res.status(411).json({ msg: "Achievement already achieved" });
    } else {
      receivedAchievement = new ReceivedAchievement({
        user: req.user.id,
        achievement: req.params.id,
      });

      await receivedAchievement.save();

      res.status(200).json({
        msg: "Achievement achieved",
        data: receivedAchievement,
      });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching ReceivedAchievement" });
  }
};

module.exports.AchievementUnachievedMiddleware = auth;

module.exports.AchievementUnachievedGetFunction = async (req, res) => {
  try {
    let receivedAchievement = await ReceivedAchievement.findOne({
      achievement: req.params.id,
      user: req.user.id,
    });
    if (!receivedAchievement) {
      res.status(404).json({ msg: "Not found" });
    } else {
      let receivedAchievement = await ReceivedAchievement.findOneAndRemove({
        achievement: req.params.id,
        user: req.user.id,
      });

      res.status(200).json({
        msg: "Achievement unachieved",
        data: receivedAchievement,
      });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching ReceivedAchievement" });
  }
};
