const { check, validationResult } = require("express-validator");

const User = require("../model/User");
const FranchiseeProfile = require("../model/FranchiseeProfile");
const FranchBuchProfile = require("../model/FranchBuchProfile");
const MentorProfile = require("../model/MentorProfile");
const Tale = require("../model/Tale");

const auth = require("../middleware/auth");
// const BranchProfile = require("../model/BranchProfile");
// const ChildProfile = require("../model/ChildProfile");
const ChildGroup = require("../model/ChildGroup");
const Coaching = require("../model/Coaching");

module.exports.coachingPostMiddleware = auth;

module.exports.coachingPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let coachingInfo = { ...req.body };

  try {
    coaching = new Coaching({
      user: req.user.id,
      ...coachingInfo,
    });

    await coaching.save();

    let query = { _id: req.user.id };

    await User.updateOne(query, coachingInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "profile created",
      profile: await Coaching.findOne({ name: coachingInfo.name }), //TODO Не правильно работает
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.coachingGetMiddleware = auth;

module.exports.coachingFunction = async (req, res) => {
  try {
    const coaching = await Coaching.findOne({ user: req.user.id });
    res.json(coaching);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching Coaching Profile" });
  }
};

module.exports.coachingPatchMiddleware = auth;

module.exports.coachingPatchFunction = async (req, res) => {
  let coaching = { ...req.body };

  let coachingQuery = { user: req.user.id };

  await Coaching.updateOne(coachingQuery, coaching, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: { profile: await Coaching.findOne(coachingQuery) },
  });
};
const { upload, avatarUpload } = require("../middleware/files");
// const { check, validationResult } = require("express-validator");
// const auth = require("../middleware/auth");

// const User = require("../model/User");
// const Coaching = require("../model/Coaching");

module.exports.coachingParentFreeGetMiddleware = auth;
module.exports.coachingParentFreeGetFunction = async (req, res) => {
  try {
    // let userId = req.user.id;
    let coachings = await Coaching.find({ customers: { $nin: [req.user.id] } });
    coachings = coachings.map((coaching) => coaching.toJSON());
    for await (coaching of coachings) {
      coaching.mentor = (await User.findById(coaching.mentor)).toJSON();
    }
    res.json({ msg: "Свободные тренинги успешно получены", data: coachings });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      msg: "Ошибка при получении тренингов. Обратитесь к администрации",
    });
  }
};

module.exports.coachingParentMyGetMiddleware = auth;
module.exports.coachingParentMyGetFunction = async (req, res) => {
  try {
    // let userId = req.user.id;
    let coachings = await Coaching.find({ customers: { $in: [req.user.id] } });
    coachings = coachings.map((coaching) => coaching.toJSON());
    for await (coaching of coachings) {
      coaching.mentor = (await User.findById(coaching.mentor)).toJSON();
    }
    res.json({ msg: "Ваши тренинги успешно получены", data: coachings });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      msg: "Ошибка при получении тренингов. Обратитесь к администрации",
    });
  }
};

module.exports.coachingPostMiddleware = [
  auth,
  check("name", "Пожалуйста, введите название тренинга").not().isEmpty(),
  check("description", "Пожалуйста, введите описание тренинга").not().isEmpty(),
  check("cost", "Пожалуйста, введите стоимость тренига").isInt(),
  check("startDate", "Пожалуйста, введите правильную дату начала тренинга")
    .not()
    .isEmpty(),
  check("endDate", "Пожалуйста, введите правильную дату окончания тренинга")
    .not()
    .isEmpty(),
];
module.exports.coachingPostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { name, description, cost, startDate, endDate } = req.body;
  try {
    let coaching = new Coaching({
      name,
      description,
      cost,
      startDate,
      endDate,
      mentor: req.user.id,
    });

    await coaching.save();

    res.status(200).json({
      msg: "Тренинг успешно создан",
      data: coaching,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.coachingSignUpPostMiddleware = auth;
module.exports.coachingSignUpPostFunction = async (req, res) => {
  try {
    let coaching = await Coaching.findById(req.params.id);
    coaching = await Coaching.updateOne(
      { _id: coaching.id },
      { customers: [...coaching.customers, req.user.id] },
      (err) => {
        if (err) {
          console.log(err);
          res.status(500).json({
            msg: "Ошибка при сохранении. Обратитесь к администратору",
          });
        }
      }
    );

    res.status(200).json({
      msg: "Вы успешно записались на тренинг",
      data: (await Coaching.findById(req.params.id)).toJSON(),
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};
