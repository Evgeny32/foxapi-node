const { upload, avatarUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const AgentProfile = require("../model/Profiles/AgentProfile");
const Package = require("../model/Package");
const FranchiseeProfile = require("../model/FranchiseeProfile");
const FranchiseePackage = require("../model/FranchiseePackage");
FranchiseeProfile;
module.exports.agentPackagesByIdGetMiddleware = auth;
module.exports.agentPackagesByIdGetFunction = async (req, res) => {
  try {
    let packages = [];
    // let agent = await AgentProfile.findOne({ user: req.params.id }).populate(
    //   "purchaser"
    // );


    let franchisees = await FranchiseeProfile.find({ agent: req.params.id });
    console.log(franchisees);

    // let agent = await AgentProfile.findOne({ user: req.params.id });
    // console.log(agent);
    // console.log(agent.clients);
    // console.log(agent.user);
    for (let franchisee of franchisees) {
      let franchiseePackages = await FranchiseePackage.find({
        purchaser: franchisee.user._id,
      }).populate("purchaser");
      if (franchiseePackages.length > 0) {
        franchiseePackages = franchiseePackages.map((pack) => pack.toJSON());
        console.log(franchiseePackages);
        packages = [...packages, ...franchiseePackages];
      }
    }


    // let agent = await FranchiseeProfile.findOne({
    //   agent: req.params.id,
    // }).populate("user");
    // console.log(agent);
    // //for await (let franchiseeId of agent.user) {
    // let franchiseePackages = await FranchiseePackage.find({
    //   purchaser: agent.user,
    // }).populate("purchaser");
    // if (franchiseePackages.length > 0) {
    //   franchiseePackages = franchiseePackages.map((pack) => pack.toJSON());
    //   packages = [...packages, ...franchiseePackages];
    // }
    //}

    console.log(packages);
    res.json({
      msg: "Пакеты агента успешно получены",
      data: packages,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      msg: "Ошибка при получении пакетов агента. Обратитесь к администрации",
    });
  }
};
