const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const ClientManagerProfile = require("../model/Profiles/ClientManagerProfile");
const BranchProfile = require("../model/Profiles/BranchProfile");
const FranchiseeProfile = require("../model/Profiles/FranchiseeProfile");
const MentorProfile = require("../model/Profiles/MentorProfile");
const ChildGroup = require("../model/ChildGroup");
const FranchiseePackage = require("../model/FranchiseePackage");

module.exports.ClientManagerProfileMeGetMiddleware = auth;

module.exports.ClientManagerProfileMeGetFunction = async (req, res) => {
  try {
    const profile = await ClientManagerProfile.findOne({
      user: req.user.id,
    }).populate("user");
    if (profile) {
      res.json(profile.toJSON());
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
module.exports.ClientManagerProfilePostMeMiddleware = auth;

module.exports.ClientManagerProfilePostMeFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let clientManager = { ...req.body };

  try {
    let clientManagerProfile = new ClientManagerProfile({
      user: req.user.id,
      ...clientManager,
    });

    await clientManagerProfile.save();

    res.status(200).json({
      msg: " Client Manager  создан",
      data: clientManagerProfile,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Ошибка при сохранении. Обратитесь к администрации");
  }
};

module.exports.ClientManagerProfilePatchMeMiddleware = auth;

module.exports.ClientManagerProfilePatchMeFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  let clientManager = { ...req.body };

  let query = { _id: req.user.id };

  await ClientManagerProfile.updateOne(query, clientManager, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " clientManager Updated",
    data: {
      client_Manager: await ClientManagerProfile.findOne(query),
    },
  });
};
// ОТДЕЛ ФРАНЧАЙЗИ

module.exports.ClientManagerProfileFranchiseeMeMiddleware = auth;

module.exports.ClientManagerProfileFranchiseeGetFunction = async (req, res) => {
  try {
    let franchisees = [];
    let profile = await ClientManagerProfile.findOne({
      user: req.params.id,
    }).populate("franchisees");

    if (profile) {
      for await (let franch of profile.franchisees) {
        let franchprofile = await FranchiseeProfile.findOne({
          user: franch._id,
        });
        franchisees.push(franchprofile);
      }
      //let franc = await [...franchisees];
      res.status(200).json(franchisees);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
module.exports.FranchiseeByIdMiddleware = auth;

module.exports.FranchiseeByIdFunction = async (req, res) => {
  try {
    let franchisees = {};
    let profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let franch of profile.franchisees) {
        let franchprofile = await FranchiseeProfile.find({
          user: franch._id,
        });
        franchisees.push(franchprofile);
      }
      res.status(200).json({
        msg: "franchess Client Manager",
        data: franchisees,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.FranchiseeDetailMiddleware = auth;

module.exports.FranchiseeDetailGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.findOne({
      _id: req.params.id,
    }).populate("user");

    res.json(franchisee);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching " });
  }
};

module.exports.FranchiseeDetailPatchMiddleware = auth;

module.exports.FranchiseeDetailPatchFunction = async (req, res) => {
  let franchInf = { ...req.body };
  let query = { _id: req.user.id };
  await User.updateOne(query, franchInf, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  let frenshQuery = { user: req.user.id };
  await FranchiseeProfile.updateOne(frenshQuery, franchInf, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: "Updated",
    data: {
      profile: await FranchiseeProfile.findOne(frenshQuery).populate("user"),
    },
  });
};

// client Manager Report

module.exports.ClientManagerProfileReportMiddleware = auth;

module.exports.ClientManagerProfileReportFunction = async (req, res) => {
  try {
    let items = [];
    let profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let franchisee of profile.franchisees) {
        let packages = await FranchiseePackage.find({
          purchaser: franchisee._id,
        });
        for await (let package of packages) {
          let branches = await BranchProfile.find({
            package: package._id,
          });
          for await (let branch of branches) {
            let mentors = await MentorProfile.find({
              branch: branch._id,
            });

            for await (let mentor of mentors) {
              let groups = await ChildGroup.find({
                mentor: mentor._id,
              });
              for await (let group of groups) {
                for await (let child of group.children) {
                  item = {
                    date: package.date,
                    doc_number: package._id,
                    franchisee_name: franchisee.name,
                    mentor_name:
                      mentor.mentor.surname +
                      mentor.mentor.name +
                      mentor.mentor.patronymic,
                    group_number: group._id,
                    child_name:
                      child.child.surname +
                      child.child.name +
                      child.child.patronymic,
                    phone: franchisee.user.phone,
                    // dont forget the condition here
                    duty:
                      franchisee.agent.surname +
                      franchisee.agent.name +
                      franchisee.agent.patronymic,

                    format: package.format,
                    age: package.name,
                  };

                  items.push(item);
                }
              }
            }
          }
        }
      }
      res.status(200).json({
        msg: "Raport Client Manager",
        data: items,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

// Franchisee Package

module.exports.ClientManagerFranchiseePackageMeMiddleware = auth;

module.exports.ClientManagerFranchiseePackageGetFunction = async (req, res) => {
  try {
    let packages = [];
    const profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let franchisee of profile.franchisees) {
        packages += await FranchiseeProfile.find({
          purchaser: franchisee._id,
        });
      }
      for await (let package of packages) {
        package.franchisee = await FranchiseeProfile.findOne({
          user: package.purchaser,
        });
        package.agent = None;
        if (!FranchiseeProfile) {
          package.agent = await AgentProfile.findOne({
            user: package.purchaser,
          });
          package.franchisee = None;
          if (!package.agent) {
            package.agent = None;
            package.franchisee = None;
          }
        }
      }

      res.status(200).json({
        msg: "franchess Packages Client Manager",
        data: packages,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

// agent client manager

module.exports.ClientManagerAgentMiddleware = auth;

module.exports.ClientManagerAgentGetFunction = async (req, res) => {
  try {
    let agents = [];
    const profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let agent of profile.agents) {
        let agentprofile = await AgentProfile.find({
          user: agent._id,
        });
        agents.push(agentprofile);
      }
      res.status(200).json(agents);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.AgentsByIdtMiddleware = auth;

module.exports.AgentsByIdGetFunction = async (req, res) => {
  try {
    let agents = [];
    const profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let agent of profile.agents) {
        let agentprofile = await AgentProfile.find({
          user: agent._id,
        });
        agents.push(agentprofile);
      }
      res.status(200).json({
        msg: "agent Client Manager",
        data: agents,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.ClientManagerAgenDetailtMiddleware = auth;

module.exports.ClientManagerAgentGetDetailFunction = async (req, res) => {
  try {
    const agent = await AgentProfile.findOne({ user: req.params.id }).populate(
      "clients"
    );
    if (agent) {
      res.status(200).json({
        msg: "agent Client Manager",
        data: agent,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
// parent client manager

module.exports.ClientManagerParentMiddleware = auth;

module.exports.ClientManagerParentGetFunction = async (req, res) => {
  try {
    let profile = await ClientManagerProfile.findOne({
      user: req.params.id,
    }).populate("clients");
    if (!profile) {
      res.status(404).json({ msg: "Not found" });
    }
    let data = await profile.clients;
    console.log(data);
    res.status(200).json(data);
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.ParentsByIdMiddleware = auth;

module.exports.ParentsByIdGetFunction = async (req, res) => {
  try {
    const profile = await ClientManagerProfile.findOne({
      user: req.params.id,
    }).populate("clients");
    if (!profile) {
      res.status(404).json({ msg: "Not found" });
    }
    const data = profile.clients;
    res.status(200).json({
      data: data,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.ParentDetailMiddleware = auth;

module.exports.ParentDetailGetFunction = async (req, res) => {
  try {
    let profile = await User.findOne({
      user: req.params.id,
    });
    if (!profile) {
      res.status(404).json({ msg: "Not found" });
    }
    res.status(200).json({
      data: profile,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

// agent client manager

module.exports.ClientManagerMentorMiddleware = auth;

module.exports.ClientManagerAgentGetFunction = async (req, res) => {
  try {
    let mentors = [];
    let profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let mentor of profile.mentors) {
        let mentorProfile = await MentorProfile.find({
          user: mentor._id,
        });
        mentors.push(mentorProfile);
      }
      res.status(200).json(mentors);
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};
module.exports.MentorsByIdMiddleware = auth;

module.exports.MentorsByIdGetFunction = async (req, res) => {
  try {
    let mentors = [];
    const profile = await ClientManagerProfile.findOne({ user: req.params.id });
    if (profile) {
      for await (let mentor of profile.mentors) {
        let mentorProfile = await MentorProfile.find({
          user: mentor._id,
        });
        mentors.push(mentorProfile);
      }
      res.status(200).json({
        msg: "mentor Client Manager",
        data: mentors,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.MentorDetailMiddleware = auth;

module.exports.MentorDetailGetFunction = async (req, res) => {
  try {
    const mentor = await MentorProfile.findOne({ user: req.params.id });
    res.status(200).json({
      msg: "mentor Client Manager",
      data: mentor,
    });
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching" });
  }
};

module.exports.MentorDetailPatchMiddleware = auth;
module.exports.MentorDetailPatchFunction = async (req, res) => {
  let mentor = { ...req.body };

  let query = { _id: req.params.id };

  await MentorProfile.updateOne(query, mentor, (err) => {
    if (err) {
      console.log(err);
      res.status(500).json({ error: "Error on saving" });
    }
  });
  res.status(200).json({
    msg: " mentor Updated",
    data: {
      mentors: await MentorProfile.findOne(query),
    },
  });
};
