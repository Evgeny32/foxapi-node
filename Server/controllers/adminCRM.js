const auth = require("../middleware/auth");
const { json } = require("express");
const { documentUpload } = require("../middleware/files");
const sendDocs = require("../mails/docs");

const FranchiseeProfile = require("../model/FranchiseeProfile");
const User = require("../model/User");
const AgentProfile = require("../model/AgentProfile");
const FranchiseePackage = require("../model/FranchiseePackage");

module.exports.franchiseePackageListGetMiddleware = auth;

module.exports.franchiseePackageListGetFunction = async (req, res) => {
  try {
    let packages = await FranchiseePackage.find();
    if (!packages)
      return res.status(404).json({ msg: "Franchisee Package not found" });
    console.log(packages);

    packages = packages.map((package) => package.toJSON());
    for await (package of packages) {
      let franchisee = await FranchiseeProfile.findOne({
        user: package.purchaser,
      });
      package.franchisee = franchisee;
      package.agent = null;
      if (!franchisee) {
        let agent = await AgentProfile.findOne({ user: package.purchaser });
        package.agent = agent;
        package.franchisee = null;
        if (!agent) {
          package.agent = null;
          package.franchisee = null;
        }
      }
    }

    res.status(200).json(packages);
  } catch (err) {
    console.log(err);

    res.status(500).json({ msg: "Error in fetching packages", error: err });
  }
};

module.exports.franchiseeOtherPackageListGetMiddleware = auth;

module.exports.franchiseeOtherPackageListGetFunction = async (req, res) => {
  try {
    let package = await FranchiseePackage.findOne({ purchaser: req.params.id });
    if (!package) return json.status(404).json({ msg: "Package not found" });
    console.log(package);

    package = package.toJSON();

    let franchisee = await FranchiseeProfile.findOne({
      user: package.purchaser,
    });

    package.franchisee = franchisee;
    package.agent = null;

    if (!franchisee) {
      let agent = await AgentProfile.findOne({ user: package.purchaser });
      package.agent = agent;
      package.franchisee = null;
      if (!agent) {
        package.agent = null;
        package.franchisee = null;
      }
    }

    res.status(200).json(package);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};

module.exports.franchiseePackagePatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "contractFile", maxCount: 1 },
    { name: "billFile", maxCount: 1 },
    { name: "aktFile", maxCount: 1 },
  ]),
];

module.exports.franchiseePackagePatchFunction = async (req, res) => {
  try {
    let packageInfo = { ...req.body };

    if (req.files) {
      for await (let fileItem of Object.entries(req.files)) {
        if (fileItem[1].length) {
          packageInfo[fileItem[0]] = fileItem[1][0].path;
        }
      }
    }

    let query = { _id: req.params.id };

    await FranchiseePackage.updateOne(query, packageInfo, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Error on saving" });
      }
    });

    res.status(200).json({
      msg: "Updated",
      data: { profile: await FranchiseePackage.findOne(query) },
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};

module.exports.franchiseePackageSendPatchMiddleware = [
  auth,
  documentUpload.fields([
    { name: "contractFile", maxCount: 1 },
    { name: "billFile", maxCount: 1 },
    { name: "aktFile", maxCount: 1 },
  ]),
];

module.exports.franchiseePackageSendPatchFunction = async (req, res) => {
  try {
    let package = await FranchiseePackage.findOne({ _id: req.params.id });
    console.log(package);

    package = package.toJSON();

    const email = req.body.email;
    const contractFile = package.contractFile.path;
    const billFile = package.billFile.path;
    const aktFile = package.aktFile.path;

    try {
      sendDocs(email, {
        contractFile: contractFile,
        billFile: billFile,
        aktFile: aktFile,
      });
    } catch (err) {
      console.log(err);
    }

    res.json({ msg: "Документы отправлены" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};

module.exports.allCountGetMiddleware = auth;

module.exports.allCountGetFunction = async (req, res) => {
  try {
    let agents = await User.find({ role: "agent" });
    let franchisees = await User.find({ role: "Franchisee" });

    let count = {};
    count.agents = agents.length;
    count.franchisee = franchisees.length;

    res.status(200).json(count);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};

module.exports.allChildPackageGetMiddleware = auth;

module.exports.allChildPackageGetFunction = async (req, res) => {
  try {
    let children = await User.find({ role: "child" });
    let packagesS2 = await FranchiseePackage.find({ status: 2 });
    let packagesS5 = await FranchiseePackage.find({ status: 5 });

    let count = {};
    count.children = children.length;
    count.packages = packagesS2.length + packagesS5.length;

    res.status(200).json(count);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching package" });
  }
};
