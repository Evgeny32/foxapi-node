const auth = require("../middleware/auth");
const { json } = require("express");
const { check, validationResult } = require("express-validator");
const { documentUpload } = require("../middleware/files");

const FranchiseeProfile = require("../model/FranchiseeProfile");
const FranchiseePackage = require("../model/FranchiseePackage");

// const name = (value, { req }) =>
//     FranchiseePackage.schema.path("name").enumValues.indexOf(value) >= 0
//         ? true
//         : false;

// const format = (value, { req }) =>
//     FranchiseePackage.schema.path("format").enumValues.indexOf(value) >= 0
//         ? true
//         : false;

// const status = (value, { req }) =>
//     FranchiseePackage.schema.path("status").enumValues.indexOf(value) >= 0
//         ? true
//         : false;

module.exports.franchiseePackagePostMiddleware = [
  auth,

  // check("name", "Please Enter a Valid name").custom(name),
  // check("format", "Please Enter a Valid name").custom(format),
  // check("status", "Please Enter a Valid name").custom(status),

  documentUpload.fields([
    { name: "contractFile", maxCount: 1 },
    { name: "billFile", maxCount: 1 },
    { name: "aktFile", maxCount: 1 },
  ]),
];

module.exports.franchiseePackagePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  let packageInfo = { ...req.body };

  if (req.files) {
    packageInfo.contractFile = req.files.contractFile.path;
    packageInfo.billFile = req.files.billFile.path;
    packageInfo.aktFile = req.files.aktFile.path;
  }

  try {
    franchiseePackage = new FranchiseePackage({
      purchaser: req.user.id,
      ...packageInfo,
    });

    await franchiseePackage.save();

    res.status(200).json({
      msg: "profile created",
      profile: await FranchiseePackage.findOne({ purchaser: req.user.id }), //TODO Данные ошибочно отправляет
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.franchiseePackageListGetMiddleware = auth;

module.exports.franchiseePackageListGetFunction = async (req, res) => {
  try {
    let packages = await FranchiseePackage.find({ purchaser: req.user.id });
    if (!packages) return res.status(404).json({ msg: "Packages Not Found" });

    res.status(200).json(packages);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching franchisee package" });
  }
};
