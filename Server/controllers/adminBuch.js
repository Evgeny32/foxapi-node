const auth = require("../middleware/auth");
const { json } = require("express");

const FranchiseeProfile = require("../model/FranchiseeProfile");
const MentorProfile = require("../model/MentorProfile");
const Tale = require("../model/Tale");
const ChildProfile = require("../model/Profiles/ChildProfile");
const ChildGroup = require("../model/ChildGroup");
const Coaching = require("../model/Coaching");
const AgentProfile = require("../model/AgentProfile");

module.exports.adminBuchChildrenGetMiddleware = auth;

module.exports.adminBuchChildrenGetFunction = async (req, res) => {
  try {
    let children = await ChildProfile.find().populate("child");
    if (!children)
      return res.status(404).json({ msg: "Children profile not found" });

    children = children.map((child) => child.toJSON());

    for await (child of children) {
      let tale = await Tale.find({ child: child.user }).sort("number")[0];
      if (tale) child.tale = tale.name;
      else child.tale = "Сказка не найдена";
    }

    res.status(200).json(children);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching children" });
  }
};

module.exports.adminBuchChildDetailGetMiddleware = auth;

module.exports.adminBuchChildDetailGetFunction = async (req, res) => {
  try {
    let child = await ChildProfile.findOne({ child: req.params.id }).populate(
      "child"
    );
    if (!child) return res.status(404).json({ msg: "Child profile not found" });

    child = child.toJSON();

    let tale = await Tale.find({ child: child.user }).sort("number")[0];
    if (tale) child.tale = tale.name;
    else child.tale = "Сказка не найдена";

    res.status(200).json(child);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching child" });
  }
};

module.exports.adminBuchAgentsGetMiddleware = auth;

module.exports.adminBuchAgentsGetFunction = async (req, res) => {
  try {
    let agents = await AgentProfile.find().populate("user");
    if (!agents)
      return res.status(404).json({ msg: "Agents profile not found" });
    res.status(200).json(agents);
  } catch (err) {
    console.log(err);
    res.status(500).json({ msg: "Error in fetching agents" });
  }
};

module.exports.adminBuchAgentDetailGetMiddleware = auth;

module.exports.adminBuchAgentDetailGetFunction = async (req, res) => {
  try {
    let agent = await AgentProfile.findOne({ user: req.params.id }).populate(
      "user"
    );
    if (!agent) return res.status(404).json({ msg: "Agent profile not found" });
    res.status(200).json(agent);
  } catch (err) {
    res.status(500).json({ msg: "Error in fetching agent" });
  }
};

module.exports.adminBuchFranchiseeGetMiddleware = auth;

module.exports.adminBuchFranchiseeGetFunction = async (req, res) => {
  try {
    const franchisee = await FranchiseeProfile.find().populate("user");
    if (!franchisee)
      return res.status(404).json({ msg: "Franchisee profile not found" });
    res.status(200).json(franchisee);
  } catch (err) {
    res.status(500).json({ msg: "Error in fetching franchisees" });
  }
};

module.exports.adminBuchFranchiseeDetailGetMiddleware = auth;

module.exports.adminBuchFranchiseeDetailGetFunction = async (req, res) => {
  try {
    let franchisee = await FranchiseeProfile.findOne({
      user: req.params.id,
    }).populate("user");
    if (!franchisee)
      return res.status(404).json({ msg: "Franchisee profile not found" });
    res.status(200).json(franchisee);
  } catch (err) {
    res.status(500).json({ msg: "Error in fetching franchisee" });
  }
};

module.exports.adminBuchMentorsGetMiddleware = auth;

module.exports.adminBuchMentorsGetFunction = async (req, res) => {
  try {
    let mentors = await MentorProfile.find().populate("user");

    // mentors = mentors.map((mentor) => mentor.toJSON());

    for await (let mentor of mentors) {
      let groups = await ChildGroup.find({ mentor: mentor.user });
      let children = 0;

      groups = groups.map((group) => group.toJSON());

      for await (group of groups) {
        children += group.children.length;
      }

      mentor.children = children;

      trainings = await Coaching.find({ mentor: mentor.user });
      mentor.trainings = trainings.length;
      mentor.groups = groups.length;
      mentor.all = 0;
    }

    res.status(200).json(mentors);
  } catch (err) {
    res.status(500).json({ msg: "Error in fetching mentors" });
  }
};

module.exports.adminBuchMentorDetailGetMiddleware = auth;

module.exports.adminBuchMentorDetailGetFunction = async (req, res) => {
  try {
    let mentor = await MentorProfile.findOne({ user: req.params.id }).populate(
      "user"
    );
    if (!mentor)
      return res.status(404).json({ msg: "Mentor profile not found" });

    res.status(200).json(mentor);
  } catch (err) {
    res.status(500).json({ msg: "Error in fetching mentor", error: err });
  }
};
