const { upload, avatarUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");
const auth = require("../middleware/auth");
const { randomString, randomNumber } = require("../scripts/random");
const sendRegistrationEmail = require("../mails/registration");
// const sendSms = require('../config/sms'); //TODO:Раскоментировать для работы отправки СМС
const bcrypt = require("bcryptjs");

const User = require("../model/User");
const ChildProfile = require("../model/ChildProfile");
const Tale = require("../model/Tale");
const Task = require("../model/Task");
const Answer = require("../model/Answer");


const userCheck = async (value, { req }) => {
  let user = await User.findById(value);
  if (user) {
    return true;
  }
  return false;
};

const rankCheck = (value, { req }) =>
  ChildProfile.schema.path("rank").enumValues.indexOf(value) >= 0
    ? true
    : false;
const learnFormatCheck = (value, { req }) =>
  ChildProfile.schema.path("learnFormat").enumValues.indexOf(value) >= 0
    ? true
    : false;

module.exports.childProfilePostMiddleware = [
  check("email", "Please enter a valid email").isEmail(),
  check("parent", "Please Enter a Valid parent id").custom(userCheck),
  check("mentor", "Please Enter a Valid mentor id").custom(userCheck),
  check("rank", "Please Enter a Valid rank").custom(rankCheck),
  check("learnFormat", "Please Enter a Valid learning format").custom(
    learnFormatCheck
  ),
  check("age", "Please enter a valid age").if(
    (value) => value >= 5 && value <= 16
  ),
  check("surName", "Please Enter a sur name").not().isEmpty(),
  check("name", "Please Enter a name").not().isEmpty(),
  check("patronymic", "Please Enter a patronymic").not().isEmpty(),
];
module.exports.childProfilePostFunction = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const { email, surName, name, patronymic } = req.body;
  try {
    // Проверка на наличие юзера
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(411).json({
        msg: "Email Already Registered",
      });
    }
    // Проверка на наличие юзера
    let password = randomString(8);
    user = new User({
      email: email,
      password,
      role: "child",
      surName,
      name,
      patronymic,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    childProfile = new ChildProfile({
      ...req.body,
      child: user.id,
      year: req.body.age - 4,
    });
    await childProfile.save();
    // TODO: Добавить распределение ребенка по группе
    sendRegistrationEmail(email, {
      email: email,
      password: password,
    });

    const data = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      data,
      "randomString",
      {
        expiresIn: 10000,
      },
      (err, token) => {
        if (err) throw err;
        res.status(200).json({
          token,
          data: { ...childProfile._doc, child: user },
        });
      }
    );
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

module.exports.childTalesGetMiddleware = auth

module.exports.childTalesGetFunction = async(req, res) => {

  try {

  const childProfile = await ChildProfile.findOne({child: req.params.id});  
  const tales = await Tale.find({child: childProfile.id}); 

  for await (tale of tales) {
    let tasks = Task.find({tale: tale.id});
    for await (task of tasks) {
      task.answers = Answer.find({task: task.id}); 
    }
    tale.tasks = tasks;
  }

  res.json(tales);

  } catch (err) {

    console.log(err)
    res.status(500).json({ msg: "Error in Fetching User" });

  }

}


module.exports.childBanPatchMiddleware = auth

module.exports.childBanPatchFunction = async(req, res) => {

  try {
    let query = { _id: req.params.id };
    let {isBaned} = req.body
    await User.updateOne(query, {isBaned}, (err) => {
        if (err) {
            console.log(err);
            res.status(500).json({ error: "Error on saving" });
        }
    });

    child = await ChildProfile.findOne({child: req.params.id})
    child.child = await User.findById(req.params.id) 
    res.json(child);

  } catch (err) {

    console.log(err)
    res.status(500).json({ msg: "Error in Fetching User" });

  }
}
