const { documentUpload } = require("../middleware/files");
const { check, validationResult } = require("express-validator");
const auth = require("../middleware/auth");

const User = require("../model/User");
const AgentProfile = require("../model/Profiles/AgentProfile");
const Package = require("../model/Package");
const { FranchiseeProfileDetailMiddleware } = require("./headSalesManager");
const FranchiseeProfile = require("../model/FranchiseeProfile");
FranchiseeProfile;

module.exports.agentProfileMeGetMiddleware = auth;

module.exports.agentProfileMeGetFunction = async (req, res) => {
  try {
    const profile = await AgentProfile.findOne({ user: req.user.id }).populate(
      "user"
    );
    if (profile) {
      res.json(profile.toJSON());
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

module.exports.agentProfilePostMeMiddleware = [
  auth,
  documentUpload.fields([
    { name: "passportPDF", maxCount: 1 },
    { name: "snilsPDF", maxCount: 1 },
    { name: "innPDF", maxCount: 1 },
    { name: "resumePDF", maxCount: 1 },
  ]),
  check("name", "name is empty").not().isEmpty(),
  check("surName", "surName is empty").not().isEmpty(),
  check("patronymic", "patronymic is empty").not().isEmpty(),
  check("email", "email is invalid").isEmail(),
  check("birthday", "birthday is invalid").isDate(),
  check("citizen", "citizen is empty").not().isEmpty(),
  check("education", "education is empty").not().isEmpty(),
  check("emailBuch", "emailBuch is invalid").isEmail(),
  check("passport", "passport is empty").not().isEmpty(),
  check("passportWho", "passportWho is empty").not().isEmpty(),
  check("passportWhen", "passportWhen is invalid").isDate(),
  check("country", "country is empty").not().isEmpty(),
  check("address", "address is empty").not().isEmpty(),
  check("factAddress", "factAddress is empty").not().isEmpty(),
  check("inn", "inn is empty").not().isEmpty(),
  check("snils", "snils is empty").not().isEmpty(),
];

module.exports.agentProfilePostMeFunction = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  //  Инфа для обновления модели User
  const userInfo = {
    name: req.body.name,
    surName: req.body.surName,
    patronymic: req.body.patronymic,
    email: req.body.email,
  };
  //  Инфа для обновления модели AgentProfile
  //let franch = [];
  let franchisees = await FranchiseeProfile.find({
    agent: req.user.id,
  }).populate("user");
  //franchisees = franchisees.map((pack) => pack.toJSON());
  console.log(franchisees);
  //franch = [...franchisees];
  let agentInfo = {
    //franchisees: franchisees,
    birthday: req.body.birthday,
    citizen: req.body.citizen,
    education: req.body.education,
    emailBuch: req.body.emailBuch,
    passport: req.body.passport,
    passportWho: req.body.passportWho,
    passportWhen: req.body.passportWhen,
    country: req.body.country,
    address: req.body.address,
    factAddress: req.body.factAddress,
    inn: req.body.inn,
    snils: req.body.snils,
  };
  // for await (fr of franchisees) {
  //   console.log(franch);
  // }

  //  Файлы для обновления модели AgentProfile
  if (req.files) {
    for await (let fileItem of Object.entries(req.files)) {
      if (fileItem[1].length) {
        agentInfo[fileItem[0]] = fileItem[1][0].path;
      }
    }
  }

  try {
    // Проверка на почту
    let user = await User.findOne({
      email: userInfo.email,
    });
    if (user && user.id != req.user.id) {
      return res.status(411).json({
        msg: "Email Existed",
      });
    }
    // Проверка на почту бухгалтера
    let agent = await AgentProfile.findOne({
      emailBuch: agentInfo.emailBuch,
    });
    if (agent && agent.user != req.user.id) {
      return res.status(411).json({
        msg: "Buch Email Existed",
      });
    }

    user = await User.updateOne({ _id: req.user.id }, userInfo, (err) => {
      if (err) {
        return res.status(500).json({ error: "Error on saving" });
      }
    });
    agent = await AgentProfile.findOne({ user: req.user.id });
    let message = "Agent updated";
    if (agent) {
      agent = await AgentProfile({ user: req.user.id }, agentInfo, (err) => {
        if (err) {
          return res.status(500).json({ error: "Error on saving" });
        }
      });
    } else {
      message = "Agent created";
      agent = new AgentProfile({ ...agentInfo, user: req.user.id });
      await agent.save();
    }

    let agentData = await AgentProfile.findOne({ user: req.user.id });
    if (agentData) {
      agentData.user = await User.findById(req.user.id);
    }

    res.status(200).json({
      msg: message,
      data: agentData,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send("Error in Saving");
  }
};

// TODO: Перепроверить  с пакетами
module.exports.agentProfileGetPackagesMiddleware = auth;

module.exports.agentProfileGetPackagesFunction = async (req, res) => {
  try {
    let packages = [];
    const agentProfile = await AgentProfile.findOne({ user: req.user.id });
    if (agentProfile) {
      for await (let franchiseeId of agentProfile.franchisees) {
        let franchiseePackages = await Package.find({
          purchaser: franchiseeId,
        });
        packages.push(franchiseePackages);
      }
      res.status(200).json({
        msg: "Packages received",
        data: packages,
      });
    } else {
      res.status(404).json({ msg: "Not found" });
    }
  } catch (err) {
    res.status(500).json({ msg: "Error in Fetching User" });
  }
};

// module.exports.agentProfileGetByIdMiddleware = auth;

// module.exports.agentProfileGetByIdFunction = async (req, res) => {
//   try {
//     const profile = await AgentProfile.findOne({ user: req.params.id });
//     if (profile) {
//       profile.user = await User.findById(req.params.id);
//       res.json(profile);
//     } else {
//       res.status(404).json({ msg: "Not found" });
//     }
//   } catch (err) {
//     res.status(500).json({ msg: "Error in Fetching User" });
//   }
// };

// module.exports.agentProfilePostByIdMiddleware = [
//   auth,
//   documentUpload.fields([
//     { name: "passportPDF", maxCount: 1 },
//     { name: "snilsPDF", maxCount: 1 },
//     { name: "innPDF", maxCount: 1 },
//     { name: "resumePDF", maxCount: 1 },
//   ]),
//   check("name", "name is empty").not().isEmpty(),
//   check("surName", "surName is empty").not().isEmpty(),
//   check("patronymic", "patronymic is empty").not().isEmpty(),
//   check("email", "email is invalid").isEmail(),
//   check("birthday", "birthday is invalid").isDate(),
//   check("citizen", "citizen is empty").not().isEmpty(),
//   check("education", "education is empty").not().isEmpty(),
//   check("emailBuch", "emailBuch is invalid").isEmail(),
//   check("passport", "passport is empty").not().isEmpty(),
//   check("passportWho", "passportWho is empty").not().isEmpty(),
//   check("passportWhen", "passportWhen is invalid").isDate(),
//   check("country", "country is empty").not().isEmpty(),
//   check("address", "address is empty").not().isEmpty(),
//   check("factAddress", "factAddress is empty").not().isEmpty(),
//   check("inn", "inn is empty").not().isEmpty(),
//   check("snils", "snils is empty").not().isEmpty(),
// ];

// module.exports.agentProfilePostByIdFunction = async (req, res) => {
//   const errors = validationResult(req);

//   if (!errors.isEmpty()) {
//     return res.status(400).json({
//       errors: errors.array(),
//     });
//   }
//   //  Инфа для обновления модели User
//   const userInfo = {
//     name: req.body.name,
//     surName: req.body.surName,
//     patronymic: req.body.patronymic,
//     email: req.body.email,
//   };
//   //  Инфа для обновления модели AgentProfile
//   let agentInfo = {
//     birthday: req.body.birthday,
//     citizen: req.body.citizen,
//     education: req.body.education,
//     emailBuch: req.body.emailBuch,
//     passport: req.body.passport,
//     passportWho: req.body.passportWho,
//     passportWhen: req.body.passportWhen,
//     country: req.body.country,
//     address: req.body.address,
//     factAddress: req.body.factAddress,
//     inn: req.body.inn,
//     snils: req.body.snils,
//   };
//   //  Файлы для обновления модели AgentProfile
//   if (req.files) {
//     for await (let fileItem of Object.entries(req.files)) {
//       if (fileItem[1].length) {
//         agentInfo[fileItem[0]] = fileItem[1][0].path;
//       }
//     }
//   }

//   try {
//     // Проверка на почту
//     let user = await User.findOne({
//       email: userInfo.email,
//     });
//     if (user && user.id != req.params.id) {
//       return res.status(411).json({
//         msg: "Email Existed",
//       });
//     }
//     // Проверка на почту бухгалтера
//     let agent = await AgentProfile.findOne({
//       emailBuch: agentInfo.emailBuch,
//     });
//     if (agent && agent.user != req.params.id) {
//       return res.status(411).json({
//         msg: "Buch Email Existed",
//       });
//     }

//     user = await User.updateOne({ _id: req.params.id }, userInfo, (err) => {
//       if (err) {
//         return res.status(500).json({ error: "Error on saving" });
//       }
//     });
//     agent = await AgentProfile.findOne({ user: req.params.id });
//     let message = "Agent updated";
//     if (agent) {
//       agent = await AgentProfile({ user: req.params.id }, agentInfo, (err) => {
//         if (err) {
//           return res.status(500).json({ error: "Error on saving" });
//         }
//       });
//     } else {
//       message = "Agent created";
//       agent = new AgentProfile({ ...agentInfo, user: req.params.id });
//       await agent.save();
//     }

//     let agentData = await AgentProfile.findOne({ user: req.params.id });
//     if (agentData) {
//       agentData.user = await User.findById(req.params.id);
//     }

//     res.status(200).json({
//       msg: message,
//       data: agentData,
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).send("Error in Saving");
//   }
// };
