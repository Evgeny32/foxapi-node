const auth = require("../middleware/auth");
const { json } = require("express");

const FranchiseeProfile = require("../model/FranchiseeProfile");
const MentorProfile = require("../model/MentorProfile");
const Tale = require("../model/Tale");
const BranchProfile = require("../model/BranchProfile");
const ChildProfile = require("../model/ChildProfile");
const ChildGroup = require("../model/ChildGroup");
const Coaching = require("../model/Coaching");


module.exports.franchBuchChildrenGetMiddleware = auth


module.exports.franchBuchChildrenGetFunction = async (req, res) => {

    try {

        let franchisee = await FranchiseeProfile.findOne({ franchBuch: req.user.id });
        if (!franchisee) return res.status(404).json({ msg: 'Franchisee profile not found' });
        let children = [];
        let branches = await BranchProfile.find({ franchisee: franchisee.user });
        if (!branches) return res.status(404).json({ msg: 'Branch profile not found' });

        for await (branch of branches) {
            let mentorList = await MentorProfile.find({ branch: branch.user });


            for await (mentor of mentorList) {
                let childList = await ChildProfile.find({ mentor: mentor.user });
                children = children.concat(childList);
                console.log(children);
            }
        }

        for await (child of children) {
            let tale = await Tale.findOne({ child: child.user });

            if (tale) child.tale = tale
            else child.tale = 'Сказка не найдена'
        }


        res.json(children); // TODO Тут про сказки ничего не выводит (пункт Tale отсутствует)!


    } catch (err) {

        console.log(err)
        res.status(500).json({ msg: "Error in Fetching User" });

    }

};


module.exports.franchBuchChildrenDetailGetMiddleware = auth

module.exports.franchBuchChildrenDetailGetFunction = async (req, res) => {

    try {

        const child = await ChildProfile.findOne({ child: req.params.id });
        if (!child) return res.status(404).json({ msg: 'Child profile not found' });
        const tale = await Tale.find({ child: child.user }).sort('number')[0];
        if (tale) child.tale = tale;
        else child.tale = 'Сказка не найдена';
        console.log(child.tale); // TODO Тут выводит!

        res.status(200).json(child); // TODO Тут про сказки ничего не выводит (пункт Tale отсутствует)!       

    } catch (err) {
        console.log(err)
        res.status(500).json({ msg: "Error in Fetching User" });
    }
}


module.exports.franchBuchMentorGetMiddleware = auth

module.exports.franchBuchMentorGetFunction = async (req, res) => {


    try {

        let franchisee = await FranchiseeProfile.findOne({ franchBuch: req.user.id });
        if (!franchisee) return res.status(404).json({ msg: 'Franchisee profile not found' });
        let branches = await BranchProfile.find({ franchisee: franchisee.user });
        if (!branches) return res.status(404).json({ msg: 'Branch profile not found' });

        let mentors = [];
        for await (branch of branches) {

            mentorList = await MentorProfile.find({ branch: branch.user });
            mentors = [...mentors, ...mentorList]

        }

        mentors = mentors.map(mentor=>mentor.toJSON())

        for await (mentor of mentors) {
          
            groups = await ChildGroup.find({ mentor: mentor.user });
            let children = 0;

            for await (group of groups) {
                children += group.children.length;
            }

            let training = await Coaching.find({mentor: mentor.user});
            mentor.trainings = training.length
            mentor.groups = groups.length
            mentor.all = 0
            mentor.children = children;

                       
        }

        res.status(200).json(mentors);

    } catch (err) {
        console.log(err)
        res.status(500).json({ msg: "Error in Fetching User" });


    }

}


module.exports.franchBuchMentorDetailGetMiddleware = auth

module.exports.franchBuchMentorDetailGetFunction = async(req, res) => {

    try {
        const mentor = await MentorProfile.findOne({user: req.params.id});

        res.status(200).json(mentor);

    } catch (err) {
        res.status(500).json({ msg: "Error in Fetching Mentor Profile" });
    }
}








